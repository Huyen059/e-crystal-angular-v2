# E-crystal Angular app

This project is created for E-crystal app.

## Run app
#### Install dependencies

Run `npm install` to install the necessary dependencies for the project.

#### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## App details
See [app details](docs/app-details.md).

## Adding custom codes

### Adding custom filters 
See [adding custom filter](docs/adding-custom-filters.md).

### Adding command buttons on top of the list of items
See [adding command buttons](docs/adding-command-buttons.md)
