## App details
Content:
- Entry points
- Urls
- Routes
- Stylesheet structures

### Entry points
- `src/index.html`: the single html page used to display all contents.
- `src/styles.css`: main stylesheet
- `src/main.ts`: bootstrap `AppModule`

### Urls

|  Url Segment Diagram  |
|---|
|  ![url-segments-diagram](../src/assets/docs/url-segments-diagram-1.png)  |
|  ![url-segments-diagram](../src/assets/docs/url-segments-diagram-2.png)  |

>Note: not all defined routes are displayed in this diagram. For other components/data elements, the structure is similar to the example shown here for component `erpTimesheets` and data element `Timesheet`.

Some examples:
- `/`
- `/login`
- `/e-crystal`
- `/e-crystal/erpTimesheets`
- `/e-crystal/erpTimesheets/timesheet`
- `/e-crystal/erpTimesheets/timesheet/new`
- `/e-crystal/erpTimesheets/timesheet/edit`
- `/e-crystal/erpTimesheets/timesheet/1234/timesheetEntry`
- `/e-crystal/erpTimesheets/timesheet/1234/timesheetEntry/new`
- `/e-crystal/erpTimesheets/timesheet/1234/timesheetEntry/edit`

### Routes

#### src/app/app-routing.module.ts

```typescript
const routes: Routes = [
  {path: '', redirectTo: 'e-crystal', pathMatch: 'full'},
  {path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule)},
  {path: 'e-crystal', loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule), canActivate: [AuthGuard]},
  {path: '**', component: PageNotFoundComponent}
];
```
> Note: Value specified by key `path` is the url segment to be added to `/`.

In this routing module, 3 routes are defined: `/`, `/e-crystal` and `/login`.
- `/`: homepage (redirect to `e-crystal`)
- `/login`: load all routes defined in module `LoginModule`
- `/e-crystal`: load all routes defined in module `PagesModule`
- For all undefined routes, the `PageNotFoundComponent` will be displayed.

When navigating to `/login`, all routes defined in module `LoginModule` will be loaded. Currently, there is only one route in that module, which will display the Angular component `LoginComponent`. See `src/app/login/login-routing.module.ts` for more detail.

Similarly, when navigating to `/e-crystal`, all routes defined in module `PagesModule` will be loaded.

> Note: The route `/e-crystal` is guarded by `AuthGuard` (`src/app/login/_services/auth.guard.ts`). If the user doesn't log in yet, when navigating to `/`, we will be redirected to the login page `/login`. After the user logged in, the `/e-crystal` route will be loaded.

#### src/app/pages/pages-routing.module.ts

```typescript
const routes: Routes = [
  {path: '', redirectTo: 'erpTimesheets', pathMatch: 'full'},
  {path: 'erpTimesheets', loadChildren: () => import('./components/erpTimesheets/erpTimesheets.module').then(m => m.ErpTimesheetsModule)},
  {path: 'enterprise', loadChildren: () => import('./components/enterprise/enterprise.module').then(m => m.EnterpriseModule)}
];
```
> Note: Value specified by key `path` is the url segment to be added to `/e-crystal`.

When navigating to `/e-crystal`, we will be redirect to `/e-crystal/erpTimesheets`.

For each backend component, one route is defined with the url segment (value of `path`) being the corresponding name of that component. When navigating to this route, all routes defined in the corresponding module (for example, `ErpTimesheetsModule`) will be loaded.

In this case, the route `/e-crystal` is redirected to `/e-crystal/erpTimesheets`.

#### src/app/pages/components/erpTimesheets/erpTimesheets-routing.module.ts

```typescript
const routes: Routes = [
  {path: '', redirectTo: 'timesheet', pathMatch: 'full'},
  {path: 'timesheet', loadChildren: () => import('./timesheet/timesheet.module').then(m => m.TimesheetModule)},
  {path: 'timesheetEntry', loadChildren: () => import('./timesheetEntry/timesheetEntry.module').then(m => m.TimesheetEntryModule)},
  {path: 'timesheetRemark', loadChildren: () => import('./timesheetRemark/timesheetRemark.module').then(m => m.TimesheetRemarkModule)},
  {path: 'expense', loadChildren: () => import('./expense/expense.module').then(m => m.ExpenseModule)},
];
```

This routing module defines routes for the data elements of `erpTimesheets` backend component.

When navigating to `/e-crystal/erpTimesheets`, we will be redirected to `/e-crystal/erpTimesheets/timesheet`.

#### src/app/pages/components/erpTimesheets/timesheet/timesheet-routing.module.ts

```typescript
const routes: Routes = [
  {path: '', component: TimesheetWorkspaceComponent, data: {name: fieldDisplayNames.timesheet},
    children: [
      {path: '', component: TimesheetListComponent, data: {name: ''}},
      {path: 'new', component: TimesheetNewComponent, data: {name: 'New'}},
      {path: 'edit', component: TimesheetEditComponent, data: {name: 'Edit'}},
      {path: ':externalId', component: TimesheetDetailComponent, data: {name: 'Detail'},
        children: [
          {path: '', redirectTo: 'timesheetEntry', pathMatch: 'full'},
          {path: 'timesheetEntry', component: TimesheetEntrySubworkspaceComponent, data: {name: fieldDisplayNames.timesheetEntry},
            children: [
              {path: '', component: TimesheetEntrySublistComponent, data: {name: ''}},
              {path: 'new', component: TimesheetEntryNewComponent, data: {name: 'New'}},
              {path: 'edit', component: TimesheetEntryEditComponent, data: {name: 'Edit'}},
            ]
          },
          {path: 'timesheetRemark', component: TimesheetRemarkSubworkspaceComponent, data: {name: fieldDisplayNames.timesheetRemark},
            children: [
              {path: '', component: TimesheetRemarkSublistComponent, data: {name: ''}},
              {path: 'new', component: TimesheetRemarkNewComponent, data: {name: 'New'}},
              {path: 'edit', component: TimesheetRemarkEditComponent, data: {name: 'Edit'}},
            ]
          },
        ]
      },
    ]
  },
];
```

When navigating to `/e-crystal/erpTimesheets/timesheet`, the `TimesheetWorkspaceComponent` will be displayed. This component contains children components:
- `TimesheetListComponent`: displayed when navigating to `/e-crystal/erpTimesheets/timesheet`
- `TimesheetNewComponent`: displayed when navigating to `/e-crystal/erpTimesheets/timesheet/new`
- `TimesheetEditComponent`: displayed when navigating to `/e-crystal/erpTimesheets/timesheet/edit`
- `TimesheetDetailComponent`: displayed when navigating to `/e-crystal/erpTimesheets/timesheet/:externalId`, with `:externalId` being the placeholder for externalId of a certain timesheet

`TimesheetDetailComponent` contains in turns children components...

### Stylesheet structures
#### General
`src/index.html` uses the main stylesheet file `src/styles.css`.

In this project, scss is used for styling. All relevant scss files, partials, and compiled css file are stored in `src/assets/scss` directory.

- The main scss file is `src/assets/scss/style.scss`.
- The styles of each Angular components are defined in different partial scss files (in directory `src/assets/scss/partials`).
- These partials are then imported into the main scss file.
- Sass compiler is used to compile this main scss file to css format (`src/assets/scss/style.css`).
- This compiled stylesheet is then imported into the main stylesheet file `src/styles.css`.

#### src/assets/scss/partials
This directory contains the scss partials (pieces of scss code which will not be compiled by Sass compiler but can be imported inside style.scss file).

The structure of this directory follows the structure of `src/app` with two sub-directories `components` and `ui`. For example, the styling for `HeaderComponent` (located in `src/app/pages/ui/header`) is in the partial: `src/assets/scss/partials/ui/_app-header.scss`.
