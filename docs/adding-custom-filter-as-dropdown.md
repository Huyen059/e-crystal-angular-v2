## Adding custom filter as dropdown(s)
Example: adding filter by link field `Employee` to `Timesheet` filter.

|  Component diagram  |
|---|
|  ![component-diagram](../src/assets/docs/filter-with-dropdown.png)  |


Only typescript files need to be modified to add a custom dropdown.

### Typescript

#### Step 1: timesheet.ts
Location: `src/app/pages/components/erpTimesheets/timesheet/_models/Timesheet.ts`

- The TimesheetService class will fetch the timesheets using data stored in an object of `Timesheet`. Therefore, a new field needs to be added to `Timesheet` class to store the value of the new filter:
```typescript
export class Timesheet {
  constructor(
    // other fields
    public employee: EmployeeOutputModel = null,
  ) {  }
}
```

>Note: the type of link field is: `[LinkFieldName]OutputModel` (don't forget to import the corresponding class).

#### Step 2: timesheet.service.ts
Add the following code to method `createQueryParameters()`:
```typescript
if (this.fetchItemConditions.employee) {
  parameters.push('employee=' + this.fetchItemConditions.employee.externalId);
}
```
as follows:
```typescript
private createQueryParameters(): string {

  if (this.fetchItemConditions.year) {
    parameters.push('employee=' + this.fetchItemConditions.employee.externalId);
  }

  // (others...)

  return parameters.join('&');
}
```

After this step is done, whenever we have a stored value for field `employee`, the HTTP request will be sent with query, for example, `&employee=[employeeExternalId]`.

#### Step 2: timesheet-filter.component.ts
Location: `src/app/pages/components/filters/components/erpTimesheets/timesheet/timesheet-filter/timesheet-filter.component.ts`

- Add new dropdown data to the `dropdownElements` array. The data is added as an object with the following properties:
  - `elementFullName`: syntax: [componentName].[dataElementName] (all in camelCase).
  - `searchBy`: name of the field to be searched for when user types into the input field of the dropdown. For example: if `searchBy` has value `name`, when the user types an employee name (or just a few letters in the name) into the input field of the dropdown, the employee list displayed in the dropdown option area contains only the employees whose name contains the user input value.
  - `defaultFilter`: whether this new filter should be the default one (i.e. the filter option is chosen by default and the dropdown is displayed from the beginning).
  - `required`: if this form field is required or not. For filter component, this value is always `false`.
  
  ```typescript
  dropdownElements = [
    // other dropdown data
    {
      elementFullName: 'enterprise.employee',
      searchBy: 'name',
      defaultFilter: false,
      required: false,
    },
  ];
  ```

- Add if condition `if (elementFullName === 'enterprise.employee')` to specify what to do with the data of this new dropdown:
  ```typescript
  if (elementFullName === 'enterprise.employee') {
    this.timesheetService.fetchItemConditions.employee = item;
  }
  ```
  as follows:
  ```typescript
  onSelectDropdownItem(item: any, elementFullName: string): void {
    // other if conditions
  
    if (elementFullName === 'enterprise.employee') {
      this.timesheetService.fetchItemConditions.employee = item;
    }
    return;
  }
  ```
