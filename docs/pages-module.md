# PagesModule

This module only contains other modules, there is no Angular component declared here.

The routing module defines the routing to different modules, each of which corresponds to a backend component. For example, the e-crystal application has 3 main components, `erpTimesheets`, `enterprise` and `crmModel`. An Angular module will be created to each of these components (by default, the name of the module is the same as the name of the component in UpperCamelCase format, e.g. ErpTimesheetsModule, EnterpriseModule...)

## `components` directory

The modules created for backend components are located inside `components` directory. This directory contains the following directories:
- `_models`: contains model classes that will be used for all backend components
- `_properties`: contains app property files
- `_services`: contains one service file, which is responsible for:
  - When displaying detail page of an item of a data element, the fullname and externalId of this item will be saved here to be used in children pages (e.g., creating new children item for this parent item). Fields and methods involved: `parentElementFullName`, `parentExternalId`, and `clearParentData()`
  - When the route changes, the PageHeaderComponent will be notified about the route change and will create correct breadcrumbs. Fields and methods involved: `router`, `currentRouter$`, `changeRouter()`.
  - In the detail page of, for example, `Timesheet`, we can create new `TimesheetEntry`. But when the new `TimesheetEntry` is created, the fields `totalManDays` and `totalHours` of the currently displayed `Timesheet` will change. To reflect the change, this detail page needs to be reloaded. 
    
    This is done thanks to the fields and methods: `reloadTime`, `reload$`, and `changeReloadTime()`.  Whenever a `TimesheetEntry` is created, we assign a new value to `reloadTime`. The detail page of `Timesheet` subscribes to `reload$` (see `src/app/pages/components/erpTimesheets/timesheet/timesheet-detail/timesheet-detail.component.ts`). When `reloadTime` changes, the `getTimesheet()` function is called and newest data will be displayed.
    
    (Note: The `reloadTime` is chosen as `Subject<Date>` just for convenient. Whenever we want the page to reload, we need to assign a new value to `reloadTime`. Here the new value is the Date object of the current moment (see `src/app/pages/components/erpTimesheets/timesheetEntry/timesheetEntry-new/timesheetEntry-new.component.ts`). Assigning this Date object to the `reloadTime` will ensure that `reloadTime` will always have a new value.

- `_utils`: contains util classes with static methods used across components.
- `erpTimesheets`/`enterprise`: each directory contains the module of the corresponding backend components. ([See detail](component-module.md)).
- `filters`: contains FilterModule ([see detail](filter-module.md)).
  
## `ui` directory

In the `ui` directory contains
- UIModule: several Angular components are declared here (HeaderComponent, NavComponent, PageHeaderComponent, PageFooterComponent)
  - NavComponent is used inside HeaderComponent
  - HeaderComponent, PageHeaderComponent and PageFooterComponent are used inside the WorkspaceComponent of each data element (thus you can find them in the `exports` array of `pages.module.ts`).
- AlertModule: contains one component: AlertComponent
- DropdownModule: contains one component: FieldDropdownComponent

>The components of the modules in `ui` directory are used in different modules (ErpTimesheetsModule, EnterpriseModule...). Therefore, to avoid possible circular dependency, it's better to create separate modules for them.
