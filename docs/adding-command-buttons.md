## Adding command buttons

The HTML codes for command buttons are put in: 

```angular2html
<div class="element-list-actions-container">
  <div class="element-list-main-content-actions-wrapper">
    <!--command buttons will be placed in here-->
  </div>
  
  <!--buttons to customize table view: set custom page size and choose which columns to be displayed-->
  <app-table-control></app-table-control>
</div>
```

> Note: the CSS class has prefix `element-list` in [DataElement]ListComponent, and prefix `element-sublist` in [DataElement]SublistComponent.

__Example: adding command button to delete Timesheet(s)__

- HTML:
  ```angular2html
  <div class="element-list-main-content-action-button-wrapper">
    <div class="element-list-main-content-action-button" (click)="deleteItems()">
      Delete
    </div>
  </div>
  ```
- Typescript:The click event handler `deleteItems()` must be defined in
  - `TimesheetListComponent` class (located in `src/app/pages/components/erpTimesheets/timesheet/timesheet-list/timesheet-list.component.ts`)
    ```typescript
    deleteItems(): void {
      this.timesheetService.deleteItems(this.selectedItems)
        .subscribe(() => {
          this.getTimesheetOnInit();
          this.selectedItems.clear();
          this.numberOfSelectedItems.next(0);
        });
    }
    ```
  - and `TimesheetService` class (located in `src/app/pages/components/erpTimesheets/timesheet/_services/timesheet.service.ts`):
    ```typescript
    deleteItems(selectedItems: Map<string, TimesheetOutputModel>): Observable<any> {
      const externalIds = [];
      selectedItems.forEach((item, externalId) => externalIds.push(externalId));
      const url = this.apiBaseUrl + '/' + externalIds.join(',') + '?multipleItems=true';
      return this.http.delete(url, this.httpWriteOptions)
        .pipe(
          tap(_ => this.logSuccess('Timesheets deleted')),
          catchError(this.handleError('Delete timesheets'))
        );
    }
    ```
In this case, the `deleteItems()` must be defined in `TimesheetService` because this class is responsible for sending all HTTP requests.
