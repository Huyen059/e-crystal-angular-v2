# Adding custom filter as simple input field(s)

## Example 1: adding filter by year for Timesheet data element.

### Typescript

#### Step 1: timesheet.service.ts
Location: `src/app/pages/components/erpTimesheets/timesheet/_services/timesheet.service.ts`

All HTTP requests are sent from the main service file `timesheet.service.ts`. Therefore, if we want to add a custom filter, the name of the filter field need to be added in this file.

- Add this new field to `filterOptions` array:
  ```typescript
  filterOptions = [
    'year',
    // (other array items...)
  ];
  ```
- If you want your custom filter to be the default one, add them to arrays `defaultFilterOptions`.
  ```typescript
  defaultFilterOptions: string[] = [
    'year',
    // (other array items...)
  ];
  ```

#### Step 2: fieldDisplayName.ts
Location: `src/app/pages/components/_properties/fieldDisplayName.ts`

Add `year: 'Year',` to `fieldDisplayNames` object if there is no field with the same name in that object yet. The value of this field is the text that will displayed in the view (in the checkbox list of filter options).

Optional: If there are multiple languages, the field `year` with correct translations in other languages need to be added too. For example, you need to add `year: 'Jaar',` to `fieldDisplayNamesNL` object in `fieldDisplayName-NL.ts` file.

If other languages are not added, the default display name is in English.

#### Step 3: Timesheet.ts
Location: `src/app/pages/components/erpTimesheets/timesheet/_models/Timesheet.ts`

The value of this new field needs to be saved in Timesheet object to be used for HTTP call later. Thus we need to add a new field in this class by adding a line `public year: number = null,` to the constructor:

```typescript
export class Timesheet {
  constructor(
    public year: number = null,
    // (other fields...)
  ) {  }
}
```

#### Step 4: Back to timesheet.service.ts
Add the following code to method `createQueryParameters()`:

```typescript
private createQueryParameters(): string {

  if (this.fetchItemConditions.year) {
    parameters.push('year=' + this.fetchItemConditions.year);
  }

  // (others...)

  return parameters.join('&');
}
```

After this step is done, whenever we have a stored value for field `year`, the HTTP request will be sent with query, for example, `&year=2020`.

### HTML
Add HTML code to filter form.

#### timesheet-filter.component.html
Location: `src/app/pages/components/filters/components/erpTimesheets/timesheet/timesheet-filter/timesheet-filter.component.html`

```angular2html
<div class="element-filter-option-content" *ngIf="isFilterOptionChosen('year')">
  <div>
    <label for="year">Year</label>
    <input class="element-filter-input"
           [(ngModel)]="fetchItemConditions.year"
           type="text" id="year" name="year">
  </div>
</div>
```

All the changes for adding a custom filter in frontend code are completed. Of course, the backend code still needs to be changed to handle the query parameter `year` sending along with HTTP request.


## Example 2: adding filter by month between for Timesheet data element.

### Typescript

#### Step 1: timesheet.service.ts
Location: `src/app/pages/components/erpTimesheets/timesheet/_services/timesheet.service.ts`

All HTTP requests are sent from the main service file `timesheet.service.ts`. Therefore, if we want to add a custom filter, the name of the filter field need to be added in this file.

- Add this new field to `filterOptions` array:
  ```typescript
  filterOptions = [
    'monthBetween',
    // (other array items...)
  ];
  ```
- If you want your custom filter to be the default one, add them to arrays `defaultFilterOptions`.

#### Step 2: fieldDisplayName.ts
Location: `src/app/pages/components/_properties/fieldDisplayName.ts`

Add `monthBetween: 'Month between',` to `fieldDisplayNames` object if there is no field with the same name in that object yet.

#### Step 3: Timesheet.ts
Location: `src/app/pages/components/erpTimesheets/timesheet/_models/Timesheet.ts`

The value of this new field needs to be saved in Timesheet object to be used for HTTP call later. Thus we need to add 2 new fields to the constructor:

```typescript
export class Timesheet {
  constructor(
    public startMonth: number = null,
    public endMonth: number = null,
    // (other fields...)
  ) {  }
}
```

#### Step 4: Back to timesheet.service.ts
Add the following code to method `createQueryParameters()`:

```typescript
private createQueryParameters(): string {

  if (this.fetchItemConditions.startMonth && this.fetchItemConditions.endMonth) {
    parameters.push('startMonth=' + this.fetchItemConditions.startMonth);
    parameters.push('endMonth=' + this.fetchItemConditions.endMonth);
  }

  // (others...)

  return parameters.join('&');
}
```

### HTML
Add HTML code to filter form.

#### timesheet-filter.component.html
Location: `src/app/pages/components/filters/components/erpTimesheets/timesheet/timesheet-filter/timesheet-filter.component.html`

```angular2html
<div class="element-filter-option-content" *ngIf="isFilterOptionChosen('monthBetween')">
  <div>
    <label for="startMonth">Start month</label>
    <input class="element-filter-input"
           [(ngModel)]="fetchItemConditions.startMonth"
           type="text" id="startMonth" name="startMonth">
  </div>
  <div>
    <label for="endMonth">End month</label>
    <input class="element-filter-input"
           [(ngModel)]="fetchItemConditions.endMonth"
           type="text" id="endMonth" name="endMonth">
  </div>
</div>
```

## Example 3: adding dynamic search by email for Employee data element.
> Note: What does it mean by 'dynamic search by email'? It's an input field where user can type some text/number in to filter Employee by email. The list of employees is updated as the user types.
> Applicable for value fields only (name, email, mobile...)

### Typescript
#### Step 1: employee.service.ts
Location: `src/app/pages/components/enterprise/employee/_services/employee.service.ts`

All HTTP requests are sent from the main service file `employee.service.ts`. Therefore, if we want to add a custom filter, the name of the filter field need to be added in this file.

- Add this new field to `filterOptions` array:
  ```typescript
  filterOptions = [
    'email',
    // (other array items...)
  ];
  ```
- If you want your custom filter to be the default one, add them to arrays `defaultFilterOptions`.

#### Step 2: fieldDisplayName.ts
Location: `src/app/pages/components/_properties/fieldDisplayName.ts`

Add `email: 'Email',` to `fieldDisplayNames` object if there is no field with the same name in that object yet.
  
#### Step 3: Employee.ts
Location: `src/app/pages/components/enterprise/employee/_models/Employee.ts`

The value of this new field needs to be saved in Employee object to be used for HTTP call later. Thus we need to add a new field in this class by adding a line `public email: string = null,` to the constructor:

```typescript
export class Employee {
  constructor(
    public email: string = null,
    // (other fields...)
  ) {  }
}
```

#### Step 4: Back to employee.service.ts
Add the following code to method `createQueryParameters()`:

```typescript
private createQueryParameters(): string {

  if (this.fetchItemConditions.email) {
    parameters.push('email=' + this.fetchItemConditions.email);
  }

  // (others...)

  return parameters.join('&');
}
```

### HTML
#### employee-filter.component.html
Simply add the child component `<app-employee-search>` inside `<form>` tag with correct data in `isFilterOptionChosen()` and `[searchBy]`:

```angular2html
<form class="element-filter-form"
      novalidate>
  <!--other contents-->
  <app-employee-search *ngIf="isFilterOptionChosen('email')" [searchBy]="'email'"
                       (searchResult)="onSearch($event)"></app-employee-search>
  <!--other contents-->
</form>
```

`isFilterOptionChosen()` and `onSearch()` should normally be defined in employee-filter.component.ts already.
