# Login

Everything related to the login/logout action is declared inside __LoginModule__ located inside directory `src/app/login`.

In this directory, there are three subdirectories: `_models` containing model classes, `_service` containing authentication service and guard classes, and `login` containing __LoginComponent__ (an Angular component).

The __LoginComponent__ is declared in `login.module.ts` and the route is defined in `login-routing.module.ts`.

## _models
- __LoginPostInputModel__ class: used to store data to send with the POST request to backend for authentication.
- __LoginOutputModel__ class: used to store the data received as response from the backend after sending POST request to the backend for authentication.
- __User__ class: to store the user data to session storage.

## _services
- __auth.service.ts__: responsible to send http POST request to the backend for authentication.
  - 2 main methods: 
    - `isAuthenticated()` accepts user data as an object of LoginPostInputModel class and return an observable of boolean value. If the user is authenticated, the user data will be saved in session storage. This method is used in LoginComponent class.
    - `logout()`: clear the saved user data in session storage. This method is used in PageHeaderComponent class.
  
- __auth.guard.ts__: responsible to guard the routes (used in `app-routing.module.ts`). Depending on the return value of method `canActivate`, the user can/can't access to the route guarded by this class.
  ```
  {path: 'e-crystal', loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule), canActivate: [AuthGuard]},
  ```
  Here we guard the path `/e-crystal` and any children paths from unauthorized users.

## LoginComponent
- The LoginComponent class is defined in login.component.ts
- The HTML is in login.component.html
- The styling is in `src/assets/scss/partials/_app-login.scss`

Main method: `onSubmit()`: when user submitted the form, the form data will be saved to an object of LoginPostInputModel and sent to the backend via AuthService `isAuthenticated()` method. The result of this method is subscribed here in the LoginComponent and the subsequent actions could be: go to homepage (if login succeeded) or display error.
