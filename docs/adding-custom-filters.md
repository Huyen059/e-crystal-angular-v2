# Adding custom filters

All filter form and filter list components are located in `src/app/pages/components/filters` directory.

Filter form for, for example, data element `Timesheet` of `erpTimesheets` component is located in `src/app/pages/components/filters/components/erpTimesheets/timesheet/timesheet-filter`. The corresponding filter list is located in `src/app/pages/components/filters/components/erpTimesheets/timesheet/timesheet-filter-list`. The filter list (if being displayed) will be under the filter form view.

> Note: When is the filter list not displayed? For example, in the `TimesheetListComponent`, the list of Timesheets are already displayed, thus, the Timesheet filter list is abundant and will not be displayed in the `FilterComponent`.

- Adding custom filters as simple input field(s) (in case filter by value field)

  [Details](adding-custom-filter-as-simple-input.md)


- Adding custom filters as dropdowns (in case filter by link field)

  [Details](adding-custom-filter-as-dropdown.md)
