import {ActionsObservable, Epic} from 'redux-observable';
import {FluxStandardAction} from 'flux-standard-action';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {concat} from 'rxjs';
import {ajaxDelete, AjaxError, ajaxGet, ajaxPost, ajaxPut} from 'rxjs/internal-compatibility';
import {getHttpReadOptions, getHttpWriteOptions} from '../reducers/session.reducer';
import {Utils} from '../../pages/components/_utils/Utils';
import {
  EnterpriseEmployeeActionFactory,
  EnterpriseEmployeeActions
} from '../actions/enterprise.employee.actions';

const elementFullName = 'enterprise.employee';

const listFetchItemsEpic: Epic = (action$: ActionsObservable<FluxStandardAction<string, any, any> >) => {
  return action$.ofType(EnterpriseEmployeeActions.LIST__ITEMS_FETCH_REQUEST_SENT)
    .pipe(
      mergeMap(action => {
        const fetchItemConditions = action.payload && action.payload.fetchItemConditions ? action.payload.fetchItemConditions : '';
        const url = generateUrlForGetItemsRequest(fetchItemConditions);
        return concat(
          ActionsObservable.of(EnterpriseEmployeeActionFactory.fetchQueriesSaved(fetchItemConditions)),
          ActionsObservable.of(EnterpriseEmployeeActionFactory.listItemsFetching()),
          ajaxGet(url, getHttpReadOptions())
            .pipe(
              // delay(1000), // uncomment to see the state change when loading
              map((ajaxResponse) => {
                return EnterpriseEmployeeActionFactory.listItemsFetchedSuccess(ajaxResponse.response);
              }),
              catchError((ajaxError: AjaxError) => {
                return ActionsObservable.of(
                  EnterpriseEmployeeActionFactory.listItemsFetchedError(ajaxError.response));
              })
            )
        );
      })
    );
};

export const listFetchItemsByPageUrlEpic: Epic = (action$: ActionsObservable<FluxStandardAction<string, any, any> >) => {
  return action$.ofType(EnterpriseEmployeeActions.LIST__ITEMS_FETCH_BY_PAGE_URL_REQUEST_SENT)
    .pipe(
      mergeMap(action => {
        const url = action.payload.pageUrl;

        return concat(
          ActionsObservable.of(EnterpriseEmployeeActionFactory.listItemsFetching()),
          ajaxGet(url, getHttpReadOptions())
            .pipe(
              // delay(1000), // uncomment to see the state change when loading
              map((ajaxResponse) => {
                return EnterpriseEmployeeActionFactory.listItemsFetchedSuccess(ajaxResponse.response);
              }),
              catchError((ajaxError: AjaxError) => {
                return ActionsObservable.of(
                  EnterpriseEmployeeActionFactory.listItemsFetchedError(ajaxError.response));
              })
            )
        );
      })
    );
};

export const listRemoveItemEpic: Epic = (action$: ActionsObservable<FluxStandardAction<string, any, any> >) => {
  return action$.ofType(EnterpriseEmployeeActions.LIST__ITEM_REMOVE_REQUEST_SENT)
    .pipe(
      mergeMap(action => {
        const itemExternalId = action.payload && action.payload.itemExternalId ? action.payload.itemExternalId : '';
        const backUrl = action.payload && action.payload.backUrl ? action.payload.backUrl : '';
        const url = generateUrlForWriteRequest(itemExternalId);

        return concat(
          ajaxDelete(url, getHttpWriteOptions())
            .pipe(
              map((ajaxResponse) => {
                return EnterpriseEmployeeActionFactory.listItemRemovedSuccess(ajaxResponse.response, backUrl);
              }),
              catchError((ajaxError: AjaxError) => {
                return ActionsObservable.of(
                  EnterpriseEmployeeActionFactory.listItemRemovedError(ajaxError.response));
              })
            )
        );
      })
    );
};

export const listRemoveItemsEpic: Epic = (action$: ActionsObservable<FluxStandardAction<string, any, any> >) => {
  return action$.ofType(EnterpriseEmployeeActions.LIST__ITEMS_REMOVE_REQUEST_SENT)
    .pipe(
      mergeMap(action => {
        const externalIds = action.payload && action.payload.externalIds ? action.payload.externalIds : '';
        const backUrl = action.payload && action.payload.backUrl ? action.payload.backUrl : '';
        const url = generateUrlForWriteRequest(externalIds);

        return concat(
          ajaxDelete(url, getHttpWriteOptions())
            .pipe(
              map((ajaxResponse) => {
                return EnterpriseEmployeeActionFactory.listItemsRemovedSuccess(ajaxResponse.response, backUrl);
              }),
              catchError((ajaxError: AjaxError) => {
                return ActionsObservable.of(
                  EnterpriseEmployeeActionFactory.listItemsRemovedError(ajaxError.response));
              })
            )
        );
      })
    );
};

export const detailFetchItemEpic: Epic = (action$: ActionsObservable<FluxStandardAction<string, any, any> >) => {
  return action$.ofType(EnterpriseEmployeeActions.DETAIL__ITEM_FETCH_REQUEST_SENT)
    .pipe(
      mergeMap(action => {
        const externalId = action.payload && action.payload.externalId ? action.payload.externalId : '';
        const url = Utils.getElementApiEndpoint(elementFullName) + '/' + externalId;

        return concat(
          ActionsObservable.of(EnterpriseEmployeeActionFactory.detailItemFetching()),
          ajaxGet(url, getHttpReadOptions())
            .pipe(
              // delay(1000), // uncomment to see the state change when loading
              map((ajaxResponse) => {
                return EnterpriseEmployeeActionFactory.detailItemFetchedSuccess(
                  ajaxResponse.response);
              }),
              catchError((ajaxError: AjaxError) => {
                return ActionsObservable.of(
                  EnterpriseEmployeeActionFactory.detailItemFetchedError(ajaxError.response));
              })
            )
        );
      })
    );
};

export const detailRemoveItemEpic: Epic = (action$: ActionsObservable<FluxStandardAction<string, any, any> >) => {
  return action$.ofType(EnterpriseEmployeeActions.DETAIL__ITEM_REMOVE_REQUEST_SENT)
    .pipe(
      mergeMap(action => {
        const itemExternalId = action.payload && action.payload.itemExternalId ? action.payload.itemExternalId : '';
        const backUrl = action.payload && action.payload.backUrl ? action.payload.backUrl : '';
        const url = generateUrlForWriteRequest(itemExternalId);

        return concat(
          ajaxDelete(url, getHttpWriteOptions())
            .pipe(
              map((ajaxResponse) => {
                return EnterpriseEmployeeActionFactory.detailItemRemovedSuccess(ajaxResponse.response, backUrl);
              }),
              catchError((ajaxError: AjaxError) => {
                return ActionsObservable.of(
                  EnterpriseEmployeeActionFactory.detailItemRemovedError(ajaxError.response));
              })
            )
        );
      })
    );
};

export const newAddItemEpic: Epic = (action$: ActionsObservable<FluxStandardAction<string, any, any> >) => {
  return action$.ofType(EnterpriseEmployeeActions.NEW__ITEM_ADD_REQUEST_SENT)
    .pipe(
      mergeMap(action => {
        const item = action.payload && action.payload.item ? action.payload.item : '';
        const backUrl = action.payload && action.payload.backUrl ? action.payload.backUrl : '';
        const url = generateUrlForWriteRequest();

        return concat(
          ajaxPost(url, item, getHttpWriteOptions())
            .pipe(
              map((ajaxResponse) => {
                return EnterpriseEmployeeActionFactory.newItemAddedSuccess(ajaxResponse.response, backUrl);
              }),
              catchError((ajaxError: AjaxError) => {
                return ActionsObservable.of(EnterpriseEmployeeActionFactory.newItemAddedError(ajaxError.response));
              })
            )
        );
      })
    );
};

export const editFetchItemEpic: Epic = (action$: ActionsObservable<FluxStandardAction<string, any, any> >) => {
  return action$.ofType(EnterpriseEmployeeActions.EDIT__ITEM_FETCH_REQUEST_SENT)
    .pipe(
      mergeMap(action => {
        const externalId = action.payload && action.payload.externalId ? action.payload.externalId : '';
        const url = Utils.getElementApiEndpoint(elementFullName) + '/' + externalId;

        return concat(
          ActionsObservable.of(EnterpriseEmployeeActionFactory.editItemFetching()),
          ajaxGet(url, getHttpReadOptions())
            .pipe(
              // delay(1000), // uncomment to see the state change when loading
              map((ajaxResponse) => {
                return EnterpriseEmployeeActionFactory.editItemFetchedSuccess(
                  ajaxResponse.response);
              }),
              catchError((ajaxError: AjaxError) => {
                return ActionsObservable.of(
                  EnterpriseEmployeeActionFactory.editItemFetchedError(ajaxError.response));
              })
            )
        );
      })
    );
};

export const editUpdateItemEpic: Epic = (action$: ActionsObservable<FluxStandardAction<string, any, any> >) => {
  return action$.ofType(EnterpriseEmployeeActions.EDIT__ITEM_UPDATE_REQUEST_SENT)
    .pipe(
      mergeMap(action => {
        const item = action.payload && action.payload.item ? action.payload.item : '';
        const backUrl = action.payload && action.payload.backUrl ? action.payload.backUrl : '';
        const url = generateUrlForWriteRequest(item.externalId);

        return concat(
          ajaxPut(url, item, getHttpWriteOptions())
            .pipe(
              map((ajaxResponse) => {
                return EnterpriseEmployeeActionFactory.editItemUpdatedSuccess(ajaxResponse.response, backUrl);
              }),
              catchError((ajaxError: AjaxError) => {
                return ActionsObservable.of(
                  EnterpriseEmployeeActionFactory.editItemUpdatedError(ajaxError.response));
              })
            )
        );
      })
    );
};

const generateUrlForGetItemsRequest = (queries: object): string => {
  let url = Utils.getElementApiEndpoint(elementFullName) + '?';

  Object.keys(queries).forEach(key => {
    if (queries[key] !== null) {
      switch (typeof queries[key]) {
        case 'string':
        case 'number':
        case 'boolean':
          url += key + '=' + queries[key] + '&';
          break;
        case 'object':
          url += key + '=' + queries[key].externalId + '&';
          break;
        default:
          url += key + '=' + queries[key] + '&';
      }
    }
  });
  url = url.slice(0, url.length - 1);

  return url;
};

const generateUrlForWriteRequest = (externalId: any = null): string => {
  let url = Utils.getElementApiEndpoint(elementFullName);

  if (externalId && typeof externalId === 'string') {
    url += '/' + externalId;
    return url;
  }

  // custom: delete multiple items
  if (externalId && Array.isArray(externalId)) {
    url += `/${(externalId as Array<string>).join(',')}` + '?multipleItems=true';
  }

  return url;
};

export const enterpriseEmployeeEpics = {
  listFetchItemsEpic,
  listFetchItemsByPageUrlEpic,
  listRemoveItemEpic,
  listRemoveItemsEpic,
  detailFetchItemEpic,
  detailRemoveItemEpic,
  newAddItemEpic,
  editFetchItemEpic,
  editUpdateItemEpic,
};

