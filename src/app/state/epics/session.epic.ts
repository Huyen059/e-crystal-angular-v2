import {ActionsObservable, Epic} from 'redux-observable';
import {FluxStandardAction} from 'flux-standard-action';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {ajaxPost, AjaxResponse} from 'rxjs/internal-compatibility';

import {erpTimesheetsProperties} from '../../pages/components/_properties/erpTimesheetsProperties';

import {LoginActionFactory, SessionActions} from '../actions/session.actions';

import {LoginOutputModel} from '../../login/_models/LoginOutputModel';
import {User} from '../../login/_models/User';

const sessionEpic: Epic = (action$: ActionsObservable<FluxStandardAction<string, any, any>>) => {
  return action$.ofType(SessionActions.USER_LOGGED_IN)
    .pipe(
      mergeMap(action => {
        const user = new User();
        user.username = action.payload.username;
        user.authorizationData = btoa(action.payload.username + ':' + action.payload.password);

        const httpWriteOptions = {Authorization: `Basic ${user.authorizationData}`, 'Content-Type': 'application/json'};
        const url = erpTimesheetsProperties.baseUrl + '/login';

        return ajaxPost(url, action.payload, httpWriteOptions)
          .pipe(
            map((ajaxResponse: AjaxResponse) => {
              const response = ajaxResponse.response as LoginOutputModel;
              if (response.authenticated) {
                user.profileName = response.profileName;
                return LoginActionFactory.userLoggedInSuccessAction(user);
              } else {
                return ActionsObservable.of(LoginActionFactory.userLoggedInErrorAction());
              }
            }),
            catchError(() => {
              return ActionsObservable.of(LoginActionFactory.userLoggedInErrorAction());
            })
          );
      })
    );
};

export const sessionEpics = [
  sessionEpic
];

