import {AppState} from './AppState';

import {combineReducers} from 'redux';
import {createEpicMiddleware} from 'redux-observable';

import {sessionReducer} from './reducers/session.reducer';
import {hierarchyReducer} from './reducers/hierarchy.reducer';

const appState = new AppState();
// Need to use object spread here because initial state should be a simple object,
// using initialState directly causes error
export const INITIAL_STATE = {...appState};

export const reducers = {
  hierarchy: hierarchyReducer,
  session: sessionReducer,
};

export const epicMiddleware = createEpicMiddleware();

export const rootReducer = combineReducers(reducers);
