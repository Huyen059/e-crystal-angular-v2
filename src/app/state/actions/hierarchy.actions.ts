export enum HierarchyActions {
  LEVEL_ADDED = 'hierarchy/LEVEL_ADDED',
  LAST_LEVEL_REMOVED = 'hierarchy/LAST_LEVEL_REMOVED',
  ALL_LEVELS_REMOVED = 'hierarchy/ALL_LEVELS_REMOVED'
}

export class HierarchyActionFactory {
  static levelAdded = (elementFullName: string, value: any, childrenFullNames: string[]) => {
    let action;
    action = {
      type: HierarchyActions.LEVEL_ADDED,
      payload: {
        elementFullName,
        value,
        childrenFullNames
      }
    };

    return action;
  }

  static lastLevelRemoved = () => {
    let action;
    action = {
      type: HierarchyActions.LAST_LEVEL_REMOVED,
    };

    return action;
  }

  static allLevelsRemoved = () => {
    let action;
    action = {
      type: HierarchyActions.ALL_LEVELS_REMOVED,
    };

    return action;
  }
}
