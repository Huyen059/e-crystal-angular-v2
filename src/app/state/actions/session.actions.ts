import {LoginPostInputModel} from '../../login/_models/LoginPostInputModel';
import {User} from '../../login/_models/User';

export enum SessionActions {
  USER_LOGGED_IN = 'session/USER_LOGGED_IN',
  USER_LOGGED_IN_SUCCESS = 'session/USER_LOGGED_IN_SUCCESS',
  USER_LOGGED_IN_ERROR = 'session/USER_LOGGED_IN_ERROR',
  USER_LOGGED_OUT = 'session/USER_LOGGED_OUT',
}

export class LoginActionFactory {
  static userLoggedInAction = (userData: LoginPostInputModel) => {
    let action;
    action = {
      type: SessionActions.USER_LOGGED_IN,
      payload: userData
    };

    return action;
  }

  static userLoggedInSuccessAction = (user: User) => {
    let action;
    action = {
      type: SessionActions.USER_LOGGED_IN_SUCCESS,
      payload: user
    };

    return action;
  }

  static userLoggedInErrorAction = () => {
    let action;
    action = {
      type: SessionActions.USER_LOGGED_IN_ERROR,
      payload: 'authError',
    };

    return action;
  }


  static userLoggedOutAction = () => {
    let action;
    action = {
      type: SessionActions.USER_LOGGED_OUT
    };

    return action;
  }
}
