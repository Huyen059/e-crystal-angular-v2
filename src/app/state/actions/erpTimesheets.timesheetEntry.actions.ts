export enum ErpTimesheetsTimesheetEntryActions {
  LIST__ITEMS_FETCH_REQUEST_SENT = 'erpTimesheets.timesheetEntry__list/ITEMS_FETCH_REQUEST_SENT',
  LIST__ITEMS_FETCH_BY_PAGE_URL_REQUEST_SENT = 'erpTimesheets.timesheetEntry__list/ITEMS_FETCH_BY_PAGE_URL_REQUEST_SENT',
  LIST__ITEMS_FETCHING = 'erpTimesheets.timesheetEntry__list/ITEMS_FETCHING',
  LIST__ITEMS_FETCHED_SUCCESS = 'erpTimesheets.timesheetEntry__list/ITEMS_FETCHED_SUCCESS',
  LIST__ITEMS_FETCHED_ERROR = 'erpTimesheets.timesheetEntry__list/ITEMS_FETCHED_ERROR',
  LIST__ITEM_REMOVE_REQUEST_SENT = 'erpTimesheets.timesheetEntry__list/ITEM_REMOVE_REQUEST_SENT',
  LIST__ITEM_REMOVED_SUCCESS = 'erpTimesheets.timesheetEntry__list/ITEM_REMOVED_SUCCESS',
  LIST__ITEM_REMOVED_ERROR = 'erpTimesheets.timesheetEntry__list/ITEM_REMOVED_ERROR',
  LIST__ITEMS_REMOVE_REQUEST_SENT = 'erpTimesheets.timesheetEntry__list/ITEMS_REMOVE_REQUEST_SENT',
  LIST__ITEMS_REMOVED_SUCCESS = 'erpTimesheets.timesheetEntry__list/ITEMS_REMOVED_SUCCESS',
  LIST__ITEMS_REMOVED_ERROR = 'erpTimesheets.timesheetEntry__list/ITEMS_REMOVED_ERROR',
  LIST__FETCH_FILTERS_SAVED = 'erpTimesheets.timesheetEntry__list/FETCH_FILTERS_SAVED',

  DETAIL__ITEM_FETCH_REQUEST_SENT = 'erpTimesheets.timesheetEntry__detail/ITEM_FETCH_REQUEST_SENT',
  DETAIL__ITEM_FETCHING = 'erpTimesheets.timesheetEntry__detail/ITEM_FETCHING',
  DETAIL__ITEM_FETCHED_SUCCESS = 'erpTimesheets.timesheetEntry__detail/ITEM_FETCHED_SUCCESS',
  DETAIL__ITEM_FETCHED_ERROR = 'erpTimesheets.timesheetEntry__detail/ITEM_FETCHED_ERROR',
  DETAIL__ITEM_REMOVE_REQUEST_SENT = 'erpTimesheets.timesheetEntry__detail/ITEM_DETAIL_REMOVE_REQUEST_SENT',
  DETAIL__ITEM_REMOVED_SUCCESS = 'erpTimesheets.timesheetEntry__detail/ITEM_DETAIL_REMOVED_SUCCESS',
  DETAIL__ITEM_REMOVED_ERROR = 'erpTimesheets.timesheetEntry__detail/ITEM_DETAIL_REMOVED_ERROR',

  NEW__ITEM_ADD_REQUEST_SENT = 'erpTimesheets.timesheetEntry__new/ITEM_ADD_REQUEST_SENT',
  NEW__ITEM_ADDED_SUCCESS = 'erpTimesheets.timesheetEntry__new/ITEM_ADDED_SUCCESS',
  NEW__ITEM_ADDED_ERROR = 'erpTimesheets.timesheetEntry__new/ITEM_ADDED_ERROR',

  EDIT__ITEM_FETCH_REQUEST_SENT = 'erpTimesheets.timesheetEntry__edit/ITEM_FETCH_REQUEST_SENT',
  EDIT__ITEM_FETCHING = 'erpTimesheets.timesheetEntry__edit/ITEM_FETCHING',
  EDIT__ITEM_FETCHED_SUCCESS = 'erpTimesheets.timesheetEntry__edit/ITEM_FETCHED_SUCCESS',
  EDIT__ITEM_FETCHED_ERROR = 'erpTimesheets.timesheetEntry__edit/ITEM_FETCHED_ERROR',
  EDIT__ITEM_UPDATE_REQUEST_SENT = 'erpTimesheets.timesheetEntry__edit/ITEM_UPDATE_REQUEST_SENT',
  EDIT__ITEM_UPDATED_SUCCESS = 'erpTimesheets.timesheetEntry__edit/ITEM_UPDATED_SUCCESS',
  EDIT__ITEM_UPDATED_ERROR = 'erpTimesheets.timesheetEntry__edit/ITEM_UPDATED_ERROR',

  DATA_REMOVED = 'erpTimesheets.timesheetEntry/DATA_REMOVED',
  FETCH_QUERIES_SAVED = 'erpTimesheets.timesheetEntry/FETCH_QUERIES_SAVED',
  ALERT_REMOVED = 'erpTimesheets.timesheetEntry/ALERT_REMOVED',
  REQUEST_DATA_REMOVED = 'erpTimesheets.timesheetEntry/REQUEST_DATA_REMOVED',
}

export class ErpTimesheetsTimesheetEntryActionFactory {

  static listItemsFetchRequestSent =
    (fetchItemConditions: object) => {
      let action;
      action = {
        type: ErpTimesheetsTimesheetEntryActions.LIST__ITEMS_FETCH_REQUEST_SENT,
        payload: {
          fetchItemConditions,
        }
      };

      return action;
    }

  static listItemsFetchByPageUrlRequestSent = (pageUrl: string) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.LIST__ITEMS_FETCH_BY_PAGE_URL_REQUEST_SENT,
      payload: {
        pageUrl,
      }
    };

    return action;
  }

  static listItemsFetching = () => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.LIST__ITEMS_FETCHING,
    };

    return action;
  }

  static listItemsFetchedSuccess = (data: any) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.LIST__ITEMS_FETCHED_SUCCESS,
      payload: {
        data,
      },
    };

    return action;
  }

  static listItemsFetchedError = (error) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.LIST__ITEMS_FETCHED_ERROR,
      payload: {
        error
      },
    };

    return action;
  }

  static listItemRemoveRequestSent = (itemExternalId: string, backUrl: string) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.LIST__ITEM_REMOVE_REQUEST_SENT,
      payload: {
        itemExternalId,
        backUrl
      }
    };

    return action;
  }

  static listItemRemovedSuccess = (data: any, backUrl: string) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.LIST__ITEM_REMOVED_SUCCESS,
      payload: {
        data,
        backUrl
      },
    };

    return action;
  }

  static listItemRemovedError = (error: any) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.LIST__ITEM_REMOVED_ERROR,
      payload: {
        error,
      },
    };

    return action;
  }

  static listItemsRemoveRequestSent = (externalIds: any[], backUrl: string) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.LIST__ITEMS_REMOVE_REQUEST_SENT,
      payload: {
        externalIds,
        backUrl
      }
    };

    return action;
  }

  static listItemsRemovedSuccess = (data: any, backUrl: string) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.LIST__ITEMS_REMOVED_SUCCESS,
      payload: {
        data,
        backUrl
      },
    };

    return action;
  }

  static listItemsRemovedError = (error: any) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.LIST__ITEMS_REMOVED_ERROR,
      payload: {
        error
      },
    };

    return action;
  }

  static listFetchFiltersSaved =
    (filterData: any) => {
      let action;
      action = {
        type: ErpTimesheetsTimesheetEntryActions.LIST__FETCH_FILTERS_SAVED,
        payload: {
          filterData,
        }
      };

      return action;
    }

  static detailItemFetchRequestSent =
    (externalId: any) => {
      let action;
      action = {
        type: ErpTimesheetsTimesheetEntryActions.DETAIL__ITEM_FETCH_REQUEST_SENT,
        payload: {
          externalId
        }
      };

      return action;
    }

  static detailItemFetching = () => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.DETAIL__ITEM_FETCHING,
    };

    return action;
  }

  static detailItemFetchedSuccess = (data: any) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.DETAIL__ITEM_FETCHED_SUCCESS,
      payload: {
        data,
      },
    };

    return action;
  }


  static detailItemFetchedError = (error) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.DETAIL__ITEM_FETCHED_ERROR,
      payload: {
        error
      },
    };

    return action;
  }

  static detailItemRemoveRequestSent = (itemExternalId: string, backUrl: string) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.DETAIL__ITEM_REMOVE_REQUEST_SENT,
      payload: {
        itemExternalId,
        backUrl
      }
    };

    return action;
  }

  static detailItemRemovedSuccess = (data: any, backUrl: string) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.DETAIL__ITEM_REMOVED_SUCCESS,
      payload: {
        data,
        backUrl
      },
    };

    return action;
  }

  static detailItemRemovedError = (error: any) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.DETAIL__ITEM_REMOVED_ERROR,
      payload: {
        error,
      },
    };

    return action;
  }

  static newItemAddRequestSent = (item: any, backUrl: string) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.NEW__ITEM_ADD_REQUEST_SENT,
      payload: {
        item,
        backUrl
      }
    };

    return action;
  }

  static newItemAddedSuccess = (data: any, backUrl: string) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.NEW__ITEM_ADDED_SUCCESS,
      payload: {
        data,
        backUrl
      },
    };

    return action;
  }

  static newItemAddedError = (error: any) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.NEW__ITEM_ADDED_ERROR,
      payload: {
        error
      },
    };

    return action;
  }

  static editItemFetchRequestSent =
    (externalId: any) => {
      let action;
      action = {
        type: ErpTimesheetsTimesheetEntryActions.EDIT__ITEM_FETCH_REQUEST_SENT,
        payload: {
          externalId
        }
      };

      return action;
    }

  static editItemFetching = () => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.EDIT__ITEM_FETCHING,
    };

    return action;
  }

  static editItemFetchedSuccess = (data: any) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.EDIT__ITEM_FETCHED_SUCCESS,
      payload: {
        data,
      },
    };

    return action;
  }


  static editItemFetchedError = (error) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.EDIT__ITEM_FETCHED_ERROR,
      payload: {
        error
      },
    };

    return action;
  }

  static editItemUpdateRequestSent = (item: any, backUrl: string) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.EDIT__ITEM_UPDATE_REQUEST_SENT,
      payload: {
        item,
        backUrl
      }
    };

    return action;
  }

  static editItemUpdatedSuccess = (data: any, backUrl: string) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.EDIT__ITEM_UPDATED_SUCCESS,
      payload: {
        data,
        backUrl
      },
    };

    return action;
  }

  static editItemUpdatedError = (error: any) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.EDIT__ITEM_UPDATED_ERROR,
      payload: {
        error
      },
    };

    return action;
  }

  static dataRemoved = () => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.DATA_REMOVED,
    };

    return action;
  }

  static fetchQueriesSaved = (queries: object) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.FETCH_QUERIES_SAVED,
      payload: {
        queries,
      }
    };

    return action;
  }

  static alertRemoved = () => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.ALERT_REMOVED,
    };

    return action;
  }

  static requestDataRemoved = (path: string) => {
    let action;
    action = {
      type: ErpTimesheetsTimesheetEntryActions.REQUEST_DATA_REMOVED,
      payload: {
        path,
      }
    };

    return action;
  }
}
