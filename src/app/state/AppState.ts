import {SessionSlice} from './slices/session.slice';
import {HierarchySlice} from './slices/hierarchy.slice';

export class AppState {
  hierarchy: HierarchySlice = {currentLevel: 0};
  session = new SessionSlice();
}
