import {Reducer} from 'redux';
import {FluxStandardAction} from 'flux-standard-action';
import {tassign} from 'tassign';
import {SessionSlice} from '../slices/session.slice';
import {SessionActions} from '../actions/session.actions';
import {User} from '../../login/_models/User';
import {AppState} from '../AppState';

export const sessionReducer: Reducer<SessionSlice> =
  (state: SessionSlice = new SessionSlice(), action: FluxStandardAction<string, any, any>): SessionSlice => {
    switch (action.type) {
      case SessionActions.USER_LOGGED_IN_SUCCESS:
        const user = action.payload as User;
        sessionStorage.setItem('currentUser', JSON.stringify(user));
        return tassign(state,
          {
            user,
            error: null
          });
      case SessionActions.USER_LOGGED_IN_ERROR:
        return tassign(state,
          {
            error: action.payload as string
          });
      case SessionActions.USER_LOGGED_OUT:
        sessionStorage.removeItem('currentUser');
        return tassign(state,
          {
            user: null,
            error: null
          });
    }
    return state;
  };

export const getAuthorizationData = (state: AppState): string => {
  return state.session.user.authorizationData;
};

export const getHttpReadOptions = (state?: AppState) => {
  if (state) {
    return {
      headers: {
        Authorization: `Basic ${getAuthorizationData(state)}`
      }
    };
  }

  const authorizationData = (JSON.parse(sessionStorage.getItem('currentUser')) as User).authorizationData;
  return {
    Authorization: `Basic ${authorizationData}`
  };
};

export const getHttpWriteOptions = (state?: AppState) => {
  if (state) {
    return {
      headers: {
        Authorization : `Basic ${getAuthorizationData(state)}`,
        'Content-Type': 'application/json'
      }
    };
  }

  const authorizationData = (JSON.parse(sessionStorage.getItem('currentUser')) as User).authorizationData;
  return {
    Authorization : `Basic ${authorizationData}`,
    'Content-Type': 'application/json'
  };
};
