import {FluxStandardAction} from 'flux-standard-action';
import {Reducer} from 'redux';
import {HierarchyActions} from '../actions/hierarchy.actions';
import {HierarchyLevelModel, HierarchySlice} from '../slices/hierarchy.slice';

export const hierarchyReducer: Reducer<HierarchySlice> =
  (state: HierarchySlice = {currentLevel: 0}, action: FluxStandardAction<string, any, any>): HierarchySlice => {
    const newState = {...state};
    const nextLevel = newState.currentLevel + 1;

    switch (action.type) {
      case HierarchyActions.LEVEL_ADDED:
        newState[nextLevel] = new HierarchyLevelModel(
          action.payload.elementFullName,
          action.payload.value,
          action.payload.childrenFullNames);
        newState.currentLevel = nextLevel;
        return newState;

      case HierarchyActions.LAST_LEVEL_REMOVED:
        delete newState[newState.currentLevel];
        newState.currentLevel--;
        return newState;

      case HierarchyActions.ALL_LEVELS_REMOVED:
        return {currentLevel: 0};
    }

    return newState;
  };
