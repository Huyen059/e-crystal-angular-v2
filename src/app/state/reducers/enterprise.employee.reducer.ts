import {FluxStandardAction} from 'flux-standard-action';
import {Reducer} from 'redux';
import {EnterpriseEmployeeSlicePaths as paths} from '../slices/enterprise.employee.slice';
import {EnterpriseEmployeeActions} from '../actions/enterprise.employee.actions';
import {AlertSettings} from '../../pages/ui/alert/_models/AlertSettings';

export const enterpriseEmployeeReducer: Reducer<object> =
  (state: object = {}, action: FluxStandardAction<string, any, any>): object => {
    const newState = {...state};

    switch (action.type) {
      case EnterpriseEmployeeActions.FETCH_QUERIES_SAVED:
        newState[paths.queries] = {};
        Object.keys(action.payload.queries).forEach(key => {
          newState[paths.queries][key] = action.payload.queries[key];
        });
        return newState;

      case EnterpriseEmployeeActions.LIST__ITEMS_FETCHING:
        newState[paths.list_requests_GET] = {
          loading: true,
        };
        return newState;

      case EnterpriseEmployeeActions.LIST__ITEMS_FETCHED_SUCCESS:
        newState[paths.list_requests_GET] = {
          loading: false,
          success: true,
          employees: action.payload.data._embedded.employees,
          page: action.payload.data._page,
          links: action.payload.data._links,
        };
        newState[paths.list_pageUrl] = action.payload.data._links.self.href;
        return newState;

      case EnterpriseEmployeeActions.LIST__ITEMS_FETCHED_ERROR:
        newState[paths.list_requests_GET] = {
          loading: false,
          success: false,
          error: action.payload.error,
        };
        newState[paths.alert] = {
          type: AlertSettings.ERROR,
          messageTranslationKey: action.payload.error.title
        };
        return newState;

      case EnterpriseEmployeeActions.LIST__ITEM_REMOVED_SUCCESS:
        newState[paths.list_requests_DELETE] = {
          success: true,
          result: action.payload.data,
          backUrl: action.payload.backUrl,
        };
        newState[paths.alert] = {
          type: AlertSettings.SUCCESS,
          messageTranslationKey: 'Item deleted'
        };
        return newState;

      case EnterpriseEmployeeActions.LIST__ITEM_REMOVED_ERROR:
        newState[paths.list_requests_DELETE] = {
          success: false,
          error: action.payload.error,
        };
        newState[paths.alert] = {
          type: AlertSettings.ERROR,
          messageTranslationKey: action.payload.error.title
        };
        return newState;

      case EnterpriseEmployeeActions.LIST__ITEMS_REMOVED_SUCCESS:
        newState[paths.list_requests_DELETE] = {
          success: true,
          result: action.payload.data,
          backUrl: action.payload.backUrl,
        };
        newState[paths.alert] = {
          type: AlertSettings.SUCCESS,
          messageTranslationKey: 'Items deleted'
        };
        return newState;

      case EnterpriseEmployeeActions.LIST__ITEMS_REMOVED_ERROR:
        newState[paths.list_requests_DELETE] = {
          success: false,
          error: action.payload.error,
        };
        newState[paths.alert] = {
          type: AlertSettings.ERROR,
          messageTranslationKey: action.payload.error.title
        };
        return newState;

      case EnterpriseEmployeeActions.LIST__FETCH_FILTERS_SAVED:
        newState[paths.filterData] = action.payload.filterData;
        return newState;

      case EnterpriseEmployeeActions.DETAIL__ITEM_FETCHING:
        newState[paths.detail_requests_GET] = {
          loading: true,
        };
        return newState;

      case EnterpriseEmployeeActions.DETAIL__ITEM_FETCHED_SUCCESS:
        newState[paths.detail_requests_GET] = {
          loading: false,
          success: true,
          employee: action.payload.data,
        };
        return newState;

      case EnterpriseEmployeeActions.DETAIL__ITEM_FETCHED_ERROR:
        newState[paths.detail_requests_GET] = {
          loading: false,
          success: false,
          error: action.payload.error,
        };
        newState[paths.alert] = {
          type: AlertSettings.ERROR,
          messageTranslationKey: action.payload.error.title
        };
        return newState;

      case EnterpriseEmployeeActions.DETAIL__ITEM_REMOVED_SUCCESS:
        newState[paths.detail_requests_DELETE] = {
          success: true,
          result: action.payload.data,
          backUrl: action.payload.backUrl,
        };
        newState[paths.alert] = {
          type: AlertSettings.SUCCESS,
          messageTranslationKey: 'Item deleted'
        };
        return newState;

      case EnterpriseEmployeeActions.DETAIL__ITEM_REMOVED_ERROR:
        newState[paths.detail_requests_DELETE] = {
          success: false,
          error: action.payload.error,
        };
        newState[paths.alert] = {
          type: AlertSettings.ERROR,
          messageTranslationKey: action.payload.error.title
        };
        return newState;

      case EnterpriseEmployeeActions.NEW__ITEM_ADDED_SUCCESS:
        newState[paths.new_requests_CREATE] = {
          success: true,
          result: action.payload.data,
          backUrl: action.payload.backUrl,
        };
        newState[paths.alert] = {
          type: AlertSettings.SUCCESS,
          messageTranslationKey: 'Item created'
        };
        return newState;

      case EnterpriseEmployeeActions.NEW__ITEM_ADDED_ERROR:
        newState[paths.new_requests_CREATE] = {
          success: false,
          error: action.payload.error,
        };
        newState[paths.alert] = {
          type: AlertSettings.ERROR,
          messageTranslationKey: action.payload.error.title
        };
        return newState;

      case EnterpriseEmployeeActions.EDIT__ITEM_FETCHING:
        newState[paths.edit_requests_GET] = {
          loading: true,
        };
        return newState;

      case EnterpriseEmployeeActions.EDIT__ITEM_FETCHED_SUCCESS:
        newState[paths.edit_requests_GET] = {
          loading: false,
          success: true,
          employee: action.payload.data,
        };
        return newState;

      case EnterpriseEmployeeActions.EDIT__ITEM_FETCHED_ERROR:
        newState[paths.edit_requests_GET] = {
          loading: false,
          success: false,
          error: action.payload.error,
        };
        newState[paths.alert] = {
          type: AlertSettings.ERROR,
          messageTranslationKey: action.payload.error.title
        };
        return newState;

      case EnterpriseEmployeeActions.EDIT__ITEM_UPDATED_SUCCESS:
        newState[paths.edit_requests_UPDATE] = {
          success: true,
          result: action.payload.data,
          backUrl: action.payload.backUrl,
        };
        newState[paths.alert] = {
          type: AlertSettings.SUCCESS,
          messageTranslationKey: 'Item updated'
        };
        return newState;

      case EnterpriseEmployeeActions.EDIT__ITEM_UPDATED_ERROR:
        newState[paths.edit_requests_UPDATE] = {
          success: false,
          error: action.payload.error,
        };
        newState[paths.alert] = {
          type: AlertSettings.ERROR,
          messageTranslationKey: action.payload.error.title
        };
        return newState;

      case EnterpriseEmployeeActions.DATA_REMOVED:
        return {};

      case EnterpriseEmployeeActions.ALERT_REMOVED:
        if (newState[paths.alert]) {
          delete newState[paths.alert];
        }
        return newState;

      case EnterpriseEmployeeActions.REQUEST_DATA_REMOVED:
        if (newState[action.payload.path]) {
          delete newState[action.payload.path];
        }
        return newState;
    }

    return newState;
  };
