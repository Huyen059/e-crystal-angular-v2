export class HierarchyLevelModel {
  elementFullName: string;
  value: any;
  childrenFullNames: string[];

  constructor(elementFullName: string, value: any, childrenFullNames: string[]) {
    this.elementFullName = elementFullName;
    this.value = value;
    this.childrenFullNames = childrenFullNames;
  }
}

export type HierarchySlice = {
  currentLevel: number,
  [key: number]: HierarchyLevelModel;
};
