import {User} from '../../login/_models/User';

export class SessionSlice {
  user: User = null;
  error: string = null;

  constructor() {
    if (sessionStorage.getItem('currentUser')) {
      this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    }
  }
}
