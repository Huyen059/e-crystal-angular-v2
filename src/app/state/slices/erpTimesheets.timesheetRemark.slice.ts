export enum ErpTimesheetsTimesheetRemarkSlicePaths {
  list_requests_GET = 'list.requests.GET',
  list_requests_DELETE = 'list.requests.DELETE',
  list_pageUrl = 'list.pageUrl',
  detail_requests_GET = 'detail.requests.GET',
  detail_requests_DELETE = 'detail.requests.DELETE',
  new_requests_CREATE = 'new.requests.CREATE',
  edit_requests_GET = 'edit.requests.GET',
  edit_requests_UPDATE = 'edit.requests.UPDATE',
  queries = 'queries',
  filterData = 'filterData',
  alert = 'alert',
}

