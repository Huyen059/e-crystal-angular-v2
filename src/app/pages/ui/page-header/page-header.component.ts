import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../../login/_models/User';
import {NavigationService} from '../../components/_services/navigation.service';
import {Languages} from '../../components/_models/Languages';
import {Language} from '../../components/_models/Language';
import {Utils} from '../../components/_utils/Utils';
import {LoginActionFactory} from '../../../state/actions/session.actions';
import {NgRedux, select} from '@angular-redux/store';
import {AppState} from '../../../state/AppState';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.css']
})
export class PageHeaderComponent implements OnInit, AfterViewInit, OnDestroy {
  private router: Router;
  pageTitle: string;
  breadcrumbs: { label: string, url: string }[];
  currentUser: User;
  isUserMenuDisplayed = false;

  @select(['session', 'user']) user$: Observable<User>;
  private userSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private navigationService: NavigationService,
    private ngRedux: NgRedux<AppState>,
  ) {
    this.navigationService.currentRouter$.subscribe(
      router => {
        this.router = router;
        this.makeBreadcrumbs();
      }
    );
  }

  ngOnInit(): void {
    this.userSubscription = this.user$.subscribe(user => {
      if (user === null) {
        this.router.navigate(['/login']);
        return;
      }
      this.currentUser = user;
    });
  }

  ngAfterViewInit(): void {
    this.userAvatarHandler();
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

  private makeBreadcrumbs(): void {
    this.breadcrumbs = [];
    let currentRoute = this.router.routerState.root.firstChild;
    let name = '';
    let url = '';
    while (currentRoute.component === undefined) {
      currentRoute = currentRoute.firstChild;
    }

    currentRoute.data.subscribe(value => this.pageTitle = value.name);

    while (currentRoute) {
      const route = currentRoute.firstChild;

      currentRoute.data.subscribe(value => name = value.name);
      url = this.getUrlForCurrentRoot(currentRoute);
      this.addBreadcrumbItem(name, url);

      currentRoute = route;
    }
  }

  private getUrlForCurrentRoot(currentRoute: ActivatedRoute): string {
    let url = '';
    const pathFromRoot = currentRoute.pathFromRoot;
    for (const activatedRoute of pathFromRoot) {
      activatedRoute.url.subscribe(
        value => {
          if (value && value.length > 0 && value[0].path) {
            url += '/' + value[0].path;
          }
        }
      );
    }
    return url;
  }

  private addBreadcrumbItem(name: string, url: string): void {
    this.breadcrumbs.push(
      {
        label: name,
        url
      }
    );
  }

  private userAvatarHandler(): void {
    const userAvatar = document.querySelector('.app-header-user-avatar-component') as HTMLElement;
    const userMenu = document.querySelector('.app-header-user-menu') as HTMLElement;
    userAvatar.addEventListener('click', () => {
      this.isUserMenuDisplayed = !this.isUserMenuDisplayed;
      if (this.isUserMenuDisplayed) {
        userMenu.style.display = 'block';
      } else {
        userMenu.style.display = 'none';
      }
    });

    window.onclick = (e) => {
      if (!userMenu.contains(e.target) && !userAvatar.contains(e.target)) {
        this.isUserMenuDisplayed = false;
        userMenu.style.display = 'none';
      }
    };
  }

  logout(): void {
    this.ngRedux.dispatch(LoginActionFactory.userLoggedOutAction());
  }

  goToLoginPage(): void {
    this.router.navigate(['/login']);
  }

  /* ----- CHOOSE LANGUAGE ----- */

  get languages(): Language[] {
    return Languages.getLanguages();
  }

  isLanguageChosen(language: Language): boolean {
    return localStorage.getItem('language') === language.name;
  }

  chooseLanguage(language: Language): void {
    localStorage.setItem('language', language.name);
  }


  /* ----- MISC ----- */

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
