import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Utils} from '../../components/_utils/Utils';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  @Input() titleTranslationKey: string;
  @Input() messageTranslationKey: string;
  @Output() buttonClickEvent = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  confirm(): void {
    this.buttonClickEvent.emit(true);
  }

  cancel(): void {
    this.buttonClickEvent.emit(false);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
