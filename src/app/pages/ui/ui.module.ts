import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {NavComponent} from './nav/nav.component';
import {PageHeaderComponent} from './page-header/page-header.component';
import {PageFooterComponent} from './page-footer/page-footer.component';
import {RouterModule} from '@angular/router';
import {AlertModule} from './alert/alert.module';
import {TableControlComponent} from './table-control/table-control.component';
import {PaginationComponent} from './pagination/pagination.component';
import {SortComponent} from './sort/sort.component';
import {TabControlComponent} from './tab-control/tab-control.component';
import {DialogComponent} from './dialog/dialog.component';
import {ButtonComponent} from './button/button.component';


@NgModule({
  declarations: [
    HeaderComponent,
    NavComponent,
    PageHeaderComponent,
    PageFooterComponent,
    TableControlComponent,
    PaginationComponent,
    SortComponent,
    TabControlComponent,
    DialogComponent,
    ButtonComponent,
  ],
  exports: [
    HeaderComponent,
    PageHeaderComponent,
    PageFooterComponent,
    TableControlComponent,
    PaginationComponent,
    SortComponent,
    TabControlComponent,
    DialogComponent,
    ButtonComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    AlertModule
  ]
})
export class UiModule {
}
