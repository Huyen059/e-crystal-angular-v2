import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PageModel} from '../../components/_models/PageModel';
import {Utils} from '../../components/_utils/Utils';
import {LinksModel} from '../../components/_models/LinksModel';
import {PaginationIn} from './PaginationIn';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  @Input() paginationIn: PaginationIn;
  @Input() page: PageModel;
  @Input() links: LinksModel;

  @Output() changePageEvent = new EventEmitter<string>();

  displayItemRange;

  constructor() { }

  ngOnInit(): void {
    this.displayItemRange = this.paginationIn === PaginationIn.ELEMENT_LIST;
  }

  displayFirstPageOfList(): void {
    this.changePageEvent.emit(this.links.first.href);
  }

  displayPreviousPageOfList(): void {
    if (this.links.prev) {
      this.changePageEvent.emit(this.links.prev.href);
    }
  }

  displayNextPageOfList(): void {
    if (this.links.next) {
      this.changePageEvent.emit(this.links.next.href);
    }
  }

  displayLastPageOfList(): void {
    this.changePageEvent.emit(this.links.last.href);
  }

  /* For displaying number of items below the list:start */
  get firstItemOfCurrentPage(): number {
    return (this.page.number - 1) * this.page.size + 1;
  }

  get lastItemOfCurrentPage(): number {
    if (this.page.number < this.page.totalPages) {
      return this.page.number * this.page.size;
    }

    return this.page.totalElements;
  }
  /* For displaying number of items below the list:end */

  /* ----- MISC ----- */

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
