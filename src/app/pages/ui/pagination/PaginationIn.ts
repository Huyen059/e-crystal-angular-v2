export enum PaginationIn {
  ELEMENT_LIST = 'element-list',
  ELEMENT_FILTER_LIST = 'element-filter-list',
}
