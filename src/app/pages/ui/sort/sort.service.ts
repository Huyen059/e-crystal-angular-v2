import { Injectable } from '@angular/core';
import {SortData} from './SortData';

@Injectable({
  providedIn: 'root'
})
export class SortService {

  allSortData = new Map<string, SortData>();

  constructor() { }

  add(elementFullName: string): void {
    if (!this.allSortData.has(elementFullName)) {
      const data = new SortData();
      this.allSortData.set(elementFullName, data);
    }
  }
}
