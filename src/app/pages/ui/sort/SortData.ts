export class SortData {
  constructor(
    public sortBy: string = null,
    public isAscending: boolean = null,
  ) {
  }

  clear(): void {
    this.sortBy = null;
    this.isAscending = null;
  }

  switchSortDirection(): void {
    this.isAscending = !this.isAscending;
  }

  setSortField(sortField: string): void {
    this.sortBy = sortField;
    this.isAscending = true;
  }
}
