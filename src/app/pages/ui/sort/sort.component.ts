import {Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {SortData} from './SortData';
import {SortService} from './sort.service';
import {Utils} from '../../components/_utils/Utils';

@Component({
  selector: 'app-sort',
  templateUrl: './sort.component.html',
  styleUrls: ['./sort.component.css']
})
export class SortComponent implements OnInit {

  @Input() elementFullName: string;
  @Input() sortField: string;

  @Output() changeSortDataEvent = new EventEmitter<boolean>();

  constructor(
    private sortService: SortService,
  ) { }

  ngOnInit(): void {
  }

  get sortData(): SortData {
    return this.sortService.allSortData.get(this.elementFullName);
  }

  /* ----- SORT ----- */

  getSortCssClass(sortField: string): string {
    if (sortField !== this.sortData.sortBy) {
      return 'fas fa-sort';
    }

    if (this.sortData.isAscending) {
      return 'fas fa-sort-down';
    }

    return 'fas fa-sort-up';
  }

  enableSort(sortField: string): void {
    if (this.sortData.sortBy === sortField) {
      this.sortData.switchSortDirection();
    } else {
      this.sortData.setSortField(sortField);
    }

    this.sort();
  }

  private sort(): void {
    this.changeSortDataEvent.emit(true);
  }


  clearSort(): void {
    this.sortData.clear();
    this.changeSortDataEvent.emit(true);
  }

  /* ----- MISC ----- */

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
