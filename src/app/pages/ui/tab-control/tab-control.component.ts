import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-tab-control',
  templateUrl: './tab-control.component.html',
  styleUrls: ['./tab-control.component.css']
})
export class TabControlComponent implements OnInit {
  @Input() tabContainerCssClassName: string;
  @Input() tabLinksContainerCssClassName: string;
  @Input() tabLinksCssClassName: string;

  private firstTabPosition = 1;
  private tabWidths: number[] = [];

  constructor() { }

  ngOnInit(): void {
    this.setTabsWidth();
    this.showScrollIcons();
  }

  private setTabsWidth(): void {
    const tabLinks = document.getElementsByClassName(this.tabLinksCssClassName) as HTMLCollectionOf<HTMLElement>;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < tabLinks.length; i++) {
      tabLinks[i].style.width = tabLinks[i].offsetWidth + 'px';
      this.tabWidths.push(tabLinks[i].offsetWidth);
    }
  }

  private showScrollIcons(): void {
    const tab = document.getElementsByClassName(this.tabContainerCssClassName)[0] as HTMLElement;
    const tabLinksContainer = document.querySelector('.' + this.tabLinksContainerCssClassName) as HTMLElement;
    const arrows = document.getElementsByClassName('ns-tabs__arrow') as HTMLCollectionOf<HTMLElement>;
    if (tabLinksContainer.offsetWidth < tab.offsetWidth) {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < arrows.length; i++) {
        arrows[i].style.display = 'none';
      }
    } else {
      this.updateArrowsDisplay();
    }
  }

  private updateArrowsDisplay(): void {
    const leftArrow = document.querySelector('.ns-tabs__left-arrow') as HTMLElement;
    const rightArrow = document.querySelector('.ns-tabs__right-arrow') as HTMLElement;
    const tabLinks = document.getElementsByClassName(this.tabLinksCssClassName) as HTMLCollectionOf<HTMLElement>;
    if (this.firstTabPosition === 1) {
      leftArrow.style.display = 'none';
    } else if (this.firstTabPosition === tabLinks.length) {
      rightArrow.style.display = 'none';
    } else {
      leftArrow.style.display = 'block';
      rightArrow.style.display = 'block';
    }
  }

  showNextTab(): void {
    const tabButtons = document.querySelector('.ns-tabs__container-buttons') as HTMLElement;
    this.firstTabPosition++;
    tabButtons.style.marginLeft = '-' + this.calculateMargin();
    this.updateArrowsDisplay();
  }

  showPreviousTab(): void {
    const tabButtons = document.querySelector('.ns-tabs__container-buttons') as HTMLElement;
    this.firstTabPosition--;
    tabButtons.style.marginLeft = '-' + this.calculateMargin();
    this.updateArrowsDisplay();
  }

  private calculateMargin(): string {
    if (this.firstTabPosition === 1) {
      return '0';
    }

    let margin = 0;

    for (let i = 0; i < this.tabWidths.length; i++) {
      if (i > 0 && i < this.firstTabPosition) {
        margin += this.tabWidths[i - 1];
      }
    }

    return margin + 'px';
  }
}
