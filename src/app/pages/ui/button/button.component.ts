import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Utils} from '../../components/_utils/Utils';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {
  @Input() showButtonCondition = true;
  @Input() buttonType: string;
  @Input() buttonCssClasses: string;
  @Input() iconCssClasses: string;
  @Input() translationKeyForButtonText: string;
  @Input() data: any;

  @Output() clickEvent = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  onButtonClick(): void {
    if (this.data) {
      return this.clickEvent.emit(this.data);
    }

    this.clickEvent.emit(true);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }

}
