import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {AlertService} from '../_services/alert.service';
import {Alert} from '../_models/Alert';
import {AlertSettings} from '../_models/AlertSettings';
import {Utils} from '../../../components/_utils/Utils';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, OnDestroy {
  @Input() id = 'default-alert';
  @Input() fade = true;

  alerts: Alert[] = [];
  alertSubscription: Subscription;
  routeSubscription: Subscription;

  constructor(
    private router: Router,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.alertSubscription = this.alertService.onAlert(this.id)
      .subscribe(alert => {
          if (!alert.message) {
            this.alerts = [];
            return;
          }

          this.alerts.push(alert);

          if (alert.autoClose) {
            setTimeout(() => this.removeAlert(alert), 3000);
          }
        }
      );

    // clear alerts on location change
    /*this.routeSubscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.alertService.clear(this.id);
      }
    });*/
  }

  ngOnDestroy(): void {
    // unsubscribe to avoid memory leaks
    if (this.alertSubscription) {
      this.alertSubscription.unsubscribe();
    }
    if (this.routeSubscription) {
      this.routeSubscription.unsubscribe();
    }
  }

  removeAlert(alert: Alert): void {
    if (!this.alerts.includes(alert)) { return; }

    if (this.fade) {
      this.alerts.find(x => x === alert).fade = true;

      setTimeout(() => {
        this.alerts = this.alerts.filter(x => x !== alert);
      }, 250);
    } else {
      this.alerts = this.alerts.filter(x => x !== alert);
    }
  }

  cssClass(alert: Alert): string {
    if (!alert) { return; }

    const classes = [];

    const alertTypeClass = {
      [AlertSettings.SUCCESS]: 'cp-alert cp-alert--success',
      [AlertSettings.ERROR]: 'cp-alert cp-alert--danger',
      [AlertSettings.INFO]: 'cp-alert cp-alert--info',
      [AlertSettings.WARNING]: 'cp-alert cp-alert--warning'
    };

    classes.push(alertTypeClass[alert.alertType]);

    return classes.join(' ');
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
