import { Injectable } from '@angular/core';
import {TableControlData} from './TableControlData';

@Injectable({
  providedIn: 'root'
})
export class TableControlService {
  /**
   * Key: dataElementFullName
   *
   * Syntax: [componentName].[dataElementName]
   */
  tables = new Map<string, TableControlData>();

  constructor() { }

  add(elementFullName: string): void {
    if (!this.tables.has(elementFullName)) {
      const data = new TableControlData();
      this.tables.set(elementFullName, data);
    }
  }
}
