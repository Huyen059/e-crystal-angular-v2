import {appPageSize} from '../../components/_properties/appPageSize';

export class TableControlData {
  useCustomColumnDisplay = false;
  /**
   * key is name of column, value is whether the column should be display or not
   */
  columnDisplay = new Map<string, boolean>();
  pageSize: number;

  showPageSizeOptions = false;
  showColumnOptions = false;

  constructor() {
    this.useDefaultPageSize();
  }

  useDefaultPageSize(): void {
    this.pageSize = appPageSize;
  }

  hasCustomPageSize(): boolean {
    return this.pageSize !== appPageSize;
  }

  hasCustomColumnDisplay(): boolean {
    let hasCustomColumnDisplay = false;
    for (const [, isDisplayed] of this.columnDisplay) {
      if (!isDisplayed) {
        hasCustomColumnDisplay = true;
        break;
      }
    }

    return hasCustomColumnDisplay;
  }

  hasCustomTableOptions(): boolean {
    return this.hasCustomPageSize() || this.hasCustomColumnDisplay();
  }

  setInitialColumnDisplay(fields: any, defaultHideFields: string[]): void {
    const names = Object.keys(fields);

    for (const name of names) {
      const display = defaultHideFields.indexOf(name) < 0;
      this.columnDisplay.set(name, display);
    }
  }

  hideAllColumns(fields: any): void {
    const names = Object.keys(fields);
    for (const name of names) {
      this.columnDisplay.set(name, false);
    }
  }

  numberOfColumnsDisplayed(): number {
    let total = 0;
    this.columnDisplay.forEach((isDisplay) => {
      if (isDisplay) {
        total++;
      }
    });
    return total;
  }

  areAllColumnsDisplayed(): boolean {
    return this.numberOfColumnsDisplayed() === this.columnDisplay.size;
  }

  isRowDisplayed(): boolean {
    return this.numberOfColumnsDisplayed() !== 0;
  }
}
