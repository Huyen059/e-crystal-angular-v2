import {Component, Input, OnInit, Output, EventEmitter, OnDestroy, AfterViewInit} from '@angular/core';
import {Utils} from '../../components/_utils/Utils';
import {TableControlService} from './table-control.service';
import {TableControlData} from './TableControlData';
import {appPageSize} from '../../components/_properties/appPageSize';

@Component({
  selector: 'app-table-control',
  templateUrl: './table-control.component.html',
  styleUrls: ['./table-control.component.css']
})
export class TableControlComponent implements OnInit, AfterViewInit, OnDestroy {
  /** Array of field names - each field is displayed in a column */
  @Input() fields: any;
  @Input() defaultHideFields: string[];
  @Input() elementFullName: string;

  @Output() changePageSizeEvent = new EventEmitter<boolean>();

  /* For window event handlers:start */
  private keyUpHandler: any;
  private clickHandler: any;
  /* For window event handlers:end */

  constructor(
    private tableControlService: TableControlService,
  ) { }

  ngOnInit(): void {
    if (!this.tableData.useCustomColumnDisplay) {
      this.tableData.setInitialColumnDisplay(this.fields, this.defaultHideFields);
      this.tableData.useCustomColumnDisplay = true;
    }
  }

  ngAfterViewInit(): void {
    this.cancelDialog();
  }

  ngOnDestroy(): void {
    window.removeEventListener('click', this.clickHandler);
    window.removeEventListener('keyup', this.keyUpHandler);
  }

  /* ----- GETTERS ----- */

  get tableData(): TableControlData {
    return this.tableControlService.tables.get(this.elementFullName);
  }

  get columns(): string[] {
    return Object.keys(this.fields);
  }

  get hasCustomTableOptions(): boolean {
    return this.tableData.hasCustomTableOptions();
  }

  get isRowDisplayed(): boolean {
    return this.tableData.isRowDisplayed();
  }

  /* ----- METHODS ----- */

  hideAllColumns(): void {
    this.tableData.hideAllColumns(this.fields);
  }

  areAllColumnsDisplayed(): boolean {
    return this.tableData.areAllColumnsDisplayed();
  }

  /* ----- CUSTOM PAGESIZE AND COLUMNS ----- */

  /* Pagesize */

  setPageSize(pageSize: number): void {
    this.tableData.showPageSizeOptions = false;
    this.tableData.pageSize = pageSize;
    this.changePageSizeEvent.emit(true);
  }

  setCustomPageSize($event: KeyboardEvent): void {
    this.tableData.pageSize = ($event.target as HTMLInputElement).value as unknown as number;
  }

  togglePageSizeOptions(): void {
    this.tableData.showPageSizeOptions = !this.tableData.showPageSizeOptions;
  }

  /* Columns */

  toggleColumnOptions(): void {
    this.tableData.showColumnOptions = !this.tableData.showColumnOptions;
  }

  toggleColumn(columnName: string): void {
    const display = !this.tableData.columnDisplay.get(columnName);
    this.tableData.columnDisplay.set(columnName, display);
  }

  toggleSelectAllColumns(): void {
    if (this.tableData.hasCustomColumnDisplay) {
      this.tableData.setInitialColumnDisplay(this.fields, this.defaultHideFields);
    } else {
      this.hideAllColumns();
    }
  }

  /* Reset both */

  resetPageSizeAndColumns(): void {
    if (this.tableData.pageSize !== appPageSize) {
      this.tableData.useDefaultPageSize();
      this.changePageSizeEvent.emit(true);
    }
    this.tableData.setInitialColumnDisplay(this.fields, this.defaultHideFields);
  }

  /* ----- DROPDOWN CONTROL ----- */

  private cancelDialog(): void {
    this.clickHandler = this.closeDropdownsOnClickOutside().bind(this);
    this.keyUpHandler = this.windowKeyUpHandler().bind(this);
    window.addEventListener('click', this.clickHandler);
    window.addEventListener('keyup', this.keyUpHandler);
  }

  private windowKeyUpHandler(): (e) => void {
    return (e) => {
      if (e.key === 'Escape') {
        this.tableData.showPageSizeOptions = false;
        this.tableData.showColumnOptions = false;
      }
    };
  }

  private closeDropdownsOnClickOutside(): (e) => void {
    return (e) => {
      const pageSizeDropdown = document.querySelector('.page-size-dropdown') as HTMLElement;
      const columnsDropdown = document.querySelector('.columns-dropdown') as HTMLElement;
      if (!pageSizeDropdown.contains(e.target)) {
        this.tableData.showPageSizeOptions = false;
      }

      if (!columnsDropdown.contains(e.target)) {
        this.tableData.showColumnOptions = false;
      }
    };
  }

  /* ----- MISC ----- */

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
