import {Utils} from '../../components/_utils/Utils';

export const menuData = {
  dropdowns: [
    // Dropdown and its items
    {
      title: Utils.getElementDisplayName('erpTimesheets.timesheet'),
      items: [
        {name: Utils.getElementDisplayName('erpTimesheets.timesheet'), link: '/e-crystal/erpTimesheets/timesheet'},
        {name: Utils.getElementDisplayName('erpTimesheets.timesheetEntry'), link: '/e-crystal/erpTimesheets/timesheetEntry'},
      ],
    },
  ],
};
