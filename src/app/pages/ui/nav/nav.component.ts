import {AfterViewInit, Component, OnInit} from '@angular/core';
import {menuDataDefault} from './MenuDataDefault';
import {menuData} from './MenuData';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit, AfterViewInit {
  dropdowns: {title: string, items: {name: string, link: string}[]}[];

  constructor() { }

  ngOnInit(): void {
    this.dropdowns = menuDataDefault.dropdowns;
    if (menuData.dropdowns.length !== 0) {
      this.dropdowns = menuData.dropdowns;
    }
  }

  ngAfterViewInit(): void {
    this.toggleDropdown();
  }

  private toggleDropdown(): void {
    const dropdownButtons = document.getElementsByClassName('dropdown-btn') as HTMLCollectionOf<HTMLElement>;
    for (let i = 0; i < dropdownButtons.length; i++) {
      dropdownButtons[i].addEventListener('click', function(): void {
        this.classList.toggle('dropdown-active');

        // hide other dropdown contents
        for (let j = 0; j < dropdownButtons.length; j++) {
          if (j !== i) {
            const otherDropdownContent = dropdownButtons[j].nextElementSibling as HTMLElement;
            otherDropdownContent.style.display = 'none';
          }
        }

        // display the selected dropdown content
        const dropdownContent = this.nextElementSibling as HTMLElement;
        if (dropdownContent.style.display === 'block') {
          dropdownContent.style.display = 'none';
        } else {
          dropdownContent.style.display = 'block';
        }
      });
    }
  }
}
