import {Injectable} from '@angular/core';
import {DropdownIn} from './_models/DropdownIn';
import {DropdownState} from './_models/DropdownState';
import {DropdownData} from './_models/DropdownData';

@Injectable({
  providedIn: 'root'
})
export class DropdownService {

  private keyUpHandler: any;
  private clickHandler: any;

  dropdowns: DropdownState = [];

  constructor() {
  }

  getDropdownData(elementFullName: string, dropdownIn: DropdownIn): DropdownData {
    return this.dropdowns.find(dropdown => {
      return dropdown.elementFullName === elementFullName && dropdown.dropdownIn === dropdownIn;
    });
  }

  getFieldDropdownData(elementFullName: string, fieldName: string, dropdownIn: DropdownIn): DropdownData {
    return this.dropdowns.find(dropdown => {
      return dropdown.elementFullName === elementFullName
        && dropdown.dropdownFieldName === fieldName
        && dropdown.dropdownIn === dropdownIn;
    });
  }

  /* ----- USED IN OTHER COMPONENTS ----- */

  // We need the clearChosenItem(), and this function depends on modifyChosenItem()
  // Since modify chosen item are used in both in component and in service
  // so this action is also dispatched here
  clearDropdownChosenItem(elementFullName: string, dropdownIn: DropdownIn): void {
    const dropdownData = this.getDropdownData(elementFullName, dropdownIn);
    if (dropdownData) {
      dropdownData.chosenItem = null;
    }
  }

  clearFieldDropdownChosenItem(elementFullName: string, fieldName: string, dropdownIn: DropdownIn): void {
    const fieldDropdownData = this.getFieldDropdownData(elementFullName, fieldName, dropdownIn);
    if (fieldDropdownData) {
      fieldDropdownData.chosenItem = null;
    }
  }

  /* ----- NON-REDUX ----- */

  addDropdown(dropdownData: DropdownData): void {
    if (!this.getDropdownData(dropdownData.elementFullName, dropdownData.dropdownIn)) {
      this.dropdowns.push(dropdownData);
    }
  }

  addFieldDropdown(dropdownData: DropdownData): void {
    if (!this.getFieldDropdownData(dropdownData.elementFullName, dropdownData.dropdownFieldName, dropdownData.dropdownIn)) {
      this.dropdowns.push(dropdownData);
    }
  }

  removeDropdown(elementFullName: string, dropdownIn: DropdownIn): void {
    this.dropdowns = this.dropdowns.filter(dropdown => {
      return dropdown.elementFullName !== elementFullName && dropdown.dropdownIn !== dropdownIn;
    });
  }

  removeFieldDropdown(elementFullName: string, fieldName: string, dropdownIn: DropdownIn): void {
    this.dropdowns = this.dropdowns.filter(dropdown => {
      return dropdown.elementFullName !== elementFullName
        && dropdown.dropdownFieldName !== fieldName
        && dropdown.dropdownIn !== dropdownIn;
    });
  }

  /* ----- DROPDOWN KEYBOARD CONTROLS ----- */

  cancelDialog(): void {
    if (this.clickHandler && this.keyUpHandler) {
      return;
    }
    this.clickHandler = this.closeDropdownsOnClickOutside().bind(this);
    this.keyUpHandler = this.windowKeyUpHandler().bind(this);
    window.addEventListener('click', this.clickHandler);
    window.addEventListener('keyup', this.keyUpHandler);
  }

  private closeDropdownsOnClickOutside(): (e) => void {
    return (e) => {
      this.dropdowns.forEach((dropdownData) => {
        let cssClass;
        if (dropdownData.dropdownForAField) {
          cssClass = this.getFieldDropdownCssClass(dropdownData.elementFullName, dropdownData.dropdownFieldName, dropdownData.dropdownIn);
        } else {
          cssClass = this.getDropdownCssClass(dropdownData.elementFullName, dropdownData.dropdownIn);
        }
        const dropdownElement = document.getElementsByClassName(cssClass)[0] as HTMLDivElement;
        if (dropdownElement && !dropdownElement.contains(e.target)) {
          dropdownData.isDropdownOptionsShown = false;
        }
      });
    };
  }

  private windowKeyUpHandler(): (e: KeyboardEvent) => void {
    return (e) => {
      const isEscape = e.key === 'Escape';
      const isTab = e.key === 'Tab';
      if (isEscape || isTab) {
        // Loop through all dropdowns in this page
        this.dropdowns.forEach((dropdownData) => {
          // First hide all dropdown options
          dropdownData.isDropdownOptionsShown = false;

          // Show dropdown options for the dropdown where the cursor is located now
          if (isTab) {
            const target = e.target as HTMLElement;
            const targetDropdownIn = target.id.split('_')[0] as DropdownIn;
            const targetElementFullName = target.id.split('_')[1];
            const targetDropdownData = this.getDropdownData(targetElementFullName, targetDropdownIn);
            if (targetDropdownData) {
              targetDropdownData.isDropdownOptionsShown = true;
              // targetDropdownData.getItems();
            }
          }
        });
      }
    };
  }

  getDropdownCssClass(elementFullName: string, dropdownIn: DropdownIn): string {
    return dropdownIn
      + '-' + elementFullName.split('.')[1]
      + '-dropdown';
  }

  getFieldDropdownCssClass(elementFullName: string, fieldName: string, dropdownIn: DropdownIn): string {
    return dropdownIn
      + '-' + elementFullName.split('.')[1]
      + '-' + fieldName
      + '-dropdown';
  }
}
