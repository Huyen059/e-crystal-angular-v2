import {PageModel} from '../../../components/_models/PageModel';
import {DropdownIn} from './DropdownIn';

export class DropdownData {
  elementFullName: string;
  dropdownForAField: boolean;
  dropdownFieldName: string;
  dropdownIn: DropdownIn;
  // used for async request
  items: any[] = [];
  itemPage: PageModel = null;
  chosenItem: any = null;
  loading = false;
  error: string = null;
  queries: {[fieldName: string]: string} = {};
  // used to toggle dropdown options view
  isDropdownOptionsShown = false;
  // used for tracking position of active item when using arrow up/down
  activeDropdownOptionPosition: number = null;
}
