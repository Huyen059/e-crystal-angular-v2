import {DropdownData} from './DropdownData';

export type DropdownState = Array<DropdownData>;
