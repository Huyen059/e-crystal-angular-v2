import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldDropdownComponent } from './field-dropdown/field-dropdown.component';



@NgModule({
  declarations: [
    FieldDropdownComponent
  ],
  exports: [
    FieldDropdownComponent
  ],
  imports: [
    CommonModule
  ]
})
export class DropdownModule { }
