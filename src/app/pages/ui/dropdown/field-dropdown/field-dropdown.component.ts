import {AfterViewInit, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {Utils} from '../../../components/_utils/Utils';
import {DropdownService} from '../dropdown.service';
import {Subject, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {DropdownIn} from '../_models/DropdownIn';
import {FieldDropdownData} from './FieldDropdownData';
import {PageModel} from '../../../components/_models/PageModel';

@Component({
  selector: 'app-dropdown',
  templateUrl: './field-dropdown.component.html',
  styleUrls: ['./field-dropdown.component.css']
})
export class FieldDropdownComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  @Input() elementFullName: string;
  @Input() dropdownFieldName: string;
  @Input() dropdownIn: DropdownIn;
  @Input() dropdownOptions: string[];
  @Input() dropdownInitialValue: any;
  @Input() required: boolean;
  @Output() showFilterEvent = new EventEmitter<boolean>();
  @Output() chosenItemModifiedEvent = new EventEmitter<any>();

  private searchTerm$ = new Subject<string>();
  private searchSubscription: Subscription;

  fieldDropdownData: FieldDropdownData;

  constructor(
    private dropdownService: DropdownService,
  ) {}

  /* ----- LIFE CYCLES ----- */

  ngOnInit(): void {
    this.addDropdownOnInit();
    this.subscribeToSearchTerm();
  }

  ngAfterViewInit(): void {
    this.dropdownService.cancelDialog();
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  ngOnDestroy(): void {
    this.searchSubscription.unsubscribe();
    this.dropdownService.removeDropdown(this.elementFullName, this.dropdownIn);
  }

  /* ----- GETTERS ----- */

  get elementName(): string {
    return this.elementFullName.split('.')[1];
  }

  get chosenItem(): any {
    return this.fieldDropdownData.chosenItem;
  }

  /* ----- ON INIT ----- */

  private addDropdownOnInit(): void {
    // Add this dropdown to the store, using default values for dropdown data
    this.addDropdown();

    this.fieldDropdownData = this.dropdownService.getDropdownData(this.elementFullName, this.dropdownIn);
    if (this.dropdownInitialValue) {
      this.modifyChosenItem(this.dropdownInitialValue);
    }
  }

  private subscribeToSearchTerm(): void {
    // Subscribe to local searchTerm$
    this.searchSubscription = this.searchTerm$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      map((term: string) => {
        const input = term.trim().toLowerCase();
        this.fieldDropdownData.items = this.dropdownOptions.filter(option => option.toLowerCase().includes(input));
      })
    ).subscribe();
  }

  /* ----- DROPDOWN ----- */

  private addDropdown(): void {
    const dropdownData = new FieldDropdownData();
    dropdownData.elementFullName = this.elementFullName;
    dropdownData.dropdownIn = this.dropdownIn;
    dropdownData.dropdownFieldName = this.dropdownFieldName;
    dropdownData.items = this.dropdownOptions;
    dropdownData.dropdownForAField = true;
    const pageModel = new PageModel();
    pageModel.totalElements = this.dropdownOptions.length;
    pageModel.size = this.dropdownOptions.length;
    dropdownData.itemPage = pageModel;
    this.dropdownService.addFieldDropdown(dropdownData);
  }

  toggleDropdownOptions(): void {
    this.fieldDropdownData.isDropdownOptionsShown = !this.fieldDropdownData.isDropdownOptionsShown;
  }

  chooseADropdownOption(item: any): void {
    this.fieldDropdownData.isDropdownOptionsShown = false;
    this.modifyChosenItem(item);
  }

  clearInputField(): void {
    this.modifyChosenItem(null);
  }

  private modifyChosenItem(item): void {
    this.fieldDropdownData.chosenItem = item;
    this.chosenItemModifiedEvent.emit(item);
  }

  handleDropdownOnKeyUp($event: KeyboardEvent): void {
    const isArrowUp = $event.key === 'ArrowUp';
    const isArrowDown = $event.key === 'ArrowDown';
    const isEnter = $event.key === 'Enter';

    if (isEnter) {
      this.toggleDropdownOptions();
      return;
    }

    if (isArrowUp || isArrowDown) {
      // show the option list
      this.fieldDropdownData.isDropdownOptionsShown = true;
      const dropdownCssClass = this.dropdownService.getFieldDropdownCssClass(this.elementFullName, this.dropdownFieldName, this.dropdownIn);
      const dropdownHTMLElement = document.querySelector('.' + dropdownCssClass);

      const activeDropdownOptionPosition = Utils.arrowKeyUpDownHandler(
        $event.key,
        dropdownHTMLElement,
        this.fieldDropdownData.itemPage,
        this.fieldDropdownData.activeDropdownOptionPosition
      );

      this.fieldDropdownData.activeDropdownOptionPosition = activeDropdownOptionPosition;

      const chosenItem = this.fieldDropdownData.items[activeDropdownOptionPosition];
      this.modifyChosenItem(chosenItem);

      return;
    }

    this.searchHandler($event);
  }

  private searchHandler($event: KeyboardEvent): void {
    const input = ($event.target as HTMLInputElement).value;
    this.fieldDropdownData.activeDropdownOptionPosition = null;
    this.fieldDropdownData.isDropdownOptionsShown = true;

    this.searchTerm$.next(input);
  }

  /* ----- MISC ----- */

  cssClass(): string {
    return this.dropdownService.getFieldDropdownCssClass(this.elementFullName, this.dropdownFieldName, this.dropdownIn);
  }

  getDropdownLabel(): string {
    return Utils.getFieldDisplayName(this.elementFullName, this.dropdownFieldName) ?? this.dropdownFieldName;
  }

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text) ?? text;
  }
}
