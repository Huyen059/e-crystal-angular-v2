import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {path: '', redirectTo: 'erpTimesheets', pathMatch: 'full'},
  {path: 'erpTimesheets', loadChildren: () => import('./components/erpTimesheets/erpTimesheets.module').then(m => m.ErpTimesheetsModule)},
  {path: 'enterprise', loadChildren: () => import('./components/enterprise/enterprise.module').then(m => m.EnterpriseModule)}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
