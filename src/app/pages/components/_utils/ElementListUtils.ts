export class ElementListUtils {
  static toggleActionButtons(numberOfSelectedItems: number): void {
    const actionButtons =
      document.getElementsByClassName('element-list-main-content-action-button-wrapper') as HTMLCollectionOf<HTMLDivElement>;
    const actionButtonsForSingleItem =
      document.getElementsByClassName('action-for-single-item-only') as HTMLCollectionOf<HTMLDivElement>;

    if (numberOfSelectedItems > 0) {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < actionButtons.length; i++) {
        actionButtons[i].style.display = 'block';
      }

      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < actionButtonsForSingleItem.length; i++) {
        actionButtonsForSingleItem[i].style.display = numberOfSelectedItems === 1 ? 'block' : 'none';
      }
    } else {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < actionButtons.length; i++) {
        actionButtons[i].style.display = 'none';
      }
    }
  }
}
