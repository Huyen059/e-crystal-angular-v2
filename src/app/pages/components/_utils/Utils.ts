import {PageModel} from '../_models/PageModel';
import {Languages} from '../_models/Languages';
import {elementDisplayNames} from '../_properties/elementDisplayNames';
import {elementDisplayNamesNL} from '../_properties/elementDisplayNames-NL';
import {fieldDisplayNames} from '../_properties/fieldDisplayName';
import {fieldDisplayNamesNL} from '../_properties/fieldDisplayName-NL';
import {labelsAndTexts} from '../_properties/labelsAndTexts';
import {labelsAndTextsNL} from '../_properties/labelsAndTexts-NL';
import {User} from '../../../login/_models/User';
import {appBaseUrl} from '../_properties/appBaseUrl';
import {endpoints} from '../_properties/endpoints';
import {externalNames} from '../_properties/externalNames';

export class Utils {
  static getElementApiEndpoint(elementFullName: string): string {
    return appBaseUrl + '/' + endpoints[elementFullName];
  }

  static getElementPluralNames(elementFullName: string): string {
    let elementPluralName;
    if (externalNames[elementFullName]) {
      elementPluralName = Utils.firstToLower(externalNames[elementFullName].split('_')[1]);
    } else {
      elementPluralName = elementFullName.split('.')[1] + 's';
    }
    return elementPluralName;
  }

  static arrowKeyUpDownHandler(key: string, dropdown: Element, pageModel: PageModel, activeDropdownOptionPosition?: number): number {
    const isArrowDown = key === 'ArrowDown';

    const dropdownOptions = dropdown.lastElementChild.children as HTMLCollectionOf<HTMLElement>;
    // set background color of all dropdown options
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < dropdownOptions.length; i++) {
      dropdownOptions[i].style.backgroundColor = '#ffffff';
    }

    if (activeDropdownOptionPosition === null) {
      activeDropdownOptionPosition =
        isArrowDown ? 0 : Math.min(pageModel.totalElements, pageModel.size) - 1;
    } else {
      // update the current position
      let current = isArrowDown ? activeDropdownOptionPosition + 1 : activeDropdownOptionPosition - 1;
      if (current < 0) {
        current = Math.min(pageModel.totalElements, pageModel.size) - 1;
      }
      activeDropdownOptionPosition = current % Math.min(pageModel.totalElements, pageModel.size);
    }

    // scroll along
    dropdown.lastElementChild.scrollTop =
      activeDropdownOptionPosition * dropdownOptions[activeDropdownOptionPosition].offsetHeight;

    // set background color of the currently-focus-on dropdown option
    dropdownOptions[activeDropdownOptionPosition].style.backgroundColor = '#e6e9ef';

    return activeDropdownOptionPosition;
  }

  // custom for entry
  static countDaysBetween(startDate: Date, endDate: Date): number {
    // The number of milliseconds in all UTC days (no DST)
    const oneDay = 1000 * 60 * 60 * 24;

    // A day in UTC always lasts 24 hours (unlike in other time formats)
    const start = Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());
    const end = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());

    // so it's safe to divide by 24 hours
    return Math.round((start - end) / oneDay) + 1;
  }

  // custom for entry
  static convertDateToValueAttribute(defaultDate: Date): string {
    return defaultDate.toISOString().substr(0, 10);
  }

  // custom for entry
  static calculateNumberOfDays(startDate: Date, endDate: Date): number {
    return Utils.countDaysBetween(startDate, endDate);
  }

  // tslint:disable-next-line:typedef
  static getElementDisplayName(elementFullName: string) {
    if (localStorage.getItem('language') === Languages.ENGLISH.name) {
      if (!elementDisplayNames[elementFullName]) {
        return elementFullName;
      }
      return elementDisplayNames[elementFullName];
    }

    if (localStorage.getItem('language') === Languages.DUTCH.name) {
      if (!elementDisplayNamesNL[elementFullName]) {
        return elementFullName;
      }
      return elementDisplayNamesNL[elementFullName];
    }

    if (!elementDisplayNames[elementFullName]) {
      return elementFullName;
    }
    return elementDisplayNames[elementFullName];
  }

  static getFieldFullName(elementFullName: string, field: string): string {
    return elementFullName + '.' + field;
  }

  // tslint:disable-next-line:typedef
  static getFieldDisplayName(elementFullName: string, field: string) {
    const fieldFullName = this.getFieldFullName(elementFullName, field);
    if (localStorage.getItem('language') === Languages.ENGLISH.name) {
      if (!fieldDisplayNames[fieldFullName]) {
        return field;
      }
      return fieldDisplayNames[fieldFullName];
    }

    if (localStorage.getItem('language') === Languages.DUTCH.name) {
      if (!fieldDisplayNamesNL[fieldFullName]) {
        return field;
      }
      return fieldDisplayNamesNL[fieldFullName];
    }

    if (!fieldDisplayNames[fieldFullName]) {
      return field;
    }
    return fieldDisplayNames[fieldFullName];
  }

  // tslint:disable-next-line:typedef
  static getDisplayText(text: string) {
    if (localStorage.getItem('language') === Languages.ENGLISH.name) {
      if (!labelsAndTexts[text]) {
        return text;
      }
      return labelsAndTexts[text];
    }

    if (localStorage.getItem('language') === Languages.DUTCH.name) {
      if (!labelsAndTextsNL[text]) {
        return text;
      }
      return labelsAndTextsNL[text];
    }

    if (!labelsAndTexts[text]) {
      return text;
    }
    return labelsAndTexts[text];
  }

  static firstToLower(text: string): string {
    return  text.charAt(0).toLowerCase() + text.slice(1);
  }

  static isAdmin = (): boolean => {
    return (JSON.parse(sessionStorage.getItem('currentUser')) as User).profileName === 'admin';
  }
}
