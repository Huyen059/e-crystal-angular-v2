export class PageModel {
  /**
   * Number of items per page
   */
  size: number;
  /**
   * Total number of items
   */
  totalElements: number;
  /**
   * Total number of pages
   */
  totalPages: number;
  /**
   * The current page
   */
  number: number;
}
