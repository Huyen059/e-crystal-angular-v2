import {HrefModel} from './HrefModel';

export class LinksModel {
  self: HrefModel;
  first: HrefModel;
  last: HrefModel;
  next: HrefModel;
  prev: HrefModel;
}
