export class Language {
  constructor(
    public name: string,
    public abbr: string,
  ) {  }
}
