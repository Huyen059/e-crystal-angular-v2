import {Language} from './Language';

export class Languages {
  static readonly ENGLISH = new Language('English', 'EN');
  static readonly DUTCH = new Language('Dutch', 'NL');
  // static readonly FRENCH = new Language('French', 'FR');
  // static readonly GERMAN = new Language('German', 'DE');

  static getLanguages(): Language[] {
    return [
      Languages.DUTCH,
      // Languages.FRENCH,
      Languages.ENGLISH,
      // Languages.GERMAN,
    ];
  }

  static getDefaultLanguage(): Language {
    return Languages.ENGLISH;
  }
}
