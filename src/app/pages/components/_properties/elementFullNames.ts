export const elementFullNames = {
  erpTimesheets: {
    timesheet: 'erpTimesheets.timesheet',
    timesheetEntry: 'erpTimesheets.timesheetEntry',
    timesheetRemark: 'erpTimesheets.timesheetRemark',
    expense: 'erpTimesheets.expense',
  },
  enterprise: {
    employee: 'enterprise.employee',
    employeeType: 'enterprise.employeeType',
    project: 'enterprise.project',
    subproject: 'enterprise.subproject',
    workingMonth: 'enterprise.workingMonth',
  }
};

