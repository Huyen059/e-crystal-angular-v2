export const elementDisplayNames = {
  'erpTimesheets.expense': 'Expense',
  'erpTimesheets.timesheet': 'Timesheet',
  'erpTimesheets.timesheetEntry': 'Timesheet Entry',
  'erpTimesheets.timesheetRemark': 'Timesheet Remark',
  'enterprise.employee': 'Employee',
  'enterprise.employeeType': 'Employee Type',
  'enterprise.project': 'Project',
  'enterprise.subproject': 'Subproject',
  'enterprise.sourcer': 'Sourcer',
  'enterprise.workingMonth': 'Working Month',
};

