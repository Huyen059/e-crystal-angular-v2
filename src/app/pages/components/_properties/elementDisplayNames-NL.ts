export const elementDisplayNamesNL = {
  'erpTimesheets.expense': 'Kosten',
  'erpTimesheets.timesheet': 'Timesheet',
  'erpTimesheets.timesheetEntry': 'Timesheet Entry',
  'erpTimesheets.timesheetRemark': 'Timesheet Opmerking',
  'enterprise.employee': 'Werknemer',
  'enterprise.employeeType': 'Werknemer Type',
  'enterprise.project': 'Project',
  'enterprise.subproject': 'Subproject',
  'enterprise.sourcer': 'Sourcer',
  'enterprise.workingMonth': 'Werkende Maand',
};

