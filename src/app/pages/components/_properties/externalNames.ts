export const externalNames = {
  'enterprise.employee': 'Employee_Employees',
  'enterprise.employeeType': 'EmployeeType_EmployeeTypes',
  'erpTimesheets.expense': 'Expense_Expenses',
  'enterprise.project': 'Project_Projects',
  'enterprise.subproject': 'Subproject_Subprojects',
  'erpTimesheets.timesheet': 'Timesheet_Timesheets',
  'erpTimesheets.timesheetEntry': 'TimesheetEntry_TimesheetEntries',
  'erpTimesheets.timesheetRemark': 'TimesheetRemark_TimesheetRemarks',
  'enterprise.workingMonth': 'WorkingMonth_WorkingMonths',
};
