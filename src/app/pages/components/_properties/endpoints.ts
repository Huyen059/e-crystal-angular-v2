export const endpoints = {
  'enterprise.employee': 'enterprise/employees',
  'enterprise.employeeType': 'enterprise/employeetypes',
  'erpTimesheets.expense': 'erpTimesheets/expenses',
  'enterprise.project': 'enterprise/projects',
  'enterprise.subproject': 'enterprise/subprojects',
  'erpTimesheets.timesheet': 'erpTimesheets/timesheets',
  'erpTimesheets.timesheetEntry': 'erpTimesheets/timesheetentries',
  'erpTimesheets.timesheetRemark': 'erpTimesheets/timesheetremarks',
  'enterprise.workingMonth': 'enterprise/workingmonths',
};
