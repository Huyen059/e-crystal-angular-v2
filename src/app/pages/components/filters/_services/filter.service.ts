import {Injectable} from '@angular/core';
import {FilterData} from '../_models/FilterData';
import {FilterState} from '../_models/FilterState';
import {SelectedLinkFieldData} from '../_models/SelectedLinkFieldData';
import {SelectedValueFieldData} from '../_models/SelectedValueFieldData';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  filters: FilterState = [];
  selectedItemFromRootFilterList = new Subject();
  selectedItemFromRootFilterList$ = this.selectedItemFromRootFilterList.asObservable();

  constructor() {
  }

  displayNewFilter(elementFullName: string, savedFilterData?: FilterData): void {
    if (this.filters.length === 0 || this.currentDisplayedFilter().elementFullName !== elementFullName) {
      this.addFilter(elementFullName);
    }

    if (savedFilterData) {
      this.currentDisplayedFilter().selectedFilterOptions = savedFilterData.selectedFilterOptions;
      this.currentDisplayedFilter().selectedLinkFields = savedFilterData.selectedLinkFields;
      this.currentDisplayedFilter().selectedValueFields = savedFilterData.selectedValueFields;
    }
  }

  isFilterListShown(): boolean {
    return this.filters.length > 1;
  }

  currentDisplayedFilter(): FilterData {
    return this.filters[this.filters.length - 1];
  }

  getFilter(elementFullName): FilterData {
    return this.filters.find(filter => filter.elementFullName === elementFullName);
  }

  /* ----- REDUX ----- */

  addFilter(elementFullName: string): void {
    const filterData = new FilterData(elementFullName);
    this.filters.push(filterData);
  }

  modifyChosenFilterOptions(elementFullName: string, selectedFilterOptions: string[]): void {
    this.getFilter(elementFullName).selectedFilterOptions = selectedFilterOptions;
  }

  displaySelectedFilter(elementFullName: string): void {
    const index = this.filters.findIndex(filter => filter.elementFullName === elementFullName);
    this.filters = this.filters.slice(0, index + 1);
  }

  onSelectItemFromChildFilterList(item: any): void {
    const selectedField = new SelectedLinkFieldData(this.currentDisplayedFilter().elementFullName, item);
    this.removeLastFilter();
    this.modifySelectedLinkField(selectedField);
  }

  modifySelectedLinkField(selectedField: SelectedLinkFieldData): void {
    const index = this.currentDisplayedFilter().selectedLinkFields
      .findIndex(linkField => linkField.elementFullName = selectedField.elementFullName);
    if (index > -1) {
      this.currentDisplayedFilter().selectedLinkFields[index].value = selectedField.value;
    } else {
      this.currentDisplayedFilter().selectedLinkFields.push(selectedField);
    }
  }

  saveAllSelectedLinkFields(selectedLinkFields: Array<SelectedLinkFieldData>): void {
    this.currentDisplayedFilter().selectedLinkFields = selectedLinkFields;
  }

  saveAllSelectedValueFields(selectedValueFields: Array<SelectedValueFieldData>): void {
    this.currentDisplayedFilter().selectedValueFields = selectedValueFields;
  }

  removeLastFilter(): void {
    this.filters.pop();
  }

  clearAllFilters(): void {
    this.filters = [];
  }

  /* ----- FILTER BREADCRUMB ----- */

  getFilterBreadcrumbs(): string[] {
    const elements = [];
    for (const item of this.filters) {
      elements.push(item.elementFullName);
    }
    return elements;
  }
}
