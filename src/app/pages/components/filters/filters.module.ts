import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {DropdownModule} from '../../ui/dropdown/dropdown.module';
import {UiModule} from '../../ui/ui.module';

import {FilterComponent} from './filter/filter.component';

import {TimesheetFilterComponent} from './components/erpTimesheets/timesheet/timesheet-filter/timesheet-filter.component';
import {TimesheetFilterListComponent} from './components/erpTimesheets/timesheet/timesheet-filter-list/timesheet-filter-list.component';
import {TimesheetSearchComponent} from './components/erpTimesheets/timesheet/timesheet-search/timesheet-search.component';

import {EmployeeFilterComponent} from './components/enterprise/employee/employee-filter/employee-filter.component';
import {EmployeeFilterListComponent} from './components/enterprise/employee/employee-filter-list/employee-filter-list.component';
import {EmployeeSearchComponent} from './components/enterprise/employee/employee-search/employee-search.component';

import {TimesheetEntryFilterComponent} from './components/erpTimesheets/timesheetEntry/timesheetEntry-filter/timesheetEntry-filter.component';
import {TimesheetEntryFilterListComponent} from './components/erpTimesheets/timesheetEntry/timesheetEntry-filter-list/timesheetEntry-filter-list.component';
import {TimesheetEntrySearchComponent} from './components/erpTimesheets/timesheetEntry/timesheetEntry-search/timesheetEntry-search.component';
import {EmployeeDropdownModule} from '../enterprise/employee/employee-dropdown/employee-dropdown.module';
import {EmployeeTypeDropdownModule} from '../enterprise/employeeType/employeeType-dropdown/employeeType-dropdown.module';
import {TimesheetDropdownModule} from '../erpTimesheets/timesheet/timesheet-dropdown/timesheet-dropdown.module';
import {ProjectDropdownModule} from '../enterprise/project/project-dropdown/project-dropdown.module';
import {SubprojectDropdownModule} from '../enterprise/subproject/subproject-dropdown/subproject-dropdown.module';
import {WorkingMonthDropdownModule} from '../enterprise/workingMonth/workingMonth-dropdown/workingMonth-dropdown.module';

@NgModule({
  declarations: [
    FilterComponent,
    TimesheetFilterComponent,
    TimesheetFilterListComponent,
    TimesheetSearchComponent,
    TimesheetEntryFilterComponent,
    TimesheetEntryFilterListComponent,
    TimesheetEntrySearchComponent,
    EmployeeFilterComponent,
    EmployeeFilterListComponent,
    EmployeeSearchComponent,
  ],
  exports: [
    FilterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DropdownModule,
    UiModule,
    EmployeeDropdownModule,
    EmployeeTypeDropdownModule,
    TimesheetDropdownModule,
    ProjectDropdownModule,
    SubprojectDropdownModule,
    WorkingMonthDropdownModule,
  ]
})
export class FiltersModule { }
