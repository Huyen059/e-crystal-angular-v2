import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';

import {Utils} from '../../../../../_utils/Utils';

import {EmployeeService} from '../../../../../enterprise/employee/_services/employee.service';

@Component({
  selector: 'app-employee-search',
  templateUrl: './employee-search.component.html',
  styleUrls: ['./employee-search.component.css']
})
export class EmployeeSearchComponent implements OnInit, OnDestroy {
  @Input() fieldName: string;

  private searchTerm$ = new Subject<string>();
  private searchTermSubscription: Subscription;

  constructor(
    private employeeService: EmployeeService,
  ) {}

  get elementFullName(): string {
    return this.employeeService.elementFullName;
  }

  /* ----- LIFE CYCLES ----- */

  ngOnInit(): void {
    this.employeeService.addSliceToReduxStore();
    this.subscribeToSearchTerm();
  }

  ngOnDestroy(): void {
    this.searchTermSubscription.unsubscribe();
  }

  /* ----- ON INIT ----- */

  private subscribeToSearchTerm(): void {
    this.searchTermSubscription = this.searchTerm$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      map((term: string) => {
        const queries = {};
        queries[this.fieldName] = term.trim();
        this.employeeService.fetchItems(queries);
      })
    ).subscribe();
  }

  /* ----- HTTP REQUESTS -----*/

  search(term: string): void {
    this.searchTerm$.next(term);
  }

  /* ----- MISC ----- */

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}

