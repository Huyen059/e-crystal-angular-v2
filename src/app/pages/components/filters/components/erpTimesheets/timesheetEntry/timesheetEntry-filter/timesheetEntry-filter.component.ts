import {Component, Input, OnInit, AfterViewInit, OnDestroy} from '@angular/core';

import {FilterService} from '../../../../_services/filter.service';
import {DropdownService} from '../../../../../../ui/dropdown/dropdown.service';

import {TimesheetEntryService} from '../../../../../erpTimesheets/timesheetEntry/_services/timesheetEntry.service';
import {TimesheetEntry} from '../../../../../erpTimesheets/timesheetEntry/_models/TimesheetEntry';

import {DropdownIn} from '../../../../../../ui/dropdown/_models/DropdownIn';

import {Utils} from '../../../../../_utils/Utils';
import {FilterData} from '../../../../_models/FilterData';
import {SelectedValueFieldData} from '../../../../_models/SelectedValueFieldData';
import {SelectedLinkFieldData} from '../../../../_models/SelectedLinkFieldData';

@Component({
  selector: 'app-timesheetentry-filter',
  templateUrl: './timesheetEntry-filter.component.html',
  styleUrls: ['./timesheetEntry-filter.component.css']
})
export class TimesheetEntryFilterComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() showListForFirstFilter: boolean;

  filterOptions = [
    this.fields.externalId,
    this.fields.timesheet,
    this.fields.employee,
    this.fields.project,
    this.fields.subproject,
    this.fields.date,
    this.fields.hours,
    this.fields.status,
    this.fields.workingMonth,
    this.fields.description,
  ];

  private defaultFilterOptions: string[] = [
    this.fields.name,
  ];

  selectedFilterOptions: string[];

  fieldsToBeDisplayed: string[] = [
    this.fields.name,
  ];

  formModel = new TimesheetEntry();

  /* For dropdown:start */
  dropdownIn = DropdownIn.ELEMENT_FILTER;
  dropdownElements = [
    {
      elementFullName: 'erpTimesheets.timesheet',
      searchBy: 'name',
      required: false,
    },
    {
      elementFullName: 'enterprise.employee',
      searchBy: 'name',
      required: false,
    },
    {
      elementFullName: 'enterprise.project',
      searchBy: 'name',
      required: false,
    },
    {
      elementFullName: 'enterprise.subproject',
      searchBy: 'name',
      required: false,
    },
    {
      elementFullName: 'enterprise.workingMonth',
      searchBy: 'name',
      required: false,
    },
  ];

  statusDropdownOptions = [
    'Open',
    'Closed',
    'Failed',
    'Locked',
    'not mapped',
  ];
  /* For dropdown:end */

  constructor(
    private timesheetEntryService: TimesheetEntryService,
    private filterService: FilterService,
    private dropdownService: DropdownService,
  ) {
    // custom:start
    this.filterOptions = [
      this.fields.employee,
      this.fields.project,
      this.fields.subproject,
      this.fields.timesheet,
      this.fields.status,
      this.fields.workingMonth,
    ];

    this.defaultFilterOptions = [
      this.fields.employee,
    ];

    // custom:end
  }

  /* ----- GETTERS ----- */

  dropdownInputData(elementFullName: string): any {
    return this.dropdownElements.find(dropdown => dropdown.elementFullName === elementFullName);
  }

  /* ----- LIFE CYCLES ----- */

  ngOnInit(): void {
    this.timesheetEntryService.addSliceToReduxStore();
    this.updateChosenFilterOptions();
    if (this.timesheetEntryService.parentFullName) {
      const parentFieldName = this.timesheetEntryService.parentFullName.split('.')[1];
      this.filterOptions = this.filterOptions.filter(option => option !== parentFieldName);
      this.selectedFilterOptions = this.selectedFilterOptions.filter(option => option !== parentFieldName);
      this.defaultFilterOptions = this.defaultFilterOptions.filter(option => option !== parentFieldName);
    }
    this.retrieveFormModelLinkFieldsData();
    this.retrieveFormModelValueFieldsData();
    if (this.isFilterListShown || this.showListForFirstFilter) {
      this.getTimesheetEntries();
    }
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    if (this.filterData
      && this.filterData.selectedFilterOptions.length === 0
      && this.selectedFilterOptions.length !== 0) {
      this.filterService.modifyChosenFilterOptions(this.elementFullName, this.selectedFilterOptions);
    }
  }

  /* ----- GETTER ----- */

  get elementFullName(): string {
    return this.timesheetEntryService.elementFullName;
  }

  get isFilterListShown(): boolean {
    return this.filterService.isFilterListShown();
  }

  get fields(): { [key: string]: string } {
    return this.timesheetEntryService.fields;
  }

  get filterData(): FilterData {
    return this.filterService.getFilter(this.elementFullName);
  }

  /* ----- ON INIT ----- */

  private updateChosenFilterOptions(): void {
    if (this.filterData.selectedFilterOptions.length > 0) {
      this.selectedFilterOptions = [...this.filterData.selectedFilterOptions];
    } else {
      this.selectedFilterOptions = [...this.defaultFilterOptions];
    }
  }

  private retrieveFormModelLinkFieldsData(): void {
    if (this.filterService.currentDisplayedFilter().selectedLinkFields) {
      this.filterService.currentDisplayedFilter().selectedLinkFields.forEach(selectedLinkField => {
        const fieldName = selectedLinkField.elementFullName.split('.')[1];
        this.formModel[fieldName] = selectedLinkField.value;
      });
    }
  }

  private retrieveFormModelValueFieldsData(): void {
    if (this.filterService.currentDisplayedFilter().selectedValueFields) {
      this.filterService.currentDisplayedFilter().selectedValueFields.forEach(selectedValueField => {
        this.formModel[selectedValueField.fieldName] = selectedValueField.value;
      });
    }
  }

  /* ----- HTTP REQUESTS -----*/

  getTimesheetEntries(): void {
    this.timesheetEntryService.fetchItems(this.formModel);
  }

  /* ----- FILTER ACTIONS -----*/

  applyFilter(): void {
    this.getTimesheetEntries();
    this.saveFilterData();
  }

  clearFilters(): void {
    this.clearDropdowns();
    this.useDefaultFilterOptions();
    this.resetFormModel();
    this.getTimesheetEntries();
    this.saveFilterData();
  }

  toggleFilterOptionDisplay(option: string): void {
    const index = this.findIndexOfChosenFilterOption(option);
    if (index !== -1) {
      this.selectedFilterOptions.splice(index, 1);
      this.formModel[option] = null;
    } else {
      this.selectedFilterOptions.push(option);
    }
  }

  /* ----- DROPDOWN -----*/

  chosenItemModifiedHandler(chosenItem: any, elementFullName: string): void {
    const fieldName = elementFullName.split('.')[1];
    this.formModel[fieldName] = chosenItem;
  }

  fieldDropdownChosenItemModifiedHandler(value: string, fieldName: string): void {
    this.formModel[fieldName] = value;
  }

  /* ----- SUPPORTED METHODS -----*/

  private saveFilterData(): void {
    this.filterData.selectedFilterOptions = this.selectedFilterOptions;
    this.filterData.selectedLinkFields = this.extractLinkFieldsData();
    this.filterData.selectedValueFields = this.extractValueFieldsData();
  }

  private clearDropdowns(): void {
    this.dropdownElements.forEach((dropdownElement) => {
      this.dropdownService.clearDropdownChosenItem(dropdownElement.elementFullName, this.dropdownIn);
    });
    this.dropdownService.clearFieldDropdownChosenItem(this.elementFullName, this.fields.status, this.dropdownIn);
  }

  private resetFormModel(): void {
    this.formModel = new TimesheetEntry();
  }

  private findIndexOfChosenFilterOption(fieldName: string): number {
    return this.selectedFilterOptions.findIndex(chosenOption => chosenOption === fieldName);
  }

  private useDefaultFilterOptions(): void {
    this.selectedFilterOptions = [...this.defaultFilterOptions];
  }

  isFilterOptionChosen(fieldName: string): boolean {
    return this.findIndexOfChosenFilterOption(fieldName) !== -1;
  }

  extractValueFieldsData(): Array<SelectedValueFieldData> {
    const result = [];
    Object.keys(this.formModel).forEach(fieldName => {
      const isValueField = this.dropdownElements.findIndex(element => element.elementFullName.split('.')[1] === fieldName) < 0;
      if (isValueField && this.formModel[fieldName]) {
        result.push(new SelectedValueFieldData(fieldName, this.formModel[fieldName]));
      }
    });
    return result;
  }

  private extractLinkFieldsData(): Array<SelectedLinkFieldData> {
    const result = [];
    this.dropdownElements.forEach(dropdown => {
      const fieldName = dropdown.elementFullName.split('.')[1];
      if (this.formModel[fieldName]) {
        result.push(new SelectedLinkFieldData(dropdown.elementFullName, this.formModel[fieldName]));
      }
    });
    return result;
  }

  /* ----- MISC ----- */

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
