import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';

import {Utils} from '../../../../../_utils/Utils';

import {TimesheetEntryService} from '../../../../../erpTimesheets/timesheetEntry/_services/timesheetEntry.service';

@Component({
  selector: 'app-timesheetentry-search',
  templateUrl: './timesheetEntry-search.component.html',
  styleUrls: ['./timesheetEntry-search.component.css']
})
export class TimesheetEntrySearchComponent implements OnInit, OnDestroy {
  @Input() fieldName: string;

  private searchTerm$ = new Subject<string>();
  private searchTermSubscription: Subscription;

  constructor(
    private timesheetEntryService: TimesheetEntryService,
  ) {}

  get elementFullName(): string {
    return this.timesheetEntryService.elementFullName;
  }

  /* ----- LIFE CYCLES ----- */

  ngOnInit(): void {
    this.timesheetEntryService.addSliceToReduxStore();
    this.subscribeToSearchTerm();
  }

  ngOnDestroy(): void {
    this.searchTermSubscription.unsubscribe();
  }

  /* ----- ON INIT ----- */

  private subscribeToSearchTerm(): void {
    this.searchTermSubscription = this.searchTerm$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      map((term: string) => {
        const queries = {};
        queries[this.fieldName] = term.trim();
        this.timesheetEntryService.fetchItems(queries);
      })
    ).subscribe();
  }

  /* ----- HTTP REQUESTS -----*/

  search(term: string): void {
    this.searchTerm$.next(term);
  }

  /* ----- MISC ----- */

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}

