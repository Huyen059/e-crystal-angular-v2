import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {PaginationIn} from '../../../../../../ui/pagination/PaginationIn';
import {Observable, Subscription} from 'rxjs';

import {FilterService} from '../../../../_services/filter.service';

import {select} from '@angular-redux/store';
import {ErpTimesheetsTimesheetSlicePaths as paths} from '../../../../../../../state/slices/erpTimesheets.timesheet.slice';

import {Utils} from '../../../../../_utils/Utils';
import {elementFullNames} from '../../../../../_properties/elementFullNames';

import {TimesheetService} from '../../../../../erpTimesheets/timesheet/_services/timesheet.service';
import {TimesheetOutputModel} from '../../../../../erpTimesheets/timesheet/_models/TimesheetOutputModel';
import {LinksModel} from '../../../../../_models/LinksModel';
import {PageModel} from '../../../../../_models/PageModel';


@Component({
  selector: 'app-timesheet-filter-list',
  templateUrl: './timesheet-filter-list.component.html',
  styleUrls: ['./timesheet-filter-list.component.css']
})
export class TimesheetFilterListComponent implements OnInit, OnChanges, OnDestroy {
  @Input() fieldsToBeDisplayed: string[];

  paginationIn = PaginationIn.ELEMENT_FILTER_LIST;

  @select([elementFullNames.erpTimesheets.timesheet, paths.list_requests_GET])
  getRequest$: Observable<any>;
  timesheets: TimesheetOutputModel[];
  links: LinksModel;
  page: PageModel;
  private getRequestSubscription: Subscription;

  constructor(
    private filterService: FilterService,
    private timesheetService: TimesheetService,
  ) {
  }

  get elementFullName(): string {
    return this.timesheetService.elementFullName;
  }

  /* ----- LIFE CYCLES ----- */

  ngOnInit(): void {
    this.getRequestSubscription = this.getRequest$.subscribe(requestData => {
      if (requestData) {
        this.timesheets = requestData.timesheets ?? [];
        this.links = requestData.links ?? null;
        this.page = requestData.page ?? null;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  ngOnDestroy(): void {
    this.getRequestSubscription.unsubscribe();
  }

  /* ----- ON INIT ----- */

  /* ----- HTTP REQUESTS -----*/

  getTimesheetsByPageUrl(pageUrl: string): void {
    this.timesheetService.fetchItemsByPageUrl(pageUrl);
  }

  /* ----- SELECT ITEM ----- */

  selectItemFromFilterList(item: any): void {
    const isItemFromRootFilterListForAFormField = this.filterService.filters.length === 1;
    if (isItemFromRootFilterListForAFormField) {
      this.filterService.selectedItemFromRootFilterList.next(item);
      return;
    }
    this.filterService.onSelectItemFromChildFilterList(item);
  }

  /* ----- PAGINATION -----*/

  changePage(pageUrl: string): void {
    if (pageUrl) {
      this.getTimesheetsByPageUrl(pageUrl);
    }
  }

  /* ----- MISC ----- */

  trackByExternalId(index: number, object: any): string {
    return object.externalId;
  }

  getElementDisplayName(elementFullName: string): string {
    return Utils.getElementDisplayName(elementFullName);
  }

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
