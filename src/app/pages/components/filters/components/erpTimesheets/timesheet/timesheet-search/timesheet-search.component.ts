import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';

import {Utils} from '../../../../../_utils/Utils';

import {TimesheetService} from '../../../../../erpTimesheets/timesheet/_services/timesheet.service';

@Component({
  selector: 'app-timesheet-search',
  templateUrl: './timesheet-search.component.html',
  styleUrls: ['./timesheet-search.component.css']
})
export class TimesheetSearchComponent implements OnInit, OnDestroy {
  @Input() fieldName: string;

  private searchTerm$ = new Subject<string>();
  private searchTermSubscription: Subscription;

  constructor(
    private timesheetService: TimesheetService,
  ) {}

  get elementFullName(): string {
    return this.timesheetService.elementFullName;
  }

  /* ----- LIFE CYCLES ----- */

  ngOnInit(): void {
    this.timesheetService.addSliceToReduxStore();
    this.subscribeToSearchTerm();
  }

  ngOnDestroy(): void {
    this.searchTermSubscription.unsubscribe();
  }

  /* ----- ON INIT ----- */

  private subscribeToSearchTerm(): void {
    this.searchTermSubscription = this.searchTerm$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      map((term: string) => {
        const queries = {};
        queries[this.fieldName] = term.trim();
        this.timesheetService.fetchItems(queries);
      })
    ).subscribe();
  }

  /* ----- HTTP REQUESTS -----*/

  search(term: string): void {
    this.searchTerm$.next(term);
  }

  /* ----- MISC ----- */

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}

