import {SelectedLinkFieldData} from './SelectedLinkFieldData';
import {SelectedValueFieldData} from './SelectedValueFieldData';

export class FilterData {
  constructor(
    public elementFullName: string = null,
    public selectedFilterOptions: string[] = [],
    public selectedLinkFields: Array<SelectedLinkFieldData> = [],
    public selectedValueFields: Array<SelectedValueFieldData> = [],
  ) {  }

  saveFilterOption(newFieldName: string): void {
    const index = this.selectedFilterOptions.findIndex(fieldName => fieldName === newFieldName);
    if (index > -1) {
      return;
    }
    this.selectedFilterOptions.push(newFieldName);
  }

  saveLinkField(newLinkFieldData: SelectedLinkFieldData): void {
    const index = this.selectedLinkFields
      .findIndex(linkFieldData => linkFieldData.elementFullName === newLinkFieldData.elementFullName);
    if (index < 0) {
      this.selectedLinkFields.push(newLinkFieldData);
    } else {
      this.selectedLinkFields[index] = newLinkFieldData.value;
    }
  }

  saveValueField(newValueFieldData: SelectedValueFieldData): void {
    const index = this.selectedValueFields
      .findIndex(linkFieldData => linkFieldData.fieldName === newValueFieldData.fieldName);
    if (index < 0) {
      this.selectedValueFields.push(newValueFieldData);
    } else {
      this.selectedValueFields[index] = newValueFieldData.value;
    }
  }

  generateQueries(): object {
    const queries = {};
    this.selectedLinkFields.forEach(linkFieldData => {
      const fieldName = linkFieldData.elementFullName.split('.')[1];
      queries[fieldName] = linkFieldData.value;
    });
    this.selectedValueFields.forEach(valueFieldData => {
      queries[valueFieldData.fieldName] = valueFieldData.value;
    });
    return queries;
  }
}
