export class SelectedValueFieldData {
  constructor(
    public fieldName: string = null,
    public value: any = null
  ) {  }
}
