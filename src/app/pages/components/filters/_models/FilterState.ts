import {FilterData} from './FilterData';

export type FilterState = Array<FilterData>;
