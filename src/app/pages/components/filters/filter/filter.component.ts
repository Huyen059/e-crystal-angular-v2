import {Component, Input, OnInit, Output, EventEmitter, OnDestroy} from '@angular/core';
import {FilterService} from '../_services/filter.service';
import {Utils} from '../../_utils/Utils';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit, OnDestroy {
  @Input() showListForFirstFilter: boolean;
  @Output() filterResult = new EventEmitter<any>();
  @Output() selectItem = new EventEmitter<any>();

  constructor(
    private filterService: FilterService,
  ) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  nameOfElementBeingFiltered(): string {
    return this.filterService.currentDisplayedFilter().elementFullName;
  }

  retrieveFilterResult(filterResult: any): void {
    if (this.filterService.filters.length === 1) {
      this.filterResult.emit(filterResult);
    }
  }

  get filterBreadcrumbs(): string[] {
    return  this.filterService.getFilterBreadcrumbs();
  }

  getElementDisplayName(elementFullName: string): string {
    return Utils.getElementDisplayName(elementFullName);
  }

  displayFilter(elementFullName: string): void {
    this.filterService.displaySelectedFilter(elementFullName);
  }

  /* ----- MISC ----- */

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
