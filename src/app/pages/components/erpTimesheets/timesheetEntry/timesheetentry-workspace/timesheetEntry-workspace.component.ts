import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {TimesheetEntryService} from '../_services/timesheetEntry.service';
import {AlertService} from '../../../../ui/alert/_services/alert.service';
import {TableControlService} from '../../../../ui/table-control/table-control.service';
import {SortService} from '../../../../ui/sort/sort.service';
import {FilterService} from '../../../filters/_services/filter.service';
import {select} from '@angular-redux/store';
import {Observable, Subscription} from 'rxjs';
import {HierarchySlice} from '../../../../../state/slices/hierarchy.slice';
import {elementFullNames} from '../../../_properties/elementFullNames';
import {ErpTimesheetsTimesheetEntrySlicePaths as paths} from '../../../../../state/slices/erpTimesheets.timesheetEntry.slice';

@Component({
  selector: 'app-timesheetentry-workspace',
  templateUrl: './timesheetEntry-workspace.component.html',
  styleUrls: ['./timesheetEntry-workspace.component.css']
})
export class TimesheetEntryWorkspaceComponent implements OnInit, OnDestroy {

  private routeSubscription: Subscription;
  isSubworkspace: boolean;

  @select() hierarchy$: Observable<HierarchySlice>;
  hierarchySubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheetEntry, paths.alert])
  alert$: Observable<{ type: string, messageTranslationKey: string }>;
  private alertSubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheetEntry, paths.queries])
  queries$: Observable<object>;
  private queriesSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private timesheetEntryService: TimesheetEntryService,
    private tableControlService: TableControlService,
    private filterService: FilterService,
    private sortService: SortService,
    private alertService: AlertService,
  ) {  }

  get elementFullName(): string {
    return this.timesheetEntryService.elementFullName;
  }

  ngOnInit(): void {
    this.timesheetEntryService.addSliceToReduxStore();

    this.alertService.clear();
    this.tableControlService.add(this.elementFullName);
    this.sortService.add(this.elementFullName);

    this.routeSubscription = this.route.data.subscribe(data => {
      this.timesheetEntryService.isChildElement = data.isMainWorkspace === undefined;
      this.isSubworkspace = this.timesheetEntryService.isChildElement;
    });

    this.extractParentData();
    this.alertSubscription = this.alert$.subscribe(alert => {
      if (alert) {
        this.alertService.alertByType(alert.type, alert.messageTranslationKey, {autoClose: true});
      }
    });

    this.queriesSubscription = this.queries$.subscribe(queries => {
      this.timesheetEntryService.queries = queries ?? null;
    });
  }

  ngOnDestroy(): void {
    this.filterService.clearAllFilters();
    this.hierarchySubscription.unsubscribe();
    this.alertSubscription.unsubscribe();
    this.routeSubscription.unsubscribe();
    this.queriesSubscription.unsubscribe();
  }

  private extractParentData(): void {
    this.hierarchySubscription = this.hierarchy$.subscribe(hierarchy => {
      if (hierarchy.currentLevel > 0) {
        const inChildrenList = hierarchy[hierarchy.currentLevel].childrenFullNames
          .findIndex(childFullName => childFullName === this.elementFullName) > -1;
        if (inChildrenList) {
          this.timesheetEntryService.parentFullName = hierarchy[hierarchy.currentLevel].elementFullName;
          this.timesheetEntryService.parentValue = hierarchy[hierarchy.currentLevel].value;
        }
      } else {
        this.timesheetEntryService.parentFullName = null;
        this.timesheetEntryService.parentValue = null;
      }
    });
  }
}
