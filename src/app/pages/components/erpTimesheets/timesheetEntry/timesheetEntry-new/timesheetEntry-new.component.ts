import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {Location} from '@angular/common';

import {Utils} from '../../../_utils/Utils';
import {elementFullNames} from '../../../_properties/elementFullNames';

import {TimesheetEntryService} from '../_services/timesheetEntry.service';
import {TimesheetEntry} from '../_models/TimesheetEntry';
import {TimesheetEntryPostInputMapper} from '../_models/TimesheetEntryPostInputMapper';

import {FilterService} from '../../../filters/_services/filter.service';
import {DropdownService} from '../../../../ui/dropdown/dropdown.service';
import {NavigationService} from '../../../_services/navigation.service';

import {select} from '@angular-redux/store';
import {ErpTimesheetsTimesheetEntrySlicePaths as paths} from '../../../../../state/slices/erpTimesheets.timesheetEntry.slice';

import {DropdownIn} from '../../../../ui/dropdown/_models/DropdownIn';

@Component({
  selector: 'app-timesheetentry-new',
  templateUrl: './timesheetEntry-new.component.html',
  styleUrls: ['./timesheetEntry-new.component.css']
})
export class TimesheetEntryNewComponent implements OnInit, AfterViewInit, OnDestroy {
  formModel: TimesheetEntry = new TimesheetEntry();

  /* For dropdown:start */
  dropdownIn = DropdownIn.ELEMENT_FORM;
  dropdownElements = [
    {
      elementFullName: 'erpTimesheets.timesheet',
      searchBy: 'name',
      required: true,
    },
    {
      elementFullName: 'enterprise.employee',
      searchBy: 'name',
      required: false,
    },
    {
      elementFullName: 'enterprise.project',
      searchBy: 'name',
      required: true,
    },
    {
      elementFullName: 'enterprise.subproject',
      searchBy: 'name',
      required: false,
    },
    {
      elementFullName: 'enterprise.workingMonth',
      searchBy: 'name',
      required: false,
    },
  ];
  /* For dropdown:end */

  isFilterMenuShown = false;

  @select([elementFullNames.erpTimesheets.timesheetEntry, paths.new_requests_CREATE])
  createRequest$: Observable<any>;
  private createRequestSubscription: Subscription;

  // custom
  // boolean values used in "check box" to allow user to create multiple entries at the same time
  isMultipleDays = false;
  isHourDifferentByDay = false;

  // custom
  // for form display
  dates: Date[] = [];

  constructor(
    private router: Router,
    private location: Location,
    private timesheetEntryService: TimesheetEntryService,
    private filterService: FilterService,
    private dropdownService: DropdownService,
    private navigationService: NavigationService,
  ) {
    this.dropdownElements = [
      {
        elementFullName: 'erpTimesheets.timesheet',
        searchBy: 'name',
        required: true,
      },
      {
        elementFullName: 'enterprise.project',
        searchBy: 'name',
        required: true,
      },
      {
        elementFullName: 'enterprise.subproject',
        searchBy: 'name',
        required: false,
      },
    ];
  }

  /* ----- LIFE CYCLES ----- */

  ngOnInit(): void {
    this.updateHeaderBreadcrumb();

    this.createRequestSubscription = this.createRequest$.subscribe(requestData => {
      if (requestData && requestData.backUrl) {
        this.router.navigate([requestData.backUrl]);
      }
    });

    this.retrieveLinkFieldInitialValues();

    // custom
    this.formModel.includeWeekend = false;
  }

  ngAfterViewInit(): void {
    // custom
    this.fillDate();
    // custom
    this.setInitialConstraintOnSubproject();
  }

  ngOnDestroy(): void {
    this.createRequestSubscription.unsubscribe();
    this.filterService.clearAllFilters();
    this.timesheetEntryService.removeAlert();
    this.timesheetEntryService.removeRequestData(paths.new_requests_CREATE);
  }

  /* ----- GETTERS ----- */

  get elementFullName(): string {
    return this.timesheetEntryService.elementFullName;
  }

  get fields(): { [key: string]: string } {
    return this.timesheetEntryService.fields;
  }

  /* ----- ON INIT ----- */

  retrieveLinkFieldInitialValues(): any {
    if (this.timesheetEntryService.parentFullName) {
      const parentFieldName = this.getElementName(this.timesheetEntryService.parentFullName);
      this.formModel[parentFieldName] = this.timesheetEntryService.parentValue;
    }

    this.dropdownElements.forEach(dropdown => {
      const fieldName = this.getElementName(dropdown.elementFullName);
      if (this.timesheetEntryService.queries && this.timesheetEntryService.queries[fieldName]) {
        this.formModel[fieldName] = this.timesheetEntryService.queries[fieldName];
      }
    });
  }

  private updateHeaderBreadcrumb(): void {
    this.navigationService.changeRouter(this.router);
  }

  // custom
  private setInitialConstraintOnSubproject(): void {
    this.dropdownService.getDropdownData('enterprise.subproject', this.dropdownIn)
      .queries.project = 'mustChooseAProject';
  }

  /* ----- HTTP REQUEST ----- */

  addTimesheetEntry(): void {
    // custom: start
    if (this.formModel.startDate && this.formModel.endDate && !this.formModel.hoursByDay) {
      this.formModel.hoursByDay = [];
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.dates.length; i++) {
        this.formModel.hoursByDay.push(this.formModel.hours);
      }
    }
    // custom: end

    const backUrl = this.timesheetEntryService.generateBackUrl();
    this.timesheetEntryService.addItem(TimesheetEntryPostInputMapper.map(this.formModel), backUrl);
  }

  /* ----- CLOSE FORM ----- */

  closeForm(): void {
    this.location.back();
  }

  /* ----- DROPDOWN INITIAL VALUE ----- */

  getDropdownInitialValue(elementFullName: string): any {
    return this.formModel[this.getElementName(elementFullName)];
  }

  /* ----- DROPDOWN -----*/

  dropdownInputData(elementFullName: string): any {
    return this.dropdownElements.find(dropdown => dropdown.elementFullName === elementFullName);
  }

  chosenItemModifiedHandler(chosenItem: any, elementFullName: string): void {
    const fieldName = elementFullName.split('.')[1];
    this.formModel[fieldName] = chosenItem;

    // custom
    if (elementFullName === 'erpTimesheets.timesheet') {
      this.setTimesheetEntryDisplayDate(chosenItem.month, chosenItem.year);
    }

    // custom
    if (elementFullName === 'enterprise.project') {
      const subprojectDropdownData = this.dropdownService.getDropdownData('enterprise.subproject', this.dropdownIn);
      if (subprojectDropdownData) {
        const value = chosenItem ? chosenItem.externalId : 'mustChooseAProject';
        subprojectDropdownData.queries.project = value;
      }
    }
  }

  /* ----- CUSTOM ----- */

  private fillDate(): void {
    // If there's no chosen timesheet yet, display the list of timesheet for user to choose
    if (this.formModel.timesheet === null) {
      // set default date to today
      this.formModel.date = Utils.convertDateToValueAttribute(new Date());
    }
  }

  // when select a certain timesheet, display the corresponding month, year in the input for date
  private setTimesheetEntryDisplayDate(month: number, year: number): void {
    const today = new Date();
    const timesheetDate = new Date();
    timesheetDate.setFullYear(year, month - 1, 1);
    if (timesheetDate > today) {
      this.formModel.date = Utils.convertDateToValueAttribute(timesheetDate);
    } else {
      this.formModel.date = Utils.convertDateToValueAttribute(today);
    }
  }

  // include weekend in the date range or not
  toggleIncludeWeekend(): void {
    this.formModel.includeWeekend = !this.formModel.includeWeekend;
    this.updateDatesArray();
    this.switchHoursValues();
  }

  // Display forms for multiple days/hours
  displayChooseMultipleDaysForm(): void {
    this.isMultipleDays = !this.isMultipleDays;
    this.switchDateValues();
  }

  displayEditHourByDayForm(): void {
    this.isHourDifferentByDay = !this.isHourDifferentByDay;
    this.switchHoursValues();
  }

  private switchDateValues(): void {
    const dateInput = document.querySelector('#date') as HTMLInputElement;
    if (this.isMultipleDays) {
      this.formModel.date = dateInput.value;
      this.formModel.startDate = this.formModel.date;
      this.formModel.endDate = this.formModel.date;
    }

    if (!this.isMultipleDays) {
      this.formModel.date = this.formModel.startDate;
      dateInput.value = this.formModel.date;
      this.formModel.startDate = null;
      this.formModel.endDate = null;
      this.formModel.hoursByDay = null;
    }
  }

  switchHoursValues(): void {
    if (this.isHourDifferentByDay) {
      this.formModel.hoursByDay = [];
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.dates.length; i++) {
        this.formModel.hoursByDay.push(this.formModel.hours);
      }
    }

    if (!this.isHourDifferentByDay) {
      if (this.formModel.hoursByDay != null) {
        this.formModel.hours = this.formModel.hoursByDay[0];
      }
      this.formModel.hoursByDay = null;
    }
  }

  // React when user change start and/or end dates
  updateDatesArray(): void {
    const startDate = new Date(this.formModel.startDate);
    const endDate = new Date(this.formModel.endDate);
    const numberOfDays = Utils.calculateNumberOfDays(startDate, endDate);
    this.dates = [];
    for (let i = 0; i < numberOfDays; i++) {
      const nextDate = new Date();
      nextDate.setFullYear(startDate.getFullYear(), startDate.getMonth());
      nextDate.setDate(startDate.getDate() + i);
      this.dates.push(nextDate);
    }

    if (!this.formModel.includeWeekend) {
      this.dates = this.dates.filter(date => date.getDay() !== 0 && date.getDay() !== 6);
    }
  }

  /* ----- MISC ----- */

  trackByIdx(index: number): any {
    return index;
  }

  getElementName(elementFullName: string): string {
    return elementFullName.split('.')[1];
  }

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
