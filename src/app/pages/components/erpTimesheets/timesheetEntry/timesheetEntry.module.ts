import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TimesheetEntryRoutingModule} from './timesheetEntry-routing.module';
import {TimesheetEntryWorkspaceComponent} from './timesheetentry-workspace/timesheetEntry-workspace.component';
import {TimesheetEntryListComponent} from './timesheetEntry-list/timesheetEntry-list.component';
import {TimesheetEntryNewComponent} from './timesheetEntry-new/timesheetEntry-new.component';
import {TimesheetEntryEditComponent} from './timesheetEntry-edit/timesheetEntry-edit.component';
import {TimesheetEntryDetailComponent} from './timesheetEntry-detail/timesheetEntry-detail.component';
import {FiltersModule} from '../../filters/filters.module';
import {UiModule} from '../../../ui/ui.module';
import {FormsModule} from '@angular/forms';
import {DropdownModule} from '../../../ui/dropdown/dropdown.module';
import {SubprojectDropdownModule} from '../../enterprise/subproject/subproject-dropdown/subproject-dropdown.module';
import {ProjectDropdownModule} from '../../enterprise/project/project-dropdown/project-dropdown.module';
import {TimesheetDropdownModule} from '../timesheet/timesheet-dropdown/timesheet-dropdown.module';


@NgModule({
  declarations: [
    TimesheetEntryWorkspaceComponent,
    TimesheetEntryListComponent,
    TimesheetEntryNewComponent,
    TimesheetEntryEditComponent,
    TimesheetEntryDetailComponent
  ],
  imports: [
    CommonModule,
    FiltersModule,
    UiModule,
    FormsModule,
    DropdownModule,
    TimesheetEntryRoutingModule,
    SubprojectDropdownModule,
    ProjectDropdownModule,
    TimesheetDropdownModule,
  ]
})
export class TimesheetEntryModule {
}
