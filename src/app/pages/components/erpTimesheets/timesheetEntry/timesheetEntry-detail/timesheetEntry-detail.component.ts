import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

import {Utils} from '../../../_utils/Utils';
import {elementFullNames} from '../../../_properties/elementFullNames';

import {TimesheetEntryService} from '../_services/timesheetEntry.service';
import {TimesheetEntryOutputModel} from '../_models/TimesheetEntryOutputModel';

import {NavigationService} from '../../../_services/navigation.service';
import {AlertService} from '../../../../ui/alert/_services/alert.service';
import {HierarchyService} from '../../../_services/hierarchy.service';

import {NgRedux, select} from '@angular-redux/store';
import {
  ErpTimesheetsTimesheetEntrySlicePaths as paths,
  ErpTimesheetsTimesheetEntrySlicePaths
} from '../../../../../state/slices/erpTimesheets.timesheetEntry.slice';

import {AppState} from '../../../../../state/AppState';

@Component({
  selector: 'app-timesheetentry-detail',
  templateUrl: './timesheetEntry-detail.component.html',
  styleUrls: ['./timesheetEntry-detail.component.css']
})
export class TimesheetEntryDetailComponent implements OnInit, AfterViewInit, OnDestroy {
  private selectedId: string;

  @select([
    elementFullNames.erpTimesheets.timesheetEntry,
    paths.detail_requests_GET,
    'timesheetEntry'
  ])
  timesheetEntry$: Observable<TimesheetEntryOutputModel>;
  timesheetEntry: TimesheetEntryOutputModel;
  private timesheetEntrySubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheetEntry, paths.detail_requests_DELETE])
  deleteRequest$: Observable<any>;
  private deleteRequestSubscription: Subscription;

  showTabControl = false;
  showTabContent = false;

  dataChildren = {
    expense: 'erpTimesheets.expense',
  };

  displayDialog = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private timesheetEntryService: TimesheetEntryService,
    private navigationService: NavigationService,
    private alertService: AlertService,
    private ngRedux: NgRedux<AppState>,
    private hierarchyService: HierarchyService,
  ) {
  }

  /* ----- LIFE CYCLES ----- */

  ngOnInit(): void {
    this.timesheetEntrySubscription = this.timesheetEntry$.subscribe(timesheetEntry => this.timesheetEntry = timesheetEntry);
    this.deleteRequestSubscription = this.deleteRequest$.subscribe(requestData => {
      if (requestData && requestData.success) {
        this.router.navigate([requestData.backUrl]);
      }
    });
    this.getTimesheetEntry();
    this.updateHeaderBreadcrumb();
    this.checkTimesheetEntry().then(() => {
      this.hierarchyService.addLevel(this.elementFullName, this.timesheetEntry, this.dataChildrenFullNames);
      this.showTabContent = true;
    });
    this.checkTabElements().then(() => this.showTabControl = true);
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.timesheetEntrySubscription.unsubscribe();
    this.deleteRequestSubscription.unsubscribe();

    this.dataChildrenFullNames.forEach(childFullName => {
      if (this.ngRedux.getState()[childFullName]) {
        const actionType = childFullName + '/DATA_REMOVED';
        this.ngRedux.dispatch({type: actionType});
      }
    });

    this.hierarchyService.removeAllLevels();
    this.timesheetEntryService.removeAlert();
    this.timesheetEntryService.removeRequestData(paths.detail_requests_GET);
    this.timesheetEntryService.removeRequestData(paths.detail_requests_DELETE);
  }

  /* ----- GETTERS ----- */

  get elementFullName(): string {
    return this.timesheetEntryService.elementFullName;
  }

  get numberOfDataChildren(): number {
    return Object.keys(this.dataChildren).length;
  }

  get fields(): { [key: string]: string } {
    return this.timesheetEntryService.fields;
  }

  get dataChildrenFullNames(): string[] {
    return Object.values(this.dataChildren);
  }

  /* -----ON INIT ----- */

  private updateHeaderBreadcrumb(): void {
    this.navigationService.changeRouter(this.router);
  }

  /* ----- HTTP REQUESTS ----- */

  private getTimesheetEntry(): void {
    this.selectedId = this.route.snapshot.paramMap.get('externalId');
    this.timesheetEntryService.detailFetchItem(this.selectedId);
  }

  delete(externalId: string): void {
    const backUrl = this.timesheetEntryService.generateBackUrl();
    this.timesheetEntryService.detailRemoveItem(externalId, backUrl);
  }

  /* ----- NAVIGATION ----- */

  goToEditTimesheetEntryForm(externalId: string): void {
    this.router.navigate([this.timesheetEntryService.elementHomeUrl + '/edit', {externalId}]);
  }

  /* ----- DIALOG ----- */

  showDialog(): void {
    this.displayDialog = true;
  }

  hideDialog(): void {
    this.displayDialog = false;
  }

  handleDialogEventEmitter(confirm: boolean): void {
    this.hideDialog();
    if (confirm) {
      this.delete(this.selectedId);
    }
  }

  /* ----- MISC ----- */

  getElementName(elementFullName: string): string {
    return elementFullName.split('.')[1];
  }

  getElementDisplayName(elementFullName: string): string {
    return Utils.getElementDisplayName(elementFullName);
  }

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }

  checkTabElements = async () => {
    while (document.getElementsByClassName('ns-tabs__links').length < this.numberOfDataChildren) {
      await new Promise(resolve => {
        requestAnimationFrame(resolve);
      });
    }

    return true;
  }

  checkTimesheetEntry = async () => {
    while (!this.timesheetEntry) {
      await new Promise(resolve => {
        requestAnimationFrame(resolve);
      });
    }

    return true;
  }
}
