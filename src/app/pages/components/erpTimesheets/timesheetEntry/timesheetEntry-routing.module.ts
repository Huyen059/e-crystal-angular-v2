import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {TimesheetEntryWorkspaceComponent} from './timesheetentry-workspace/timesheetEntry-workspace.component';
import {TimesheetEntryListComponent} from './timesheetEntry-list/timesheetEntry-list.component';
import {TimesheetEntryNewComponent} from './timesheetEntry-new/timesheetEntry-new.component';
import {TimesheetEntryEditComponent} from './timesheetEntry-edit/timesheetEntry-edit.component';
import {TimesheetEntryDetailComponent} from './timesheetEntry-detail/timesheetEntry-detail.component';

import {ExpenseWorkspaceComponent} from '../expense/expense-workspace/expense-workspace.component';
import {ExpenseListComponent} from '../expense/expense-list/expense-list.component';

import {Utils} from '../../_utils/Utils';

const routes: Routes = [
  {path: '', component: TimesheetEntryWorkspaceComponent,
    data: {name: Utils.getElementDisplayName('erpTimesheets.timesheetEntry'), isMainWorkspace: true},
    children: [
      {path: '', component: TimesheetEntryListComponent, data: {name: '', isMainList: true}},
      {path: 'new', component: TimesheetEntryNewComponent, data: {name: Utils.getDisplayText('new')}},
      {path: 'edit', component: TimesheetEntryEditComponent, data: {name: Utils.getDisplayText('edit')}},
      {path: ':externalId', component: TimesheetEntryDetailComponent, data: {name: Utils.getDisplayText('detail')},
        children: [
          {path: '', redirectTo: 'expense', pathMatch: 'full'},
          {path: 'expense', component: ExpenseWorkspaceComponent,
            data: {name: Utils.getElementDisplayName('erpTimesheets.expense')},
            children: [
              {path: '', component: ExpenseListComponent, data: {name: ''}},
            ]
          },
        ]},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimesheetEntryRoutingModule { }
