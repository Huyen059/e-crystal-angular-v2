import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subject, Subscription} from 'rxjs';

import {Utils} from '../../../_utils/Utils';
import {ElementListUtils} from '../../../_utils/ElementListUtils';
import {elementFullNames} from '../../../_properties/elementFullNames';

import {TimesheetEntryService} from '../_services/timesheetEntry.service';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {TimesheetEntryOutputModel} from '../_models/TimesheetEntryOutputModel';

import {FilterService} from '../../../filters/_services/filter.service';
import {DropdownService} from '../../../../ui/dropdown/dropdown.service';
import {NavigationService} from '../../../_services/navigation.service';
import {TableControlService} from '../../../../ui/table-control/table-control.service';

import {NgRedux} from '@angular-redux/store';
import {select} from '@angular-redux/store';

import {AppState} from '../../../../../state/AppState';
import {ErpTimesheetsTimesheetEntrySlicePaths as paths} from '../../../../../state/slices/erpTimesheets.timesheetEntry.slice';

import {TableControlData} from '../../../../ui/table-control/TableControlData';
import {FilterData} from '../../../filters/_models/FilterData';
import {PaginationIn} from '../../../../ui/pagination/PaginationIn';

@Component({
  selector: 'app-timesheetentry-list',
  templateUrl: './timesheetEntry-list.component.html',
  styleUrls: ['./timesheetEntry-list.component.css']
})
export class TimesheetEntryListComponent implements OnInit, AfterViewInit, OnDestroy {

  @select([elementFullNames.erpTimesheets.timesheetEntry, paths.list_requests_GET])
  getRequest$: Observable<any>;
  timesheetEntries: TimesheetEntryOutputModel[] = [];
  links: LinksModel;
  page: PageModel;
  private getRequestSubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheetEntry, paths.filterData])
  filterData$: Observable<FilterData>;
  filterData: FilterData;
  private filterDataSubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheetEntry, paths.list_pageUrl])
  pageUrl$: Observable<string>;
  pageUrl: string;
  private pageUrlSubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheetEntry, paths.list_requests_DELETE])
  deleteRequest$: Observable<any>;
  private deleteRequestSubscription: Subscription;

  defaultHideFields = [
    this.fields.externalId,
    // custom
    this.fields.timesheet,
  ];

  sortFields = [
    this.fields.externalId,
    this.fields.date,
    this.fields.hours,
    this.fields.status,
    this.fields.description,
  ];

  // For pagination
  paginationIn = PaginationIn.ELEMENT_LIST;

  // For expand/collapse filter
  isFilterExpand = true;

  /* For multiple item selections:start */
  selectedItems = new Map<string, TimesheetEntryOutputModel>();
  numberOfSelectedItems = new Subject<number>();
  numberOfSelectedItems$ = this.numberOfSelectedItems.asObservable();
  numberOfSelectedItemsSubscription: Subscription;
  /* For multiple item selections:end */

  displayDialog = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private ngRedux: NgRedux<AppState>,
    private filterService: FilterService,
    private dropdownService: DropdownService,
    private tableControlService: TableControlService,
    private navigationService: NavigationService,
    private timesheetEntryService: TimesheetEntryService,
  ) {
  }

  ngOnInit(): void {
    this.filterDataSubscription = this.filterData$.subscribe(filterData => {
      this.filterData = filterData;
    });

    this.getRequestSubscription = this.getRequest$.subscribe(requestData => {
      if (requestData) {
        this.timesheetEntries = requestData.timesheetEntries ?? [];
        this.links = requestData.links ?? null;
        this.page = requestData.page ?? null;
      }
    });

    this.pageUrlSubscription = this.pageUrl$.subscribe(pageUrl => {
      this.pageUrl = pageUrl ?? null;
    });

    this.deleteRequestSubscription = this.deleteRequest$.subscribe(requestData => {
      if (requestData && requestData.success) {
        this.selectedItems.clear();
        this.numberOfSelectedItems.next(0);
        this.getTimesheetEntries();
      }
    });

    this.subscribeToNumberOfSelectedItems();

    this.filterService.displayNewFilter(this.elementFullName, this.filterData);
    this.updateHeaderBreadcrumb();
    if (this.timesheetEntryService.isChildElement) {
      // collapse filter by default for sublist
      this.toggleFilterDisplay();
    }

    this.getTimesheetEntries();
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.getRequestSubscription.unsubscribe();
    this.filterDataSubscription.unsubscribe();
    this.pageUrlSubscription.unsubscribe();
    this.deleteRequestSubscription.unsubscribe();
    this.numberOfSelectedItemsSubscription.unsubscribe();
    this.timesheetEntryService.saveFilterData(this.filterService.getFilter(this.elementFullName));
    this.filterService.clearAllFilters();
    this.timesheetEntryService.removeAlert();
    this.timesheetEntryService.removeRequestData(paths.list_requests_GET);
    this.timesheetEntryService.removeRequestData(paths.list_requests_DELETE);
  }

  /* ----- GETTERS ----- */

  get elementFullName(): string {
    return this.timesheetEntryService.elementFullName;
  }

  get fields(): { [key: string]: string } {
    return this.timesheetEntryService.fields;
  }

  get areActionButtonsDisplayed(): boolean {
    return this.selectedItems.size !== 0;
  }

  // For table control functionality
  get tableData(): TableControlData {
    return this.tableControlService.tables.get(this.elementFullName);
  }

  get isRowDisplayed(): boolean {
    return this.tableData.isRowDisplayed();
  }

  /* ----- ON INIT ----- */

  private updateHeaderBreadcrumb(): void {
    this.navigationService.changeRouter(this.router);
  }

  private subscribeToNumberOfSelectedItems(): void {
    // For multiple item selections
    this.numberOfSelectedItemsSubscription = this.numberOfSelectedItems$.subscribe(numberOfSelectedItems => {
      ElementListUtils.toggleActionButtons(numberOfSelectedItems);
    });
  }

  /* ----- HTTP REQUESTS -----*/

  private getTimesheetEntries(): void {
    if (this.pageUrl) {
      this.timesheetEntryService.fetchItemsByPageUrl(this.pageUrl);
      return;
    }

    this.timesheetEntryService.fetchItems();
  }

  delete(externalId: string): void {
    this.timesheetEntryService.listRemoveItem(externalId, this.timesheetEntryService.generateBackUrl());
  }

  deleteItems(): void {
    const externalIds = [];
    this.selectedItems.forEach((item, externalId) => externalIds.push(externalId));
    if (externalIds.length === 1) {
      return this.delete(externalIds[0]);
    }
    this.timesheetEntryService.removeItems(externalIds, this.timesheetEntryService.generateBackUrl());
  }

  /* ----- NAVIGATION -----*/

  goToTimesheetEntryDetailPage(externalId: string): void {
    this.router.navigate([this.timesheetEntryService.elementHomeUrl + '/', externalId]);
  }

  goToAddTimesheetEntryForm(): void {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  goToEditTimesheetEntryForm(externalId: string): void {
    this.router.navigate(['edit', {externalId}], {relativeTo: this.route});
  }

  /* ----- PAGINATION ----- */

  changePage(pageUrl: string): void {
    if (pageUrl) {
      this.timesheetEntryService.fetchItemsByPageUrl(pageUrl);
    }
  }

  /* ----- FILTER ----- */

  /* toggle filter display */

  toggleFilterDisplay(): void {
    this.isFilterExpand = !this.isFilterExpand;
    const mainContent = document.querySelector('.element-list') as HTMLDivElement;

    if (this.isFilterExpand) {
      mainContent.style.gridTemplateColumns = 'minmax(0, 7fr) minmax(0, 3fr)';
    } else {
      mainContent.style.gridTemplateColumns = 'calc(100% - 20px) 20px';
    }
  }

  /* ----- SORT ----- */

  isSortable(fieldName: string): boolean {
    return this.sortFields.findIndex(field => field === fieldName) !== -1;
  }

  onChangeSortData(isSortDataChange: boolean): void {
    if (isSortDataChange) {
      this.timesheetEntryService.fetchItems();
    }
  }

  /* ----- MULTIPLE SELECTION ----- */

  editItem(): void {
    this.goToEditTimesheetEntryForm(this.selectedItems.keys().next().value);
  }

  toggleSelectItem(item: TimesheetEntryOutputModel): void {
    if (!this.selectedItems.has(item.externalId)) {
      this.addSelectedItem(item);
    } else {
      this.removeSelectedItem(item);
    }
  }

  addSelectedItem(item: TimesheetEntryOutputModel): void {
    this.selectedItems.set(item.externalId, item);
    this.numberOfSelectedItems.next(this.selectedItems.size);
  }

  private removeSelectedItem(item: TimesheetEntryOutputModel): void {
    this.selectedItems.delete(item.externalId);
    this.numberOfSelectedItems.next(this.selectedItems.size);
  }

  isRowSelected(externalId: string): boolean {
    return this.selectedItems.has(externalId);
  }

  resetSelectedItems(): void {
    this.selectedItems.clear();
    this.numberOfSelectedItems.next(this.selectedItems.size);
  }

  allItemsSelected(): boolean {
    return this.selectedItems.size === Math.min(this.page.totalElements, this.page.size);
  }

  toggleSelectAllItems(): void {
    if (this.selectedItems.size < Math.min(this.page.totalElements, this.page.size)) {
      this.resetSelectedItems();
      this.timesheetEntries.forEach(timesheetEntry => {
        this.selectedItems.set(timesheetEntry.externalId, timesheetEntry);
        this.numberOfSelectedItems.next(this.selectedItems.size);
      });
    } else {
      this.resetSelectedItems();
    }
  }

  /* ----- CUSTOM PAGESIZE AND COLUMNS ----- */

  onChangePageSize(isPageSizeChange: boolean): void {
    if (isPageSizeChange) {
      this.timesheetEntryService.fetchItems();
    }
  }

  isColumnShown(name: string): boolean {
    return this.tableData.columnDisplay.get(name);
  }

  /* ----- DIALOG ----- */

  showDialog(): void {
    this.displayDialog = true;
  }

  hideDialog(): void {
    this.displayDialog = false;
  }

  handleDialogEventEmitter(confirm: boolean): void {
    this.hideDialog();
    if (confirm) {
      this.deleteItems();
    }
  }

  /* ----- MISC ----- */

  trackByExternalId(index: number, object: any): string {
    return object.externalId;
  }

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
