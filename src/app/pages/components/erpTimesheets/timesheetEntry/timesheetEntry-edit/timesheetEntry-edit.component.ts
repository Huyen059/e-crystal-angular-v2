import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {Location} from '@angular/common';

import {Utils} from '../../../_utils/Utils';
import {elementFullNames} from '../../../_properties/elementFullNames';

import {TimesheetEntryService} from '../_services/timesheetEntry.service';
import {TimesheetEntry} from '../_models/TimesheetEntry';
import {TimesheetEntryOutputModel} from '../_models/TimesheetEntryOutputModel';
import {TimesheetEntryPutInputMapper} from '../_models/TimesheetEntryPutInputMapper';
import {TimesheetEntryMapper} from '../_models/TimesheetEntryMapper';

import {FilterService} from '../../../filters/_services/filter.service';
import {DropdownService} from '../../../../ui/dropdown/dropdown.service';
import {NavigationService} from '../../../_services/navigation.service';

import {select} from '@angular-redux/store';
import {ErpTimesheetsTimesheetEntrySlicePaths as paths} from '../../../../../state/slices/erpTimesheets.timesheetEntry.slice';

import {DropdownIn} from '../../../../ui/dropdown/_models/DropdownIn';

// custom
import {TimesheetOutputModel} from '../../timesheet/_models/TimesheetOutputModel';
import {TimesheetOutputMapper} from '../../timesheet/_models/TimesheetOutputMapper';

@Component({
  selector: 'app-timesheetentry-edit',
  templateUrl: './timesheetEntry-edit.component.html',
  styleUrls: ['./timesheetEntry-edit.component.css']
})
export class TimesheetEntryEditComponent implements OnInit, AfterViewInit, OnDestroy {
  private selectedId: string;
  formModel: TimesheetEntry = new TimesheetEntry();

  /* For dropdown:start */
  displayDropdown = false;
  dropdownIn = DropdownIn.ELEMENT_FORM;
  dropdownElements = [
    {
      elementFullName: 'erpTimesheets.timesheet',
      searchBy: 'name',
      required: true,
    },
    {
      elementFullName: 'enterprise.employee',
      searchBy: 'name',
      required: false,
    },
    {
      elementFullName: 'enterprise.project',
      searchBy: 'name',
      required: true,
    },
    {
      elementFullName: 'enterprise.subproject',
      searchBy: 'name',
      required: false,
    },
    {
      elementFullName: 'enterprise.workingMonth',
      searchBy: 'name',
      required: false,
    },
  ];
  /* For dropdown:end */

  isFilterMenuShown = false;

  @select([elementFullNames.erpTimesheets.timesheetEntry, paths.edit_requests_GET, 'timesheetEntry'])
  timesheetEntry$: Observable<TimesheetEntryOutputModel>;
  timesheetEntrySubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheetEntry, paths.edit_requests_UPDATE]) updateRequest$: Observable<any>;
  private updateRequestSubscription: Subscription;

  // Custom
  timesheetOriginalValue: TimesheetOutputModel;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private timesheetEntryService: TimesheetEntryService,
    private filterService: FilterService,
    private dropdownService: DropdownService,
    private navigationService: NavigationService,
  ) {
    this.dropdownElements = [
      {
        elementFullName: 'erpTimesheets.timesheet',
        searchBy: 'name',
        required: true,
      },
      {
        elementFullName: 'enterprise.project',
        searchBy: 'name',
        required: true,
      },
      {
        elementFullName: 'enterprise.subproject',
        searchBy: 'name',
        required: false,
      },
    ];
  }

  /* ----- LIFE CYCLES ----- */

  ngOnInit(): void {
    this.updateHeaderBreadcrumb();

    this.getTimesheetEntry();
    this.checkFormModelValueRetrieved().then(() => {
      this.displayDropdown = true;
    });

    this.timesheetEntrySubscription = this.timesheetEntry$.subscribe(timesheetEntry => {
      if (timesheetEntry) {
        this.formModel = TimesheetEntryMapper.map(timesheetEntry);

        // custom: start
        if (!this.timesheetOriginalValue) {
          this.timesheetOriginalValue = TimesheetOutputMapper.map(timesheetEntry.timesheet);
        }
        // custom: end
      }
    });

    this.updateRequestSubscription = this.updateRequest$.subscribe(requestData => {
      if (requestData && requestData.backUrl) {
        this.router.navigate([requestData.backUrl]);
      }
    });
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.timesheetEntrySubscription.unsubscribe();
    this.updateRequestSubscription.unsubscribe();
    this.filterService.clearAllFilters();
    this.timesheetEntryService.removeAlert();
    this.timesheetEntryService.removeRequestData(paths.edit_requests_UPDATE);
    this.timesheetEntryService.removeRequestData(paths.edit_requests_GET);
  }

  /* ----- GETTERS ----- */

  get elementFullName(): string {
    return this.timesheetEntryService.elementFullName;
  }

  get fields(): { [key: string]: string } {
    return this.timesheetEntryService.fields;
  }

  /* ----- ON INIT ----- */

  private updateHeaderBreadcrumb(): void {
    this.navigationService.changeRouter(this.router);
  }

  /* ----- HTTP REQUESTS ----- */

  private getTimesheetEntry(): void {
    this.selectedId = this.route.snapshot.paramMap.get('externalId');
    this.timesheetEntryService.editFetchItem(this.selectedId);
  }

  saveTimesheetEntry(): void {
    const backUrl = this.timesheetEntryService.generateBackUrl();
    this.timesheetEntryService.updateItem(TimesheetEntryPutInputMapper.map(this.formModel), backUrl);
  }

  /* ----- CLOSE FORM ----- */

  closeForm(): void {
    this.location.back();
  }

  /* ----- DROPDOWN INITIAL VALUE ----- */

  getDropdownInitialValue(elementFullName: string): any {
    return this.formModel[this.getElementName(elementFullName)];
  }

  /* ----- DROPDOWN -----*/

  dropdownInputData(elementFullName: string): any {
    return this.dropdownElements.find(dropdown => dropdown.elementFullName === elementFullName);
  }

  chosenItemModifiedHandler(chosenItem: any, elementFullName: string): void {
    const fieldName = elementFullName.split('.')[1];
    this.formModel[fieldName] = chosenItem;

    // custom
    if (elementFullName === 'erpTimesheets.timesheet' && chosenItem.externalId !== this.timesheetOriginalValue.externalId) {
      this.setTimesheetEntryDisplayDate(chosenItem.month, chosenItem.year);
    }

    // custom
    if (elementFullName === 'enterprise.project') {
      const subprojectDropdownData = this.dropdownService.getDropdownData('enterprise.subproject', this.dropdownIn);
      if (subprojectDropdownData) {
        const value = chosenItem ? chosenItem.externalId : 'mustChooseAProject';
        subprojectDropdownData.queries.project = value;
      }
    }
  }

  /* ----- CUSTOM ----- */

  // when select a certain timesheet, display the corresponding month, year in the input for date
  private setTimesheetEntryDisplayDate(month: number, year: number): void {
    const today = new Date();
    const timesheetDate = new Date();
    timesheetDate.setFullYear(year, month - 1, 1);
    if (timesheetDate > today) {
      this.formModel.date = Utils.convertDateToValueAttribute(timesheetDate);
    } else {
      this.formModel.date = Utils.convertDateToValueAttribute(today);
    }
  }

  /* ----- MISC ----- */

  getElementName(elementFullName: string): string {
    return elementFullName.split('.')[1];
  }

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }

  checkFormModelValueRetrieved = async () => {
    while (!this.formModel.externalId) {
      await new Promise(resolve => {
        requestAnimationFrame(resolve);
      });
    }
    return true;
  }
}
