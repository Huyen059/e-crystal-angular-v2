import {TimesheetReferenceModel} from '../../timesheet/_models/TimesheetReferenceModel';
import {ProjectReferenceModel} from '../../../enterprise/project/_models/ProjectReferenceModel';
import {SubprojectReferenceModel} from '../../../enterprise/subproject/_models/SubprojectReferenceModel';
import {WorkingMonthReferenceModel} from '../../../enterprise/workingMonth/_models/WorkingMonthReferenceModel';
import {EmployeeReferenceModel} from '../../../enterprise/employee/_models/EmployeeReferenceModel';

export class TimesheetEntryOutputModel {
  date: string;
  hours: number;
  description: string;
  status: string;
  externalId: string;
  timesheet: TimesheetReferenceModel;
  project: ProjectReferenceModel;
  subproject: SubprojectReferenceModel;
  employee: EmployeeReferenceModel;
  name: string;
  // custom
  workingMonth: WorkingMonthReferenceModel;
  numberOfExpenses: number;
}
