import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {TimesheetEntryOutputListModel} from './TimesheetEntryOutputListModel';

export class PaginatedResponse {
  constructor(
    public _embedded: TimesheetEntryOutputListModel = new TimesheetEntryOutputListModel(),
    public _links: LinksModel = null,
    public _page: PageModel = null
  ) {
  }
}
