export class TimesheetEntryReferenceModel {
  resourceUri: string;
  name: string;
  externalId: string;
}
