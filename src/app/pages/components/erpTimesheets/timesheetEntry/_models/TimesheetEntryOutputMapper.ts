import {TimesheetEntryReferenceModel} from './TimesheetEntryReferenceModel';
import {TimesheetEntryOutputModel} from './TimesheetEntryOutputModel';

export class TimesheetEntryOutputMapper {
  static map(timesheetEntryReferenceModel: TimesheetEntryReferenceModel): TimesheetEntryOutputModel {
    const timesheetEntryOutputModel = new TimesheetEntryOutputModel();
    timesheetEntryOutputModel.name = timesheetEntryReferenceModel.name ? timesheetEntryReferenceModel.name : null;
    timesheetEntryOutputModel.externalId = timesheetEntryReferenceModel.externalId ? timesheetEntryReferenceModel.externalId : null;

    return timesheetEntryOutputModel;
  }
}
