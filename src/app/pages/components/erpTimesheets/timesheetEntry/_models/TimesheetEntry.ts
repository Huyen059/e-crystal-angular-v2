import {ProjectOutputModel} from '../../../enterprise/project/_models/ProjectOutputModel';
import {SubprojectOutputModel} from '../../../enterprise/subproject/_models/SubprojectOutputModel';
import {TimesheetOutputModel} from '../../timesheet/_models/TimesheetOutputModel';
import {EmployeeOutputModel} from '../../../enterprise/employee/_models/EmployeeOutputModel';
import {WorkingMonthOutputModel} from '../../../enterprise/workingMonth/_models/WorkingMonthOutputModel';


export class TimesheetEntry {
  constructor(
    public date: string = null,
    public hours: number = null,
    public description: string = null,
    public status: string = null,
    public externalId: string = null,
    public timesheet: TimesheetOutputModel = null,
    public project: ProjectOutputModel = null,
    public subproject: SubprojectOutputModel = null,
    public name: string = null,
    public workingMonth: WorkingMonthOutputModel = null,
    public employee: EmployeeOutputModel = null,
    // custom
    public startDate: string = null,
    public endDate: string = null,
    public includeWeekend: boolean = null,
    public hoursByDay: number[] = null,
  ) {  }
}
