import {TimesheetEntry} from './TimesheetEntry';
import {TimesheetEntryPostInputModel} from './TimesheetEntryPostInputModel';

export class TimesheetEntryPostInputMapper {
  static map(timesheetEntry: TimesheetEntry): TimesheetEntryPostInputModel {
    const timesheetEntryPostInputModel = new TimesheetEntryPostInputModel();
    timesheetEntryPostInputModel.date = timesheetEntry.date ? timesheetEntry.date : null;
    timesheetEntryPostInputModel.hours = timesheetEntry.hours ? timesheetEntry.hours : null;
    timesheetEntryPostInputModel.description = timesheetEntry.description ? timesheetEntry.description : null;
    timesheetEntryPostInputModel.status = timesheetEntry.status ? timesheetEntry.status : null;
    timesheetEntryPostInputModel.externalId = timesheetEntry.externalId ? timesheetEntry.externalId : null;
    timesheetEntryPostInputModel.timesheet = timesheetEntry.timesheet ? timesheetEntry.timesheet.externalId : null;
    timesheetEntryPostInputModel.project = timesheetEntry.project ? timesheetEntry.project.externalId : null;
    timesheetEntryPostInputModel.subproject = timesheetEntry.subproject ? timesheetEntry.subproject.externalId : null;
    // custom
    timesheetEntryPostInputModel.startDate = timesheetEntry.startDate ? timesheetEntry.startDate : null;
    timesheetEntryPostInputModel.endDate = timesheetEntry.endDate ? timesheetEntry.endDate : null;
    timesheetEntryPostInputModel.includeWeekend = timesheetEntry.includeWeekend;
    timesheetEntryPostInputModel.hoursByDay = timesheetEntry.hoursByDay ? timesheetEntry.hoursByDay : null;

    // custom
    timesheetEntryPostInputModel.hours = timesheetEntry.hours ? timesheetEntry.hours : 0;

    return timesheetEntryPostInputModel;
  }
}
