import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimesheetRemarkRoutingModule } from './timesheetRemark-routing.module';
import { TimesheetRemarkWorkspaceComponent } from './timesheetRemark-workspace/timesheetRemark-workspace.component';
import { TimesheetRemarkListComponent } from './timesheetRemark-list/timesheetRemark-list.component';
import {FiltersModule} from '../../filters/filters.module';
import {UiModule} from '../../../ui/ui.module';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    TimesheetRemarkWorkspaceComponent,
    TimesheetRemarkListComponent,
  ],
    imports: [
      CommonModule,
      FiltersModule,
      FormsModule,
      UiModule,
      TimesheetRemarkRoutingModule,
    ]
})
export class TimesheetRemarkModule { }
