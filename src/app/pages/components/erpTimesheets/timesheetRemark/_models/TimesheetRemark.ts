import {TimesheetOutputModel} from '../../timesheet/_models/TimesheetOutputModel';

export class TimesheetRemark {
  constructor(
    public remark: string = null,
    public externalId: string = null,
    public timesheet: TimesheetOutputModel = null,
  ) {  }

}
