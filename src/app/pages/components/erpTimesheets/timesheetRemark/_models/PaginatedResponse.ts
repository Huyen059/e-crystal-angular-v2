import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {TimesheetRemarkOutputListModel} from './TimesheetRemarkOutputListModel';

export class PaginatedResponse {
  constructor(
    public _embedded: TimesheetRemarkOutputListModel = new TimesheetRemarkOutputListModel(),
    public _links: LinksModel = null,
    public _page: PageModel = null
  ) {
  }
}
