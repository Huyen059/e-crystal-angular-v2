export class TimesheetRemarkPostInputModel {
  constructor(
    public remark: string = null,
    public externalId: string = null,
    public timesheet: string = null,
  ) {  }

}
