import {TimesheetRemarkOutputModel} from './TimesheetRemarkOutputModel';

export class TimesheetRemarkOutputListModel {
  constructor(
    public timesheetRemarks: TimesheetRemarkOutputModel[] = []
  ) {
  }
}
