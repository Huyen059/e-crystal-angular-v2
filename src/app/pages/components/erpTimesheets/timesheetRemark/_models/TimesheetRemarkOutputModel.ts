import {TimesheetReferenceModel} from '../../timesheet/_models/TimesheetReferenceModel';

export class TimesheetRemarkOutputModel {
  remark: string;
  externalId: string;
  timesheet: TimesheetReferenceModel;
}
