export class TimesheetRemarkPutInputModel {
  constructor(
    public remark: string = null,
    public externalId: string = null,
    public timesheet: string = null,
  ) {  }

}
