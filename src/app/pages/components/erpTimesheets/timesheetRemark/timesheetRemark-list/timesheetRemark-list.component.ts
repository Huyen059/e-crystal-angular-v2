import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subject, Subscription} from 'rxjs';

import {Utils} from '../../../_utils/Utils';
import {ElementListUtils} from '../../../_utils/ElementListUtils';
import {elementFullNames} from '../../../_properties/elementFullNames';

import {TimesheetRemarkService} from '../_services/timesheetRemark.service';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {TimesheetRemarkOutputModel} from '../_models/TimesheetRemarkOutputModel';

import {FilterService} from '../../../filters/_services/filter.service';
import {DropdownService} from '../../../../ui/dropdown/dropdown.service';
import {NavigationService} from '../../../_services/navigation.service';
import {TableControlService} from '../../../../ui/table-control/table-control.service';

import {NgRedux} from '@angular-redux/store';
import {select} from '@angular-redux/store';

import {AppState} from '../../../../../state/AppState';
import {ErpTimesheetsTimesheetRemarkSlicePaths as paths} from '../../../../../state/slices/erpTimesheets.timesheetRemark.slice';

import {TableControlData} from '../../../../ui/table-control/TableControlData';
import {FilterData} from '../../../filters/_models/FilterData';
import {PaginationIn} from '../../../../ui/pagination/PaginationIn';

@Component({
  selector: 'app-timesheetremark-list',
  templateUrl: './timesheetRemark-list.component.html',
  styleUrls: ['./timesheetRemark-list.component.css']
})
export class TimesheetRemarkListComponent implements OnInit, AfterViewInit, OnDestroy {

  @select([elementFullNames.erpTimesheets.timesheetRemark, paths.list_requests_GET])
  getRequest$: Observable<any>;
  timesheetRemarks: TimesheetRemarkOutputModel[] = [];
  links: LinksModel;
  page: PageModel;
  private getRequestSubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheetRemark, paths.filterData])
  filterData$: Observable<FilterData>;
  filterData: FilterData;
  private filterDataSubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheetRemark, paths.list_pageUrl])
  pageUrl$: Observable<string>;
  pageUrl: string;
  private pageUrlSubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheetRemark, paths.list_requests_DELETE])
  deleteRequest$: Observable<any>;
  private deleteRequestSubscription: Subscription;

  defaultHideFields = [
    this.fields.externalId,
  ];

  sortFields = [
    this.fields.externalId,
    this.fields.remark,
  ];

  // For pagination
  paginationIn = PaginationIn.ELEMENT_LIST;

  // For expand/collapse filter
  isFilterExpand = true;

  /* For multiple item selections:start */
  selectedItems = new Map<string, TimesheetRemarkOutputModel>();
  numberOfSelectedItems = new Subject<number>();
  numberOfSelectedItems$ = this.numberOfSelectedItems.asObservable();
  numberOfSelectedItemsSubscription: Subscription;
  /* For multiple item selections:end */

  displayDialog = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private ngRedux: NgRedux<AppState>,
    private filterService: FilterService,
    private dropdownService: DropdownService,
    private tableControlService: TableControlService,
    private navigationService: NavigationService,
    private timesheetRemarkService: TimesheetRemarkService,
  ) {
  }

  ngOnInit(): void {
    this.filterDataSubscription = this.filterData$.subscribe(filterData => {
      this.filterData = filterData;
    });

    this.getRequestSubscription = this.getRequest$.subscribe(requestData => {
      if (requestData) {
        this.timesheetRemarks = requestData.timesheetRemarks ?? [];
        this.links = requestData.links ?? null;
        this.page = requestData.page ?? null;
      }
    });

    this.pageUrlSubscription = this.pageUrl$.subscribe(pageUrl => {
      this.pageUrl = pageUrl ?? null;
    });

    this.deleteRequestSubscription = this.deleteRequest$.subscribe(requestData => {
      if (requestData && requestData.success) {
        this.selectedItems.clear();
        this.numberOfSelectedItems.next(0);
        this.getTimesheetRemarks();
      }
    });

    this.subscribeToNumberOfSelectedItems();

    this.filterService.displayNewFilter(this.elementFullName, this.filterData);
    this.updateHeaderBreadcrumb();
    if (this.timesheetRemarkService.isChildElement) {
      // collapse filter by default for sublist
      this.toggleFilterDisplay();
    }

    this.getTimesheetRemarks();
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.getRequestSubscription.unsubscribe();
    this.filterDataSubscription.unsubscribe();
    this.pageUrlSubscription.unsubscribe();
    this.deleteRequestSubscription.unsubscribe();
    this.numberOfSelectedItemsSubscription.unsubscribe();
    this.timesheetRemarkService.saveFilterData(this.filterService.getFilter(this.elementFullName));
    this.filterService.clearAllFilters();
    this.timesheetRemarkService.removeAlert();
    this.timesheetRemarkService.removeRequestData(paths.list_requests_GET);
    this.timesheetRemarkService.removeRequestData(paths.list_requests_DELETE);
  }

  /* ----- GETTERS ----- */

  get elementFullName(): string {
    return this.timesheetRemarkService.elementFullName;
  }

  get fields(): { [key: string]: string } {
    return this.timesheetRemarkService.fields;
  }

  get areActionButtonsDisplayed(): boolean {
    return this.selectedItems.size !== 0;
  }

  // For table control functionality
  get tableData(): TableControlData {
    return this.tableControlService.tables.get(this.elementFullName);
  }

  get isRowDisplayed(): boolean {
    return this.tableData.isRowDisplayed();
  }

  /* ----- ON INIT ----- */

  private updateHeaderBreadcrumb(): void {
    this.navigationService.changeRouter(this.router);
  }

  private subscribeToNumberOfSelectedItems(): void {
    // For multiple item selections
    this.numberOfSelectedItemsSubscription = this.numberOfSelectedItems$.subscribe(numberOfSelectedItems => {
      ElementListUtils.toggleActionButtons(numberOfSelectedItems);
    });
  }

  /* ----- HTTP REQUESTS -----*/

  private getTimesheetRemarks(): void {
    if (this.pageUrl) {
      this.timesheetRemarkService.fetchItemsByPageUrl(this.pageUrl);
      return;
    }

    this.timesheetRemarkService.fetchItems();
  }

  delete(externalId: string): void {
    this.timesheetRemarkService.listRemoveItem(externalId, this.timesheetRemarkService.generateBackUrl());
  }

  deleteItems(): void {
    const externalIds = [];
    this.selectedItems.forEach((item, externalId) => externalIds.push(externalId));
    if (externalIds.length === 1) {
      return this.delete(externalIds[0]);
    }
    this.timesheetRemarkService.removeItems(externalIds, this.timesheetRemarkService.generateBackUrl());
  }

  /* ----- NAVIGATION -----*/

  goToTimesheetRemarkDetailPage(externalId: string): void {
    this.router.navigate([this.timesheetRemarkService.elementHomeUrl + '/', externalId]);
  }

  goToAddTimesheetRemarkForm(): void {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  goToEditTimesheetRemarkForm(externalId: string): void {
    this.router.navigate(['edit', {externalId}], {relativeTo: this.route});
  }

  /* ----- PAGINATION ----- */

  changePage(pageUrl: string): void {
    if (pageUrl) {
      this.timesheetRemarkService.fetchItemsByPageUrl(pageUrl);
    }
  }

  /* ----- FILTER ----- */

  /* toggle filter display */

  toggleFilterDisplay(): void {
    this.isFilterExpand = !this.isFilterExpand;
    const mainContent = document.querySelector('.element-list') as HTMLDivElement;

    if (this.isFilterExpand) {
      mainContent.style.gridTemplateColumns = 'minmax(0, 7fr) minmax(0, 3fr)';
    } else {
      mainContent.style.gridTemplateColumns = 'calc(100% - 20px) 20px';
    }
  }

  /* ----- SORT ----- */

  isSortable(fieldName: string): boolean {
    return this.sortFields.findIndex(field => field === fieldName) !== -1;
  }

  onChangeSortData(isSortDataChange: boolean): void {
    if (isSortDataChange) {
      this.timesheetRemarkService.fetchItems();
    }
  }

  /* ----- MULTIPLE SELECTION ----- */

  editItem(): void {
    this.goToEditTimesheetRemarkForm(this.selectedItems.keys().next().value);
  }

  toggleSelectItem(item: TimesheetRemarkOutputModel): void {
    if (!this.selectedItems.has(item.externalId)) {
      this.addSelectedItem(item);
    } else {
      this.removeSelectedItem(item);
    }
  }

  addSelectedItem(item: TimesheetRemarkOutputModel): void {
    this.selectedItems.set(item.externalId, item);
    this.numberOfSelectedItems.next(this.selectedItems.size);
  }

  private removeSelectedItem(item: TimesheetRemarkOutputModel): void {
    this.selectedItems.delete(item.externalId);
    this.numberOfSelectedItems.next(this.selectedItems.size);
  }

  isRowSelected(externalId: string): boolean {
    return this.selectedItems.has(externalId);
  }

  resetSelectedItems(): void {
    this.selectedItems.clear();
    this.numberOfSelectedItems.next(this.selectedItems.size);
  }

  allItemsSelected(): boolean {
    return this.selectedItems.size === Math.min(this.page.totalElements, this.page.size);
  }

  toggleSelectAllItems(): void {
    if (this.selectedItems.size < Math.min(this.page.totalElements, this.page.size)) {
      this.resetSelectedItems();
      this.timesheetRemarks.forEach(timesheetRemark => {
        this.selectedItems.set(timesheetRemark.externalId, timesheetRemark);
        this.numberOfSelectedItems.next(this.selectedItems.size);
      });
    } else {
      this.resetSelectedItems();
    }
  }

  /* ----- CUSTOM PAGESIZE AND COLUMNS ----- */

  onChangePageSize(isPageSizeChange: boolean): void {
    if (isPageSizeChange) {
      this.timesheetRemarkService.fetchItems();
    }
  }

  isColumnShown(name: string): boolean {
    return this.tableData.columnDisplay.get(name);
  }

  /* ----- DIALOG ----- */

  showDialog(): void {
    this.displayDialog = true;
  }

  hideDialog(): void {
    this.displayDialog = false;
  }

  handleDialogEventEmitter(confirm: boolean): void {
    this.hideDialog();
    if (confirm) {
      this.deleteItems();
    }
  }

  /* ----- MISC ----- */

  trackByExternalId(index: number, object: any): string {
    return object.externalId;
  }

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
