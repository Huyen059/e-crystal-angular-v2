import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {TimesheetRemarkWorkspaceComponent} from './timesheetRemark-workspace/timesheetRemark-workspace.component';
import {TimesheetRemarkListComponent} from './timesheetRemark-list/timesheetRemark-list.component';
import {Utils} from '../../_utils/Utils';

const routes: Routes = [
  {path: '', component: TimesheetRemarkWorkspaceComponent,
    data: {name: Utils.getElementDisplayName('erpTimesheets.timesheetRemark'), isMainWorkspace: true},
    children: [
      {path: '', component: TimesheetRemarkListComponent, data: {name: '', isMainList: true}},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimesheetRemarkRoutingModule { }
