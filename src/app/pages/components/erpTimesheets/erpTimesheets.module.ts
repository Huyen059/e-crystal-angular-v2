import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ErpTimesheetsRoutingModule} from './erpTimesheets-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ErpTimesheetsRoutingModule
  ]
})
export class ErpTimesheetsModule {
}
