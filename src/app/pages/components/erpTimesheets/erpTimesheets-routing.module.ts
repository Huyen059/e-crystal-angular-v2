import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {path: '', redirectTo: 'timesheet', pathMatch: 'full'},
  {path: 'timesheet', loadChildren: () => import('./timesheet/timesheet.module').then(m => m.TimesheetModule)},
  {path: 'timesheetEntry', loadChildren: () => import('./timesheetEntry/timesheetEntry.module').then(m => m.TimesheetEntryModule)},
  {path: 'timesheetRemark', loadChildren: () => import('./timesheetRemark/timesheetRemark.module').then(m => m.TimesheetRemarkModule)},
  {path: 'expense', loadChildren: () => import('./expense/expense.module').then(m => m.ExpenseModule)},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErpTimesheetsRoutingModule {
}
