import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {ExpenseService} from '../_services/expense.service';
import {AlertService} from '../../../../ui/alert/_services/alert.service';
import {TableControlService} from '../../../../ui/table-control/table-control.service';
import {SortService} from '../../../../ui/sort/sort.service';
import {FilterService} from '../../../filters/_services/filter.service';
import {select} from '@angular-redux/store';
import {Observable, Subscription} from 'rxjs';
import {HierarchySlice} from '../../../../../state/slices/hierarchy.slice';
import {elementFullNames} from '../../../_properties/elementFullNames';
import {ErpTimesheetsExpenseSlicePaths as paths} from '../../../../../state/slices/erpTimesheets.expense.slice';

@Component({
  selector: 'app-expense-workspace',
  templateUrl: './expense-workspace.component.html',
  styleUrls: ['./expense-workspace.component.css']
})
export class ExpenseWorkspaceComponent implements OnInit, OnDestroy {

  private routeSubscription: Subscription;
  isSubworkspace: boolean;

  @select() hierarchy$: Observable<HierarchySlice>;
  hierarchySubscription: Subscription;

  @select([elementFullNames.erpTimesheets.expense, paths.alert])
  alert$: Observable<{ type: string, messageTranslationKey: string }>;
  private alertSubscription: Subscription;

  @select([elementFullNames.erpTimesheets.expense, paths.queries])
  queries$: Observable<object>;
  private queriesSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private expenseService: ExpenseService,
    private tableControlService: TableControlService,
    private filterService: FilterService,
    private sortService: SortService,
    private alertService: AlertService,
  ) {  }

  get elementFullName(): string {
    return this.expenseService.elementFullName;
  }

  ngOnInit(): void {
    this.expenseService.addSliceToReduxStore();

    this.alertService.clear();
    this.tableControlService.add(this.elementFullName);
    this.sortService.add(this.elementFullName);

    this.routeSubscription = this.route.data.subscribe(data => {
      this.expenseService.isChildElement = data.isMainWorkspace === undefined;
      this.isSubworkspace = this.expenseService.isChildElement;
    });

    this.extractParentData();
    this.alertSubscription = this.alert$.subscribe(alert => {
      if (alert) {
        this.alertService.alertByType(alert.type, alert.messageTranslationKey, {autoClose: true});
      }
    });

    this.queriesSubscription = this.queries$.subscribe(queries => {
      this.expenseService.queries = queries ?? null;
    });
  }

  ngOnDestroy(): void {
    this.filterService.clearAllFilters();
    this.hierarchySubscription.unsubscribe();
    this.alertSubscription.unsubscribe();
    this.routeSubscription.unsubscribe();
    this.queriesSubscription.unsubscribe();
  }

  private extractParentData(): void {
    this.hierarchySubscription = this.hierarchy$.subscribe(hierarchy => {
      if (hierarchy.currentLevel > 0) {
        const inChildrenList = hierarchy[hierarchy.currentLevel].childrenFullNames
          .findIndex(childFullName => childFullName === this.elementFullName) > -1;
        if (inChildrenList) {
          this.expenseService.parentFullName = hierarchy[hierarchy.currentLevel].elementFullName;
          this.expenseService.parentValue = hierarchy[hierarchy.currentLevel].value;
        }
      } else {
        this.expenseService.parentFullName = null;
        this.expenseService.parentValue = null;
      }
    });
  }
}
