import {ExpenseOutputModel} from './ExpenseOutputModel';

export class ExpenseOutputListModel {
  constructor(
    public expenses: ExpenseOutputModel[] = []
  ) {
  }
}
