export class ExpensePutInputModel {
  constructor(
    public cost: number = null,
    public description: string = null,
    public status: string = null,
    public externalId: string = null,
    public timesheetEntry: string = null,
  ) {  }
}
