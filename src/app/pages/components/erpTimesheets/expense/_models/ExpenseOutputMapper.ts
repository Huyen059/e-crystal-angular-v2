import {ExpenseReferenceModel} from './ExpenseReferenceModel';
import {ExpenseOutputModel} from './ExpenseOutputModel';

export class ExpenseOutputMapper {
  static map(expenseReferenceModel: ExpenseReferenceModel): ExpenseOutputModel {
    const expenseOutputModel = new ExpenseOutputModel();
    // expenseOutputModel.name = expenseReferenceModel.name ? expenseReferenceModel.name : null;
    expenseOutputModel.externalId = expenseReferenceModel.externalId ? expenseReferenceModel.externalId : null;

    return expenseOutputModel;
  }
}
