import {TimesheetEntryReferenceModel} from '../../timesheetEntry/_models/TimesheetEntryReferenceModel';

export class ExpenseOutputModel {
  cost: number;
  description: string;
  status: string;
  externalId: string;
  timesheetEntry: TimesheetEntryReferenceModel;
}
