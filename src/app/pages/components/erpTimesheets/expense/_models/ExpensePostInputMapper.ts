import {Expense} from './Expense';
import {ExpensePostInputModel} from './ExpensePostInputModel';

export class ExpensePostInputMapper {
  static map(expense: Expense): ExpensePostInputModel {
    const expensePostInputModel = new ExpensePostInputModel();
    expensePostInputModel.cost = expense.cost ? expense.cost : null;
    expensePostInputModel.description = expense.description ? expense.description : null;
    expensePostInputModel.status = expense.status ? expense.status : null;
    expensePostInputModel.externalId = expense.externalId ? expense.externalId : null;
    expensePostInputModel.timesheetEntry = expense.timesheetEntry ? expense.timesheetEntry.externalId : null;

    return expensePostInputModel;
  }

}
