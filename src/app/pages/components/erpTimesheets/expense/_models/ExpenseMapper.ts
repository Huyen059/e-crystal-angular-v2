import {ExpenseOutputModel} from './ExpenseOutputModel';
import {Expense} from './Expense';
import {TimesheetEntryOutputMapper} from '../../timesheetEntry/_models/TimesheetEntryOutputMapper';

export class ExpenseMapper {
  static map(expenseOutputModel: ExpenseOutputModel): Expense {
    const expense = new Expense();
    expense.cost = expenseOutputModel.cost ? expenseOutputModel.cost : null;
    expense.description = expenseOutputModel.description ? expenseOutputModel.description : null;
    expense.status = expenseOutputModel.status ? expenseOutputModel.status : null;
    expense.externalId = expenseOutputModel.externalId ? expenseOutputModel.externalId : null;
    expense.timesheetEntry = expenseOutputModel.timesheetEntry ? TimesheetEntryOutputMapper.map(expenseOutputModel.timesheetEntry) : null;

    return expense;
  }
}
