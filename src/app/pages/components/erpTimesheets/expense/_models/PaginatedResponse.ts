import {ExpenseOutputListModel} from './ExpenseOutputListModel';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';

export class PaginatedResponse {
  constructor(
    public _embedded: ExpenseOutputListModel = new ExpenseOutputListModel(),
    public _links: LinksModel = null,
    public _page: PageModel = null
  ) {
  }
}
