import {Expense} from './Expense';
import {ExpensePutInputModel} from './ExpensePutInputModel';

export class ExpensePutInputMapper {
  static map(expense: Expense): ExpensePutInputModel {
    const expensePutInputModel = new ExpensePutInputModel();
    expensePutInputModel.cost = expense.cost ? expense.cost : null;
    expensePutInputModel.description = expense.description ? expense.description : null;
    expensePutInputModel.status = expense.status ? expense.status : null;
    expensePutInputModel.externalId = expense.externalId ? expense.externalId : null;
    expensePutInputModel.timesheetEntry = expense.timesheetEntry ? expense.timesheetEntry.externalId : null;

    return expensePutInputModel;
  }

}
