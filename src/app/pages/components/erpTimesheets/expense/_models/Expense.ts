import {TimesheetEntryOutputModel} from '../../timesheetEntry/_models/TimesheetEntryOutputModel';

export class Expense {
  constructor(
    public cost: number = null,
    public description: string = null,
    public status: string = null,
    public externalId: string = null,
    public timesheetEntry: TimesheetEntryOutputModel = null,
  ) {  }
}
