import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExpenseRoutingModule } from './expense-routing.module';
import { ExpenseWorkspaceComponent } from './expense-workspace/expense-workspace.component';
import { ExpenseListComponent } from './expense-list/expense-list.component';
import {FormsModule} from '@angular/forms';
import {UiModule} from '../../../ui/ui.module';


@NgModule({
  declarations: [
    ExpenseWorkspaceComponent,
    ExpenseListComponent,
  ],
    imports: [
      CommonModule,
      FormsModule,
      UiModule,
      ExpenseRoutingModule,
    ]
})
export class ExpenseModule { }
