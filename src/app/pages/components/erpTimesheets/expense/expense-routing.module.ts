import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {Utils} from '../../_utils/Utils';
import {ExpenseWorkspaceComponent} from './expense-workspace/expense-workspace.component';
import {ExpenseListComponent} from './expense-list/expense-list.component';

const routes: Routes = [
  {
    path: '', component: ExpenseWorkspaceComponent,
    data: {name: Utils.getElementDisplayName('erpTimesheets.expense'), isMainWorkspace: true},
    children: [
      {path: '', component: ExpenseListComponent, data: {name: '', isMainList: true}},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExpenseRoutingModule { }
