import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {Location} from '@angular/common';

import {Utils} from '../../../_utils/Utils';
import {elementFullNames} from '../../../_properties/elementFullNames';

import {TimesheetService} from '../_services/timesheet.service';
import {Timesheet} from '../_models/Timesheet';
import {TimesheetPostInputMapper} from '../_models/TimesheetPostInputMapper';

import {FilterService} from '../../../filters/_services/filter.service';
import {DropdownService} from '../../../../ui/dropdown/dropdown.service';
import {NavigationService} from '../../../_services/navigation.service';

import {select} from '@angular-redux/store';
import {ErpTimesheetsTimesheetSlicePaths as paths} from '../../../../../state/slices/erpTimesheets.timesheet.slice';

import {DropdownIn} from '../../../../ui/dropdown/_models/DropdownIn';

@Component({
  selector: 'app-timesheet-new',
  templateUrl: './timesheet-new.component.html',
  styleUrls: ['./timesheet-new.component.css']
})
export class TimesheetNewComponent implements OnInit, AfterViewInit, OnDestroy {
  formModel: Timesheet = new Timesheet();

  /* For dropdown:start */
  dropdownIn = DropdownIn.ELEMENT_FORM;
  dropdownElements = {
    'enterprise.employee': true,
  };

  employeeDropdown = {
    elementFullName: 'enterprise.employee',
    searchBy: 'name',
    required: true,
    hasExtraInfo: true,
    showExtraInfo: false,
    errors: []
  };
  /* For dropdown:end */

  /* For normal input fields:start */
  inputFields = {
    month: true,
    year: true
  };

  monthInput = {
    elementFullName: this.timesheetService.elementFullName,
    fieldName: 'month',
    required: true,
    hasExtraInfo: true,
    showExtraInfo: false,
    errors: []
  };

  yearInput = {
    elementFullName: this.timesheetService.elementFullName,
    fieldName: 'year',
    required: true,
    hasExtraInfo: true,
    showExtraInfo: false,
    errors: []
  };
  /* For normal input fields:end */

  buttons: {[key: string]: {[key: string]: any}} = {};

  isFilterMenuShown = false;

  @select([elementFullNames.erpTimesheets.timesheet, paths.new_requests_CREATE])
  createRequest$: Observable<any>;
  private createRequestSubscription: Subscription;

  constructor(
    private router: Router,
    private location: Location,
    private timesheetService: TimesheetService,
    private filterService: FilterService,
    private dropdownService: DropdownService,
    private navigationService: NavigationService
  ) {

  }

  /* ----- LIFE CYCLES ----- */

  ngOnInit(): void {
    this.loadButtonsData();
    this.updateHeaderBreadcrumb();

    this.createRequestSubscription = this.createRequest$.subscribe(requestData => {
      if (requestData && requestData.backUrl) {
        this.router.navigate([requestData.backUrl]);
      }
    });

    this.retrieveLinkFieldInitialValues();
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.createRequestSubscription.unsubscribe();
    this.filterService.clearAllFilters();
    this.timesheetService.removeAlert();
    this.timesheetService.removeRequestData(paths.new_requests_CREATE);
  }

  /* ----- GETTERS ----- */

  get elementFullName(): string {
    return this.timesheetService.elementFullName;
  }

  get fields(): { [key: string]: string } {
    return this.timesheetService.fields;
  }

  /* ----- ON INIT ----- */

  retrieveLinkFieldInitialValues(): any {
    if (this.timesheetService.parentFullName) {
      const parentFieldName = this.getElementName(this.timesheetService.parentFullName);
      this.formModel[parentFieldName] = this.timesheetService.parentValue;
    }

    Object.keys(this.dropdownElements).forEach(fullName => {
      const fieldName = this.getElementName(fullName);
      if (this.timesheetService.queries && this.timesheetService.queries[fieldName]) {
        this.formModel[fieldName] = this.timesheetService.queries[fieldName];
      }
    });
  }

  private updateHeaderBreadcrumb(): void {
    this.navigationService.changeRouter(this.router);
  }

  /* ----- HTTP REQUESTS ----- */

  addTimesheet(): void {
    const backUrl = this.timesheetService.generateBackUrl();
    this.timesheetService.addItem(TimesheetPostInputMapper.map(this.formModel), backUrl);
  }

  /* ----- CLOSE FORM ----- */

  closeForm(): void {
    this.location.back();
  }

  /* ----- DROPDOWN INITIAL VALUE ----- */

  getDropdownInitialValue(elementFullName: string): any {
    return this.formModel[this.getElementName(elementFullName)];
  }

  /* ----- DROPDOWN -----*/

  chosenItemModifiedHandler(chosenItem: any, elementFullName: string): void {
    const fieldName = elementFullName.split('.')[1];
    this.formModel[fieldName] = chosenItem;
  }

  /* ----- NORMAL INPUT FIELD -----*/

  toggleExtraInfo(inputField: any): void {
    switch (inputField.fieldName) {
      case this.monthInput.fieldName:
        this.monthInput.showExtraInfo = !this.monthInput.showExtraInfo;
        break;
      case this.yearInput.fieldName:
        this.yearInput.showExtraInfo = !this.yearInput.showExtraInfo;
        break;
    }
  }

  /* ----- BUTTONS -----*/
  private loadButtonsData(): void {
    this.buttons = {
      cancel: {
        name: 'CANCEL',
        buttonType: 'button',
        buttonCssClasses: 'ns-button ns-button--cancel ns-button__inline',
        iconCssClasses: '',
        translationKeyForButtonText: 'cancel',
      },
      add: {
        name: 'ADD',
        buttonType: 'button',
        buttonCssClasses: 'ns-button ns-button--primary ns-button__inline',
        iconCssClasses: '',
        translationKeyForButtonText: 'add',
      }
    };
  }

  getShowButtonCondition(buttonName: string): boolean {
    switch (buttonName) {
      case this.buttons.cancel.name:
      case this.buttons.add.name:
      default:
        return true;
    }
  }

  onButtonClick(data: any, buttonName: string): void {
    switch (buttonName) {
      case this.buttons.cancel.name:
        this.closeForm();
        break;
      case this.buttons.add.name:
        this.addTimesheet();
        break;
      default:
        return;
    }
  }

  /* ----- MISC ----- */

  getElementName(elementFullName: string): string {
    return elementFullName.split('.')[1];
  }

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
