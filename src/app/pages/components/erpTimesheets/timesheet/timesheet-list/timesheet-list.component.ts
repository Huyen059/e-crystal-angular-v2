import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subject, Subscription} from 'rxjs';

import {Utils} from '../../../_utils/Utils';
import {ElementListUtils} from '../../../_utils/ElementListUtils';
import {elementFullNames} from '../../../_properties/elementFullNames';

import {TimesheetService} from '../_services/timesheet.service';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {TimesheetOutputModel} from '../_models/TimesheetOutputModel';

import {FilterService} from '../../../filters/_services/filter.service';
import {DropdownService} from '../../../../ui/dropdown/dropdown.service';
import {NavigationService} from '../../../_services/navigation.service';
import {TableControlService} from '../../../../ui/table-control/table-control.service';

import {NgRedux} from '@angular-redux/store';
import {select} from '@angular-redux/store';

import {AppState} from '../../../../../state/AppState';
import {ErpTimesheetsTimesheetSlicePaths as paths} from '../../../../../state/slices/erpTimesheets.timesheet.slice';

import {TableControlData} from '../../../../ui/table-control/TableControlData';
import {FilterData} from '../../../filters/_models/FilterData';
import {PaginationIn} from '../../../../ui/pagination/PaginationIn';

@Component({
  selector: 'app-timesheet-list',
  templateUrl: './timesheet-list.component.html',
  styleUrls: ['./timesheet-list.component.css']
})
export class TimesheetListComponent implements OnInit, AfterViewInit, OnDestroy {

  @select([elementFullNames.erpTimesheets.timesheet, paths.list_requests_GET])
  getRequest$: Observable<any>;
  timesheets: TimesheetOutputModel[] = [];
  links: LinksModel;
  page: PageModel;
  private getRequestSubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheet, paths.filterData])
  filterData$: Observable<FilterData>;
  filterData: FilterData;
  private filterDataSubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheet, paths.list_pageUrl])
  pageUrl$: Observable<string>;
  pageUrl: string;
  private pageUrlSubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheet, paths.list_requests_DELETE])
  deleteRequest$: Observable<any>;
  private deleteRequestSubscription: Subscription;

  defaultHideFields = [
    this.fields.externalId,
    // custom
    this.fields.name,
  ];

  sortFields = [
    this.fields.externalId,
    this.fields.name,
    this.fields.month,
    this.fields.year,
    this.fields.status,
  ];

  buttons: {[key: string]: {[key: string]: any}} = {};

  // For pagination
  paginationIn = PaginationIn.ELEMENT_LIST;

  // For expand/collapse filter
  isFilterExpand = true;

  /* For multiple item selections:start */
  selectedItems = new Map<string, TimesheetOutputModel>();
  numberOfSelectedItems = new Subject<number>();
  numberOfSelectedItems$ = this.numberOfSelectedItems.asObservable();
  numberOfSelectedItemsSubscription: Subscription;
  /* For multiple item selections:end */

  displayDialog = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private ngRedux: NgRedux<AppState>,
    private filterService: FilterService,
    private dropdownService: DropdownService,
    private tableControlService: TableControlService,
    private navigationService: NavigationService,
    private timesheetService: TimesheetService,
  ) {
  }

  ngOnInit(): void {
    this.loadButtonsData();
    this.filterDataSubscription = this.filterData$.subscribe(filterData => {
      this.filterData = filterData;
    });

    this.getRequestSubscription = this.getRequest$.subscribe(requestData => {
      if (requestData) {
        this.timesheets = requestData.timesheets ?? [];
        this.links = requestData.links ?? null;
        this.page = requestData.page ?? null;
      }
    });

    this.pageUrlSubscription = this.pageUrl$.subscribe(pageUrl => {
      this.pageUrl = pageUrl ?? null;
    });

    this.deleteRequestSubscription = this.deleteRequest$.subscribe(requestData => {
      if (requestData && requestData.success) {
        this.selectedItems.clear();
        this.numberOfSelectedItems.next(0);
        this.getTimesheets();
      }
    });

    this.subscribeToNumberOfSelectedItems();

    this.filterService.displayNewFilter(this.elementFullName, this.filterData);
    this.updateHeaderBreadcrumb();
    if (this.timesheetService.isChildElement) {
      // collapse filter by default for sublist
      this.toggleFilterDisplay();
    }

    this.getTimesheets();
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.getRequestSubscription.unsubscribe();
    this.filterDataSubscription.unsubscribe();
    this.pageUrlSubscription.unsubscribe();
    this.deleteRequestSubscription.unsubscribe();
    this.numberOfSelectedItemsSubscription.unsubscribe();
    this.timesheetService.saveFilterData(this.filterService.getFilter(this.elementFullName));
    this.filterService.clearAllFilters();
    this.timesheetService.removeAlert();
    this.timesheetService.removeRequestData(paths.list_requests_GET);
    this.timesheetService.removeRequestData(paths.list_requests_DELETE);
  }

  /* ----- GETTERS ----- */

  get elementFullName(): string {
    return this.timesheetService.elementFullName;
  }

  get fields(): { [key: string]: string } {
    return this.timesheetService.fields;
  }

  get areActionButtonsDisplayed(): boolean {
    return this.selectedItems.size !== 0;
  }

  // For table control functionality
  get tableData(): TableControlData {
    return this.tableControlService.tables.get(this.elementFullName);
  }

  get isRowDisplayed(): boolean {
    return this.tableData.isRowDisplayed();
  }

  // Custom: disable actions for non-admin users
  get isAdmin(): boolean {
    return Utils.isAdmin();
  }

  /* ----- ON INIT ----- */

  private updateHeaderBreadcrumb(): void {
    this.navigationService.changeRouter(this.router);
  }

  private subscribeToNumberOfSelectedItems(): void {
    // For multiple item selections
    this.numberOfSelectedItemsSubscription = this.numberOfSelectedItems$.subscribe(numberOfSelectedItems => {
      ElementListUtils.toggleActionButtons(numberOfSelectedItems);
    });
  }

  /* ----- HTTP REQUESTS -----*/

  private getTimesheets(): void {
    if (this.pageUrl) {
      this.timesheetService.fetchItemsByPageUrl(this.pageUrl);
      return;
    }

    this.timesheetService.fetchItems();
  }

  delete(externalId: string): void {
    this.timesheetService.listRemoveItem(externalId, this.timesheetService.generateBackUrl());
  }

  deleteItems(): void {
    const externalIds = [];
    this.selectedItems.forEach((item, externalId) => externalIds.push(externalId));
    if (externalIds.length === 1) {
      return this.delete(externalIds[0]);
    }
    this.timesheetService.removeItems(externalIds, this.timesheetService.generateBackUrl());
  }

  /* ----- NAVIGATION -----*/

  goToTimesheetDetailPage(externalId: string): void {
    this.router.navigate([this.timesheetService.elementHomeUrl + '/', externalId]);
  }

  goToAddTimesheetForm(): void {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  goToEditTimesheetForm(externalId: string): void {
    this.router.navigate(['edit', {externalId}], {relativeTo: this.route});
  }

  /* ----- CLICK HANDLERS -----*/

  handleClickOnRow(timesheet: any): void {
    this.toggleSelectItem(timesheet);
  }

  /* ----- PAGINATION ----- */

  changePage(pageUrl: string): void {
    if (pageUrl) {
      this.timesheetService.fetchItemsByPageUrl(pageUrl);
      this.selectedItems.clear();
    }
  }

  /* ----- FILTER ----- */

  /* toggle filter display */

  toggleFilterDisplay(): void {
    this.isFilterExpand = !this.isFilterExpand;
    const mainContent = document.querySelector('.element-list') as HTMLDivElement;

    if (this.isFilterExpand) {
      mainContent.style.gridTemplateColumns = 'minmax(0, 7fr) minmax(0, 3fr)';
    } else {
      mainContent.style.gridTemplateColumns = 'calc(100% - 20px) 20px';
    }
  }

  /* ----- SORT ----- */

  isSortable(fieldName: string): boolean {
    return this.sortFields.findIndex(field => field === fieldName) !== -1;
  }

  onChangeSortData(isSortDataChange: boolean): void {
    if (isSortDataChange) {
      this.timesheetService.fetchItems();
    }
  }

  /* ----- MULTIPLE SELECTION ----- */

  viewDetail(): void {
    this.goToTimesheetDetailPage(this.selectedItems.keys().next().value);
  }

  editItem(): void {
    this.goToEditTimesheetForm(this.selectedItems.keys().next().value);
  }

  toggleSelectItem(item: TimesheetOutputModel): void {
    if (!this.selectedItems.has(item.externalId)) {
      this.addSelectedItem(item);
    } else {
      this.removeSelectedItem(item);
    }
  }

  addSelectedItem(item: TimesheetOutputModel): void {
    this.selectedItems.set(item.externalId, item);
    this.numberOfSelectedItems.next(this.selectedItems.size);
  }

  private removeSelectedItem(item: TimesheetOutputModel): void {
    this.selectedItems.delete(item.externalId);
    this.numberOfSelectedItems.next(this.selectedItems.size);
  }

  isRowSelected(externalId: string): boolean {
    return this.selectedItems.has(externalId);
  }

  resetSelectedItems(): void {
    this.selectedItems.clear();
    this.numberOfSelectedItems.next(this.selectedItems.size);
  }

  allItemsSelected(): boolean {
    return this.selectedItems.size === Math.min(this.page.totalElements, this.page.size);
  }

  toggleSelectAllItems(): void {
    if (this.selectedItems.size < Math.min(this.page.totalElements, this.page.size)) {
      this.resetSelectedItems();
      this.timesheets.forEach(timesheet => {
        this.selectedItems.set(timesheet.externalId, timesheet);
        this.numberOfSelectedItems.next(this.selectedItems.size);
      });
    } else {
      this.resetSelectedItems();
    }
  }

  /* ----- CUSTOM PAGESIZE AND COLUMNS ----- */

  onChangePageSize(isPageSizeChange: boolean): void {
    if (isPageSizeChange) {
      this.timesheetService.fetchItems();
    }
  }

  isColumnShown(name: string): boolean {
    return this.tableData.columnDisplay.get(name);
  }

  /* ----- DIALOG ----- */

  showDialog(): void {
    this.displayDialog = true;
  }

  hideDialog(): void {
    this.displayDialog = false;
  }

  handleDialogEventEmitter(confirm: boolean): void {
    this.hideDialog();
    if (confirm) {
      this.deleteItems();
    }
  }

  /* ----- BUTTONS -----*/

  private loadButtonsData(): void {
    this.buttons = {
      new: {
        name: 'NEW',
        buttonType: 'button',
        buttonCssClasses: 'ns-button ns-button--primary ns-button__inline',
        iconCssClasses: 'fas fa-plus',
        translationKeyForButtonText: '',
      },
      filter: {
        name: 'FILTER',
        buttonType: 'button',
        buttonCssClasses: 'ns-button ns-button--small--x ns-button--border ns-button__inline',
        iconCssClasses: 'fas fa-filter',
        translationKeyForButtonText: '',
      },
      detail: {
        name: 'DETAIL',
        buttonType: 'button',
        buttonCssClasses: 'ns-button ns-button--small--x ns-button--border ns-button__inline',
        iconCssClasses: 'fas fa-info',
        translationKeyForButtonText: '',
      },
      edit: {
        name: 'EDIT',
        buttonType: 'button',
        buttonCssClasses: 'ns-button ns-button--small--x ns-button--border ns-button__inline',
        iconCssClasses: 'fas fa-pen',
        translationKeyForButtonText: '',
      },
      delete: {
        name: 'DELETE',
        buttonType: 'button',
        buttonCssClasses: 'ns-button ns-button--small--x ns-button--border ns-button__inline',
        iconCssClasses: 'fas fa-trash',
        translationKeyForButtonText: '',
      }
    };

    // custom
    this.buttons.new.iconCssClasses = '';
    this.buttons.new.translationKeyForButtonText = 'new';
  }

  getShowButtonCondition(buttonName: string): boolean {
    switch (buttonName) {
      case this.buttons.new.name:
        // custom
        return this.isAdmin;
      case this.buttons.filter.name:
        // custom
        return this.timesheets.length > 0;
      case this.buttons.detail.name:
      case this.buttons.edit.name:
        // custom
        return this.isAdmin && this.selectedItems.size === 1;
      case this.buttons.delete.name:
        // custom
        return this.isAdmin && this.selectedItems.size >= 1;
      default:
        return true;
    }
  }

  onButtonClick(data: any, buttonName: string): void {
    switch (buttonName) {
      case this.buttons.new.name:
        this.goToAddTimesheetForm();
        break;
      case this.buttons.filter.name:
        this.toggleFilterDisplay();
        break;
      case this.buttons.detail.name:
        this.viewDetail();
        break;
      case this.buttons.edit.name:
        this.editItem();
        break;
      case this.buttons.delete.name:
        this.showDialog();
        break;
      default:
        return;
    }
  }

  /* ----- MISC ----- */

  trackByExternalId(index: number, object: any): string {
    return object.externalId;
  }

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
