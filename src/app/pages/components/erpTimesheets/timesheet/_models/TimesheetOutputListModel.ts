import {TimesheetOutputModel} from './TimesheetOutputModel';

export class TimesheetOutputListModel {
  constructor(
    public timesheets: TimesheetOutputModel[] = []
  ) {
  }
}
