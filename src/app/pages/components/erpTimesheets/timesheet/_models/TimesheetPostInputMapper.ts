import {Timesheet} from './Timesheet';
import {TimesheetPostInputModel} from './TimesheetPostInputModel';

export class TimesheetPostInputMapper {
  static map(timesheet: Timesheet): TimesheetPostInputModel {
    const timesheetPostInputModel = new TimesheetPostInputModel();
    timesheetPostInputModel.externalId = timesheet.externalId ? timesheet.externalId : null;
    timesheetPostInputModel.month = timesheet.month ? timesheet.month : null;
    timesheetPostInputModel.status = timesheet.status ? timesheet.status : null;
    timesheetPostInputModel.year = timesheet.year ? timesheet.year : null;
    timesheetPostInputModel.employee = timesheet.employee ? timesheet.employee.externalId : null;
    timesheetPostInputModel.name = timesheet.name ? timesheet.name : null;

    return timesheetPostInputModel;
  }

}
