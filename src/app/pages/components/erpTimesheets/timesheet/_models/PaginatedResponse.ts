import {TimesheetOutputListModel} from './TimesheetOutputListModel';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';

export class PaginatedResponse {
  constructor(
    public _embedded: TimesheetOutputListModel = new TimesheetOutputListModel(),
    public _links: LinksModel = null,
    public _page: PageModel = null
  ) {
  }
}
