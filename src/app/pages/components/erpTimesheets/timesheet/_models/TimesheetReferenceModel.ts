export class TimesheetReferenceModel {
  resourceUri: string;
  name: string;
  externalId: string;
}
