import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {AppState} from '../../../../../state/AppState';
import {ErpTimesheetsTimesheetActionFactory} from '../../../../../state/actions/erpTimesheets.timesheet.actions';
import {AlertService} from '../../../../ui/alert/_services/alert.service';
import {TableControlService} from '../../../../ui/table-control/table-control.service';
import {SortService} from '../../../../ui/sort/sort.service';
import {appPageSize} from '../../../_properties/appPageSize';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';
import {FilterData} from '../../../filters/_models/FilterData';
import {elementFullNames} from '../../../_properties/elementFullNames';
import {ErpTimesheetsTimesheetSlicePaths as paths} from '../../../../../state/slices/erpTimesheets.timesheet.slice';
import {epicMiddleware, reducers} from '../../../../../state/store';
import {combineReducers} from 'redux';
import {erpTimesheetsTimesheetReducer} from '../../../../../state/reducers/erpTimesheets.timesheet.reducer';
import {erpTimesheetsTimesheetEpics} from '../../../../../state/epics/erpTimesheets.timesheet.epics';


@Injectable({
  providedIn: 'root'
})
export class TimesheetService {
  elementHomeUrl = '/e-crystal/erpTimesheets/timesheet';
  elementFullName = elementFullNames.erpTimesheets.timesheet;

  isChildElement: boolean;
  parentFullName: string;
  parentValue: any;

  queries: object;

  // List of field names, each field is displayed as a column
  fields = {
    externalId: 'externalId',
    name: 'name',
    employee: 'employee',
    month: 'month',
    year: 'year',
    status: 'status',
    totalHours: 'totalHours',
    totalManDays: 'totalManDays',
    numberOfTimesheetEntries: 'numberOfTimesheetEntries',
    numberOfTimesheetRemarks: 'numberOfTimesheetRemarks',
  };

  constructor(
    private ngRedux: NgRedux<AppState>,
    private alertService: AlertService,
    private tableControlService: TableControlService,
    private sortService: SortService,
  ) {}

  /* ----- REDUX ----- */

  addSliceToReduxStore(): void {
    if (!reducers[this.elementFullName]) {
      reducers[this.elementFullName] = erpTimesheetsTimesheetReducer;
      this.ngRedux.replaceReducer(combineReducers(reducers));
      Object.values(erpTimesheetsTimesheetEpics).forEach(epic => epicMiddleware.run(epic));
    }
  }

  fetchItems(fetchItemConditions: object = {}): void {
    if (this.queries) {
      fetchItemConditions = {...this.queries, ...fetchItemConditions};
    }

    let pageSize = appPageSize;
    if (this.tableControlService.tables.get(this.elementFullName)) {
      pageSize = this.tableControlService.tables.get(this.elementFullName).pageSize;
    }
    // @ts-ignore
    fetchItemConditions.pagesize = pageSize;

    const sortBy = this.getSortBy(this.elementFullName);
    const isAscending = this.getIsAscending(this.elementFullName);
    if (!sortBy) {
      // @ts-ignore
      delete fetchItemConditions.sortBy;
      // @ts-ignore
      delete fetchItemConditions.isAscending;
    } else {
      // @ts-ignore
      fetchItemConditions.sortBy = sortBy;
      // @ts-ignore
      fetchItemConditions.isAscending = isAscending;
    }

    if (this.isChildElement) {
      this.checkParentDataUpdated().then(() => {
        const fieldName = this.parentFullName.split('.')[1];
        fetchItemConditions[fieldName] = this.parentValue;
        this.ngRedux.dispatch(ErpTimesheetsTimesheetActionFactory.listItemsFetchRequestSent(
          fetchItemConditions));
      });
      return;
    }

    this.ngRedux.dispatch(ErpTimesheetsTimesheetActionFactory.listItemsFetchRequestSent(
      fetchItemConditions));
  }

  fetchItemsByPageUrl(pageUrl: string): void {
    this.ngRedux.dispatch(ErpTimesheetsTimesheetActionFactory.listItemsFetchByPageUrlRequestSent(pageUrl));
  }

  listRemoveItem(itemExternalId: string, backUrl: string): void {
    this.ngRedux.dispatch(ErpTimesheetsTimesheetActionFactory.listItemRemoveRequestSent(itemExternalId, backUrl));
  }

  removeItems(externalIds: any[], backUrl: string): void {
    this.ngRedux.dispatch(ErpTimesheetsTimesheetActionFactory.listItemsRemoveRequestSent(externalIds, backUrl));
  }

  detailFetchItem(externalId: string): void {
    this.ngRedux.dispatch(ErpTimesheetsTimesheetActionFactory.detailItemFetchRequestSent(externalId));
  }

  detailRemoveItem(itemExternalId: string, backUrl: string): void {
    this.ngRedux.dispatch(ErpTimesheetsTimesheetActionFactory.detailItemRemoveRequestSent(itemExternalId, backUrl));
  }

  addItem(item: any, backUrl: string): void {
    this.ngRedux.dispatch(ErpTimesheetsTimesheetActionFactory.newItemAddRequestSent(item, backUrl));
  }

  editFetchItem(externalId: string): void {
    this.ngRedux.dispatch(ErpTimesheetsTimesheetActionFactory.editItemFetchRequestSent(externalId));
  }

  updateItem(item: any, backUrl: string): void {
    this.ngRedux.dispatch(ErpTimesheetsTimesheetActionFactory.editItemUpdateRequestSent(item, backUrl));
  }

  saveFilterData(filterData: FilterData): void {
    this.ngRedux.dispatch(ErpTimesheetsTimesheetActionFactory.listFetchFiltersSaved(filterData));
  }

  removeAlert(): void {
    if (this.ngRedux.getState()[this.elementFullName][paths.alert]) {
      this.ngRedux.dispatch(ErpTimesheetsTimesheetActionFactory.alertRemoved());
    }
  }

  removeRequestData(path: string): void {
    if (this.ngRedux.getState()[this.elementFullName][path]) {
      this.ngRedux.dispatch(ErpTimesheetsTimesheetActionFactory.requestDataRemoved(path));
    }
  }

  /* ----- SUPPORT METHODS ----- */

  generateBackUrl(): string {
    if (!this.parentFullName) {
      return this.elementHomeUrl;
    }
    let url = '/e-crystal/';
    const segments = [];
    segments.push(...this.parentFullName.split('.'));
    segments.push(this.parentValue.externalId);
    segments.push(this.elementFullName.split('.')[1]);
    url += segments.join('/');
    return url;
  }

  getSortBy(elementFullName: string): string {
    const sortData = this.sortService.allSortData.get(elementFullName);

    if (sortData && sortData.sortBy) {
      return sortData.sortBy;
    }

    return null;
  }

  getIsAscending(elementFullName: string): boolean {
    const sortData = this.sortService.allSortData.get(elementFullName);

    if (sortData && isNotNullOrUndefined(sortData.isAscending)) {
      return sortData.isAscending;
    }

    return null;
  }

  checkParentDataUpdated = async () => {
    while (!this.parentFullName) {
      await new Promise(resolve => {
        requestAnimationFrame(resolve);
      });
    }

    return true;
  }
}
