import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

import {Utils} from '../../../_utils/Utils';
import {elementFullNames} from '../../../_properties/elementFullNames';

import {TimesheetService} from '../_services/timesheet.service';
import {TimesheetOutputModel} from '../_models/TimesheetOutputModel';

import {NavigationService} from '../../../_services/navigation.service';
import {AlertService} from '../../../../ui/alert/_services/alert.service';
import {HierarchyService} from '../../../_services/hierarchy.service';

import {NgRedux, select} from '@angular-redux/store';
import {
  ErpTimesheetsTimesheetSlicePaths as paths,
  ErpTimesheetsTimesheetSlicePaths
} from '../../../../../state/slices/erpTimesheets.timesheet.slice';

import {AppState} from '../../../../../state/AppState';

@Component({
  selector: 'app-timesheet-detail',
  templateUrl: './timesheet-detail.component.html',
  styleUrls: ['./timesheet-detail.component.css']
})
export class TimesheetDetailComponent implements OnInit, AfterViewInit, OnDestroy {
  private selectedId: string;

  @select([
    elementFullNames.erpTimesheets.timesheet,
    paths.detail_requests_GET,
    'timesheet'
  ])
  timesheet$: Observable<TimesheetOutputModel>;
  timesheet: TimesheetOutputModel;
  private timesheetSubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheet, paths.detail_requests_DELETE])
  deleteRequest$: Observable<any>;
  private deleteRequestSubscription: Subscription;

  showTabControl = false;
  showTabContent = false;

  dataChildren = {
    timesheetEntry: 'erpTimesheets.timesheetEntry',
    timesheetRemark: 'erpTimesheets.timesheetRemark',
  };

  displayDialog = false;

  buttons: {[key: string]: {[key: string]: any}} = {};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private timesheetService: TimesheetService,
    private navigationService: NavigationService,
    private alertService: AlertService,
    private ngRedux: NgRedux<AppState>,
    private hierarchyService: HierarchyService,
  ) {
    // CUSTOM
    // Use this if the change in child components affect detail of the parent
    // In this case, when a new timesheet entry is created, the details of parent (totalHours, totalManDays) will change
    // and we want the change to be displayed
    this.navigationService.reload$.subscribe(() => {
      this.getTimesheet();
    });
  }

  /* ----- LIFE CYCLES ----- */

  ngOnInit(): void {
    this.loadButtonsData();
    this.timesheetSubscription = this.timesheet$.subscribe(timesheet => this.timesheet = timesheet);
    this.deleteRequestSubscription = this.deleteRequest$.subscribe(requestData => {
      if (requestData && requestData.success) {
        this.router.navigate([requestData.backUrl]);
      }
    });
    this.getTimesheet();
    this.updateHeaderBreadcrumb();
    this.checkTimesheet().then(() => {
      this.hierarchyService.addLevel(this.elementFullName, this.timesheet, this.dataChildrenFullNames);
      this.showTabContent = true;
    });
    this.checkTabElements().then(() => this.showTabControl = true);
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.timesheetSubscription.unsubscribe();
    this.deleteRequestSubscription.unsubscribe();

    this.dataChildrenFullNames.forEach(childFullName => {
      if (this.ngRedux.getState()[childFullName]) {
        const actionType = childFullName + '/DATA_REMOVED';
        this.ngRedux.dispatch({type: actionType});
      }
    });

    this.hierarchyService.removeAllLevels();
    this.timesheetService.removeAlert();
    this.timesheetService.removeRequestData(paths.detail_requests_GET);
    this.timesheetService.removeRequestData(paths.detail_requests_DELETE);
  }

  /* ----- GETTERS ----- */

  get elementFullName(): string {
    return this.timesheetService.elementFullName;
  }

  get numberOfDataChildren(): number {
    return Object.keys(this.dataChildren).length;
  }

  get fields(): { [key: string]: string } {
    return this.timesheetService.fields;
  }

  get dataChildrenFullNames(): string[] {
    return Object.values(this.dataChildren);
  }

  /* -----ON INIT ----- */

  private updateHeaderBreadcrumb(): void {
    this.navigationService.changeRouter(this.router);
  }

  /* ----- HTTP REQUESTS ----- */

  private getTimesheet(): void {
    this.selectedId = this.route.snapshot.paramMap.get('externalId');
    this.timesheetService.detailFetchItem(this.selectedId);
  }

  delete(externalId: string): void {
    const backUrl = this.timesheetService.generateBackUrl();
    this.timesheetService.detailRemoveItem(externalId, backUrl);
  }

  /* ----- NAVIGATION ----- */

  goToEditTimesheetForm(externalId: string): void {
    this.router.navigate([this.timesheetService.elementHomeUrl + '/edit', {externalId}]);
  }

  /* ----- DIALOG ----- */

  showDialog(): void {
    this.displayDialog = true;
  }

  hideDialog(): void {
    this.displayDialog = false;
  }

  handleDialogEventEmitter(confirm: boolean): void {
    this.hideDialog();
    if (confirm) {
      this.delete(this.selectedId);
    }
  }

  /* ----- BUTTONS -----*/
  private loadButtonsData(): void {
    this.buttons = {
      edit: {
        name: 'EDIT',
        buttonType: 'button',
        buttonCssClasses: 'ns-button ns-button--small ns-button--border ns-button__inline',
        iconCssClasses: 'fas fa-pen',
        translationKeyForButtonText: '',
      },
      delete: {
        name: 'DELETE',
        buttonType: 'button',
        buttonCssClasses: 'ns-button ns-button--small ns-button--border ns-button__inline',
        iconCssClasses: 'fas fa-trash',
        translationKeyForButtonText: '',
      }
    };
  }

  getShowButtonCondition(buttonName: string): boolean {
    switch (buttonName) {
      case this.buttons.edit.name:
      case this.buttons.delete.name:
      default:
        return true;
    }
  }

  onButtonClick(data: any, buttonName: string): void {
    switch (buttonName) {
      case this.buttons.edit.name:
        this.goToEditTimesheetForm(this.selectedId);
        break;
      case this.buttons.delete.name:
        this.showDialog();
        break;
      default:
        return;
    }
  }

  /* ----- MISC ----- */

  getElementName(elementFullName: string): string {
    return elementFullName.split('.')[1];
  }

  getElementDisplayName(elementFullName: string): string {
    return Utils.getElementDisplayName(elementFullName);
  }

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }

  checkTabElements = async () => {
    while (document.getElementsByClassName('ns-tabs__links').length < this.numberOfDataChildren) {
      await new Promise(resolve => {
        requestAnimationFrame(resolve);
      });
    }

    return true;
  }

  checkTimesheet = async () => {
    while (!this.timesheet) {
      await new Promise(resolve => {
        requestAnimationFrame(resolve);
      });
    }

    return true;
  }
}
