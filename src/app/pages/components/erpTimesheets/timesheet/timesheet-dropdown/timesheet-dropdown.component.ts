import {AfterViewInit, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {DropdownIn} from '../../../../ui/dropdown/_models/DropdownIn';
import {of, Subject, Subscription} from 'rxjs';
import {DropdownService} from '../../../../ui/dropdown/dropdown.service';
import {FilterService} from '../../../filters/_services/filter.service';
import {catchError, debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {Utils} from '../../../_utils/Utils';
import {TimesheetDropdownData} from '../_models/TimesheetDropdownData';
import {appPageSize} from '../../../_properties/appPageSize';
import {AjaxError, ajaxGet} from 'rxjs/internal-compatibility';
import {getHttpReadOptions} from '../../../../../state/reducers/session.reducer';
import { externalNames } from '../../../_properties/externalNames';
import {elementFullNames} from '../../../_properties/elementFullNames';

@Component({
  selector: 'app-timesheet-dropdown',
  templateUrl: './timesheet-dropdown.component.html',
  styleUrls: ['./timesheet-dropdown.component.css']
})
export class TimesheetDropdownComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  /**
   * Name of component where the dropdown is located.
   *
   * Valid values: 'element-form', 'element-filter'
   */
  @Input() dropdownIn: DropdownIn;

  /**
   * Name of the field that the items should be filtered by when user types in the input field of dropdown
   *
   * For value fields only
   *
   * Examples: name, email, mobile...
   */
  @Input() searchBy: string;
  @Input() dropdownInitialValue: any;
  @Input() required: boolean;
  @Output() showFilterEvent = new EventEmitter<boolean>();
  @Output() chosenItemModifiedEvent = new EventEmitter<any>();

  private searchTerm$ = new Subject<string>();
  private searchSubscription: Subscription;

  private selectedItemFromRootFilterListSubscription: Subscription;

  timesheetDropdownData: TimesheetDropdownData;

  constructor(
    private dropdownService: DropdownService,
    private filterService: FilterService,
  ) {}

  get elementFullName(): string {
    return elementFullNames.erpTimesheets.timesheet;
  }

  /* ----- LIFE CYCLES ----- */

  ngOnInit(): void {
    this.addDropdownOnInit();
    this.subscribeToSearchTerm();
    this.subscribeToSelectedItemFromRootFilterList();
  }

  ngAfterViewInit(): void {
    this.dropdownService.cancelDialog();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.dropdownInitialValue) {
      this.modifyChosenItem(this.dropdownInitialValue);
    }
  }

  ngOnDestroy(): void {
    this.selectedItemFromRootFilterListSubscription.unsubscribe();
    this.searchSubscription.unsubscribe();
    this.dropdownService.removeDropdown(this.elementFullName, this.dropdownIn);
  }

  /* ----- GETTERS ----- */

  get elementName(): string {
    return this.elementFullName.split('.')[1];
  }

  get chosenItem(): any {
    return this.timesheetDropdownData.chosenItem;
  }

  /* ----- ON INIT ----- */

  private addDropdownOnInit(): void {
    // Add this dropdown to the store, using default values for dropdown data
    this.addDropdown();

    this.timesheetDropdownData = this.dropdownService.getDropdownData(this.elementFullName, this.dropdownIn);

    // this is true only when this dropdown is created in a form
    if (this.dropdownInitialValue) {
      this.modifyChosenItem(this.dropdownInitialValue);
    }

    /*
    When user selects an item from filter list:
    - If there is a parent filter, this filter view will be closed to display the parent filter.
    In this parent filter view, the dropdown for that selected item is created with fresh data.
    To display the selected item, we need to put this selected value to the 'chosenItem' field of dropdown data.
    This is done by 'this.modifyChosenItem()'.

    - If there's no parent filter, the filter view will not be closed, thus no dropdown view is created.
    */
    if (this.dropdownIn === DropdownIn.ELEMENT_FILTER) {
      this.retrieveChosenItemFromFilterData();
    }
  }

  private retrieveChosenItemFromFilterData(): void {
    if (this.filterService.currentDisplayedFilter().selectedLinkFields.length > 0) {
      const savedSelectedLinkField = this.filterService.currentDisplayedFilter().selectedLinkFields
        .find(savedItem => savedItem.elementFullName === this.elementFullName);
      if (savedSelectedLinkField && savedSelectedLinkField.value) {
        this.modifyChosenItem(savedSelectedLinkField.value);
      }
    }
  }

  private subscribeToSearchTerm(): void {
    // Subscribe to local searchTerm$
    this.searchSubscription = this.searchTerm$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      map((term: string) => {
        this.timesheetDropdownData.queries[this.searchBy] = term.trim();
        this.getItems();
      })
    ).subscribe();
  }

  private subscribeToSelectedItemFromRootFilterList(): void {
    this.selectedItemFromRootFilterListSubscription = this.filterService.selectedItemFromRootFilterList$.subscribe(
      selectedItemFromRootFilterList => {
        if (this.elementFullName === this.filterService.currentDisplayedFilter().elementFullName) {
          this.modifyChosenItem(selectedItemFromRootFilterList);
        }
      }
    );
  }

  /* ----- HTTP REQUESTS ----- */

  private getItems(): void {
    this.timesheetDropdownData.loading = true;

    let url = Utils.getElementApiEndpoint(this.elementFullName);
    url += '?pagesize=' + appPageSize;

    const fieldNames = Object.keys(this.timesheetDropdownData.queries);
    if (fieldNames.length > 0) {
      fieldNames.forEach(fieldName => {
        url += '&' + fieldName + '=' + this.timesheetDropdownData.queries[fieldName];
      });
    }

    // Todo: see how to do the ajaxGet for dropdown
    ajaxGet(url, getHttpReadOptions())
      .pipe(
        // delay(1000), // uncomment to see the state change when loading
        map((ajaxResponse) => {
          return ajaxResponse.response;
        }),
        catchError((ajaxError: AjaxError) => {
          return of(ajaxError.response);
        })
      )
      .subscribe(
        response => {
          let elementPluralName;
          if (externalNames[this.elementFullName]) {
            elementPluralName = Utils.firstToLower(externalNames[this.elementFullName].split('_')[1]);
          } else {
            elementPluralName = this.elementFullName.split('.')[1] + 's';
          }
          this.timesheetDropdownData.items = response._embedded[elementPluralName];
          this.timesheetDropdownData.itemPage = response._page;
          this.timesheetDropdownData.loading = false;
          if (this.timesheetDropdownData.error) {
            this.timesheetDropdownData.error = null;
          }
        },
        error => {
          this.timesheetDropdownData.error = error;
        });
  }

  /* ----- DROPDOWN ----- */

  private addDropdown(): void {
    const dropdownData = new TimesheetDropdownData();
    dropdownData.dropdownIn = this.dropdownIn;
    this.dropdownService.addDropdown(dropdownData);
  }

  toggleDropdownOptions(): void {
    if (this.timesheetDropdownData.isDropdownOptionsShown) {
      this.timesheetDropdownData.isDropdownOptionsShown = false;
    } else {
      this.timesheetDropdownData.isDropdownOptionsShown = true;
      this.getItems();
    }
  }

  toggleFilterComponentForCurrentField(): void {
    if (this.timesheetDropdownData.isDropdownOptionsShown) {
      this.timesheetDropdownData.isDropdownOptionsShown = false;
    }

    // In a form page, we have only one area to display filter, but we can have a few possible filters (for each dropdown)
    // When we open filter for a dropdown field of a form, this filter must be added as root filter
    // Therefore we need to reset filter data
    if (this.dropdownIn === DropdownIn.ELEMENT_FORM) {
      const isFilterShown = this.filterService.filters.length !== 0 &&
        this.filterService.filters[0].elementFullName === this.elementFullName;

      if (this.filterService.filters.length !== 0) {
        this.filterService.clearAllFilters();
      }

      if (isFilterShown) {
        this.showFilterEvent.emit(false);
      } else {
        this.showFilterEvent.emit(true);
        this.filterService.displayNewFilter(this.elementFullName);
      }
      return;
    }

    this.filterService.addFilter(this.elementFullName);
  }

  chooseADropdownOption(item: any): void {
    this.timesheetDropdownData.isDropdownOptionsShown = false;
    this.modifyChosenItem(item);
  }

  clearInputField(): void {
    this.modifyChosenItem(null);
  }

  private modifyChosenItem(item): void {
    this.timesheetDropdownData.chosenItem = item;
    this.chosenItemModifiedEvent.emit(item);
  }

  handleDropdownOnKeyUp($event: KeyboardEvent): void {
    const isArrowUp = $event.key === 'ArrowUp';
    const isArrowDown = $event.key === 'ArrowDown';
    const isEnter = $event.key === 'Enter';

    if (isEnter) {
      if (this.timesheetDropdownData.isDropdownOptionsShown) {
        this.timesheetDropdownData.isDropdownOptionsShown = false;
        return;
      }
      this.timesheetDropdownData.isDropdownOptionsShown = true;
      return;
    }

    if (isArrowUp || isArrowDown) {
      // show the option list
      this.timesheetDropdownData.isDropdownOptionsShown = true;
      const dropdownCssClass = this.dropdownService.getDropdownCssClass(this.elementFullName, this.dropdownIn);
      const dropdownHTMLElement = document.querySelector('.' + dropdownCssClass);

      const activeDropdownOptionPosition = Utils.arrowKeyUpDownHandler(
        $event.key,
        dropdownHTMLElement,
        this.timesheetDropdownData.itemPage,
        this.timesheetDropdownData.activeDropdownOptionPosition
      );

      this.timesheetDropdownData.activeDropdownOptionPosition = activeDropdownOptionPosition;

      const chosenItem = this.timesheetDropdownData.items[activeDropdownOptionPosition];
      this.modifyChosenItem(chosenItem);

      return;
    }

    this.searchHandler($event);
  }

  private searchHandler($event: KeyboardEvent): void {
    const input = ($event.target as HTMLInputElement).value;
    this.timesheetDropdownData.activeDropdownOptionPosition = null;
    this.timesheetDropdownData.isDropdownOptionsShown = true;

    this.searchTerm$.next(input);
  }

  /* ----- MISC ----- */

  cssClass(): string {
    return this.dropdownService.getDropdownCssClass(this.elementFullName, this.dropdownIn);
  }

  getDropdownLabel(): string {
    if (Utils.getElementDisplayName(this.elementFullName)) {
      return Utils.getElementDisplayName(this.elementFullName);
    }
    return this.elementFullName;
  }

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
