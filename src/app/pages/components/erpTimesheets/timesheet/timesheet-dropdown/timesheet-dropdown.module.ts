import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TimesheetDropdownComponent} from './timesheet-dropdown.component';



@NgModule({
  declarations: [
    TimesheetDropdownComponent
  ],
  exports: [
    TimesheetDropdownComponent
  ],
  imports: [
    CommonModule
  ]
})
export class TimesheetDropdownModule { }
