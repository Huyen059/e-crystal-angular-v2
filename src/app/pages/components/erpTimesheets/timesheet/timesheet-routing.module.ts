import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {TimesheetWorkspaceComponent} from './timesheet-workspace/timesheet-workspace.component';
import {TimesheetListComponent} from './timesheet-list/timesheet-list.component';
import {TimesheetNewComponent} from './timesheet-new/timesheet-new.component';
import {TimesheetEditComponent} from './timesheet-edit/timesheet-edit.component';
import {TimesheetDetailComponent} from './timesheet-detail/timesheet-detail.component';

import {TimesheetEntryWorkspaceComponent} from '../timesheetEntry/timesheetentry-workspace/timesheetEntry-workspace.component';
import {TimesheetEntryListComponent} from '../timesheetEntry/timesheetEntry-list/timesheetEntry-list.component';
import {TimesheetEntryNewComponent} from '../timesheetEntry/timesheetEntry-new/timesheetEntry-new.component';
import {TimesheetEntryEditComponent} from '../timesheetEntry/timesheetEntry-edit/timesheetEntry-edit.component';

import {TimesheetRemarkWorkspaceComponent} from '../timesheetRemark/timesheetRemark-workspace/timesheetRemark-workspace.component';
import {TimesheetRemarkListComponent} from '../timesheetRemark/timesheetRemark-list/timesheetRemark-list.component';
import {Utils} from '../../_utils/Utils';
import {elementFullNames} from '../../_properties/elementFullNames';

const routes: Routes = [
  {path: '', component: TimesheetWorkspaceComponent,
    data: {name: Utils.getElementDisplayName(elementFullNames.erpTimesheets.timesheet), isMainWorkspace: true},
    children: [
      {path: '', component: TimesheetListComponent, data: {name: '', isMainList: true}},
      {path: 'new', component: TimesheetNewComponent, data: {name: Utils.getDisplayText('new')}},
      {path: 'edit', component: TimesheetEditComponent, data: {name: Utils.getDisplayText('edit')}},
      {path: ':externalId', component: TimesheetDetailComponent, data: {name: Utils.getDisplayText('detail')},
        children: [
          {path: '', redirectTo: 'timesheetEntry', pathMatch: 'full'},
          {path: 'timesheetEntry', component: TimesheetEntryWorkspaceComponent,
            data: {name: Utils.getElementDisplayName('erpTimesheets.timesheetEntry')},
            children: [
              {path: '', component: TimesheetEntryListComponent, data: {name: ''}},
              {path: 'new', component: TimesheetEntryNewComponent, data: {name: Utils.getDisplayText('new')}},
              {path: 'edit', component: TimesheetEntryEditComponent, data: {name: Utils.getDisplayText('edit')}},
            ]
          },
          {path: 'timesheetRemark', component: TimesheetRemarkWorkspaceComponent,
            data: {name: Utils.getElementDisplayName('erpTimesheets.timesheetRemark')},
            children: [
              {path: '', component: TimesheetRemarkListComponent, data: {name: ''}},
            ]
          },
        ]
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimesheetRoutingModule { }
