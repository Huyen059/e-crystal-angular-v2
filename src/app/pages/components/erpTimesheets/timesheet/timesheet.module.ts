import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {TimesheetRoutingModule} from './timesheet-routing.module';

import {TimesheetWorkspaceComponent} from './timesheet-workspace/timesheet-workspace.component';
import {TimesheetListComponent} from './timesheet-list/timesheet-list.component';
import {TimesheetNewComponent} from './timesheet-new/timesheet-new.component';
import {TimesheetEditComponent} from './timesheet-edit/timesheet-edit.component';
import {TimesheetDetailComponent} from './timesheet-detail/timesheet-detail.component';

import {FiltersModule} from '../../filters/filters.module';
import {UiModule} from '../../../ui/ui.module';
import {EmployeeDropdownModule} from '../../enterprise/employee/employee-dropdown/employee-dropdown.module';


@NgModule({
  declarations: [
    TimesheetWorkspaceComponent,
    TimesheetListComponent,
    TimesheetNewComponent,
    TimesheetEditComponent,
    TimesheetDetailComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    FiltersModule,
    UiModule,
    TimesheetRoutingModule,
    EmployeeDropdownModule,
  ]
})
export class TimesheetModule {
}
