import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {Location} from '@angular/common';

import {Utils} from '../../../_utils/Utils';
import {elementFullNames} from '../../../_properties/elementFullNames';

import {TimesheetService} from '../_services/timesheet.service';
import {Timesheet} from '../_models/Timesheet';
import {TimesheetOutputModel} from '../_models/TimesheetOutputModel';
import {TimesheetPutInputMapper} from '../_models/TimesheetPutInputMapper';
import {TimesheetMapper} from '../_models/TimesheetMapper';

import {FilterService} from '../../../filters/_services/filter.service';
import {DropdownService} from '../../../../ui/dropdown/dropdown.service';
import {NavigationService} from '../../../_services/navigation.service';

import {select} from '@angular-redux/store';
import {ErpTimesheetsTimesheetSlicePaths as paths} from '../../../../../state/slices/erpTimesheets.timesheet.slice';

import {DropdownIn} from '../../../../ui/dropdown/_models/DropdownIn';

@Component({
  selector: 'app-timesheet-edit',
  templateUrl: './timesheet-edit.component.html',
  styleUrls: ['./timesheet-edit.component.css']
})
export class TimesheetEditComponent implements OnInit, AfterViewInit, OnDestroy {
  private selectedId: string;
  formModel: Timesheet = new Timesheet();

  /* For dropdown:start */
  displayDropdown = false;
  dropdownIn = DropdownIn.ELEMENT_FORM;
  dropdownElements = [
    {
      elementFullName: 'enterprise.employee',
      searchBy: 'name',
      required: true,
    },
  ];
  /* For dropdown:end */

  buttons: {[key: string]: {[key: string]: any}} = {};

  isFilterMenuShown = false;

  @select([elementFullNames.erpTimesheets.timesheet, paths.edit_requests_GET, 'timesheet'])
  timesheet$: Observable<TimesheetOutputModel>;
  timesheetSubscription: Subscription;

  @select([elementFullNames.erpTimesheets.timesheet, paths.edit_requests_UPDATE]) updateRequest$: Observable<any>;
  private updateRequestSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private timesheetService: TimesheetService,
    private filterService: FilterService,
    private dropdownService: DropdownService,
    private navigationService: NavigationService,
  ) {

  }

  /* ----- LIFE CYCLES ----- */

  ngOnInit(): void {
    this.loadButtonsData();
    this.updateHeaderBreadcrumb();

    this.getTimesheet();
    this.checkFormModelValueRetrieved().then(() => {
      this.displayDropdown = true;
    });

    this.timesheetSubscription = this.timesheet$.subscribe(timesheet => {
      if (timesheet) {
        this.formModel = TimesheetMapper.map(timesheet);
      }
    });

    this.updateRequestSubscription = this.updateRequest$.subscribe(requestData => {
      if (requestData && requestData.backUrl) {
        this.router.navigate([requestData.backUrl]);
      }
    });
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.timesheetSubscription.unsubscribe();
    this.updateRequestSubscription.unsubscribe();
    this.filterService.clearAllFilters();
    this.timesheetService.removeAlert();
    this.timesheetService.removeRequestData(paths.edit_requests_UPDATE);
    this.timesheetService.removeRequestData(paths.edit_requests_GET);
  }

  /* ----- GETTERS ----- */

  get elementFullName(): string {
    return this.timesheetService.elementFullName;
  }

  get fields(): { [key: string]: string } {
    return this.timesheetService.fields;
  }

  /* ----- ON INIT ----- */

  private updateHeaderBreadcrumb(): void {
    this.navigationService.changeRouter(this.router);
  }

  /* ----- HTTP REQUESTS ----- */

  private getTimesheet(): void {
    this.selectedId = this.route.snapshot.paramMap.get('externalId');
    this.timesheetService.editFetchItem(this.selectedId);
  }

  saveTimesheet(): void {
    const backUrl = this.timesheetService.generateBackUrl();
    this.timesheetService.updateItem(TimesheetPutInputMapper.map(this.formModel), backUrl);
  }

  /* ----- CLOSE FORM ----- */

  closeForm(): void {
    this.location.back();
  }

  /* ----- DROPDOWN INITIAL VALUE ----- */

  getDropdownInitialValue(elementFullName: string): any {
    return this.formModel[this.getElementName(elementFullName)];
  }

  /* ----- DROPDOWN -----*/

  dropdownInputData(elementFullName: string): any {
    return this.dropdownElements.find(dropdown => dropdown.elementFullName === elementFullName);
  }

  chosenItemModifiedHandler(chosenItem: any, elementFullName: string): void {
    const fieldName = elementFullName.split('.')[1];
    this.formModel[fieldName] = chosenItem;
  }

  /* ----- BUTTONS -----*/
  private loadButtonsData(): void {
    this.buttons = {
      cancel: {
        name: 'CANCEL',
        buttonType: 'button',
        buttonCssClasses: 'ns-button ns-button--cancel ns-button__inline',
        iconCssClasses: '',
        translationKeyForButtonText: 'cancel',
      },
      save: {
        name: 'SAVE',
        buttonType: 'button',
        buttonCssClasses: 'ns-button ns-button--primary ns-button__inline',
        iconCssClasses: '',
        translationKeyForButtonText: 'save',
      }
    };
  }

  getShowButtonCondition(buttonName: string): boolean {
    switch (buttonName) {
      case this.buttons.cancel.name:
      case this.buttons.save.name:
      default:
        return true;
    }
  }

  onButtonClick(data: any, buttonName: string): void {
    switch (buttonName) {
      case this.buttons.cancel.name:
        this.closeForm();
        break;
      case this.buttons.save.name:
        this.saveTimesheet();
        break;
      default:
        return;
    }
  }

  /* ----- MISC ----- */

  getElementName(elementFullName: string): string {
    return elementFullName.split('.')[1];
  }

  getFieldDisplayName(fieldName: string): string {
    return Utils.getFieldDisplayName(this.elementFullName, fieldName);
  }

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }

  checkFormModelValueRetrieved = async () => {
    while (!this.formModel.externalId) {
      await new Promise(resolve => {
        requestAnimationFrame(resolve);
      });
    }
    return true;
  }
}
