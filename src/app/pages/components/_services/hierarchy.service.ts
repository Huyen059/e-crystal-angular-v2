import { Injectable } from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {AppState} from '../../../state/AppState';
import {HierarchyActionFactory} from '../../../state/actions/hierarchy.actions';

@Injectable({
  providedIn: 'root'
})
export class HierarchyService {

  constructor(
    private ngRedux: NgRedux<AppState>,
  ) { }

  /* ----- REDUX ----- */

  addLevel(elementFullName: string, value: any, childrenFullNames: string[] = []): void {
    this.ngRedux.dispatch(HierarchyActionFactory.levelAdded(elementFullName, value, childrenFullNames));
  }

  removeLastLevel(): void {
    this.ngRedux.dispatch(HierarchyActionFactory.lastLevelRemoved());
  }

  removeAllLevels(): void {
    this.ngRedux.dispatch(HierarchyActionFactory.allLevelsRemoved());
  }
}
