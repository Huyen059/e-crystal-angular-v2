import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkingYearRoutingModule } from './workingYear-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    WorkingYearRoutingModule
  ]
})
export class WorkingYearModule { }
