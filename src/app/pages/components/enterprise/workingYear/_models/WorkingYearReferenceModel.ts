export class WorkingYearReferenceModel {
  resourceUri: string;
  name: string;
  externalId: string;
}
