import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmployeeTypeDropdownComponent} from './employeeType-dropdown.component';



@NgModule({
  declarations: [
    EmployeeTypeDropdownComponent
  ],
  exports: [
    EmployeeTypeDropdownComponent
  ],
  imports: [
    CommonModule
  ]
})
export class EmployeeTypeDropdownModule { }
