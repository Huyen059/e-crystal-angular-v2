import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeTypeRoutingModule } from './employeeType-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EmployeeTypeRoutingModule
  ]
})
export class EmployeeTypeModule { }
