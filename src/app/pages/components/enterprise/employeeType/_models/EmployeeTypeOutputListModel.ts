import {EmployeeOutputModel} from '../../employee/_models/EmployeeOutputModel';

export class EmployeeTypeOutputListModel {
  constructor(
    public employeeTypes: EmployeeOutputModel[] = []
  ) {
  }
}
