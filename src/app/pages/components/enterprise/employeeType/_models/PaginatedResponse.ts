import {EmployeeTypeOutputListModel} from './EmployeeTypeOutputListModel';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';

export class PaginatedResponse {
  constructor(
    public _embedded: EmployeeTypeOutputListModel = new EmployeeTypeOutputListModel(),
    public _links: LinksModel = null,
    public _page: PageModel = null
  ) {
  }
}
