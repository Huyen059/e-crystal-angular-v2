import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubprojectRoutingModule } from './subproject-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SubprojectRoutingModule
  ]
})
export class SubprojectModule { }
