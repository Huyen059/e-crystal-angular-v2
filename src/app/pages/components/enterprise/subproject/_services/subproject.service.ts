import {Injectable} from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class SubprojectService {
  elementFullName = 'enterprise.subproject';

  fields = {
    name: 'name',
    project: 'project',
  };

  constructor() {}
}
