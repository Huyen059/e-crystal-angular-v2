import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SubprojectDropdownComponent} from './subproject-dropdown.component';



@NgModule({
  declarations: [
    SubprojectDropdownComponent
  ],
  exports: [
    SubprojectDropdownComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SubprojectDropdownModule { }
