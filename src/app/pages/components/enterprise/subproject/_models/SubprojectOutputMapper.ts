import {SubprojectReferenceModel} from './SubprojectReferenceModel';
import {SubprojectOutputModel} from './SubprojectOutputModel';

export class SubprojectOutputMapper {
  static map(subprojectReferenceModel: SubprojectReferenceModel): SubprojectOutputModel {
    const subprojectOutputModel = new SubprojectOutputModel();
    subprojectOutputModel.name = subprojectReferenceModel.name;
    subprojectOutputModel.externalId = subprojectReferenceModel.externalId;
    return subprojectOutputModel;
  }
}
