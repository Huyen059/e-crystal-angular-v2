import {SubprojectOutputModel} from './SubprojectOutputModel';

export class SubprojectOutputListModel {
  constructor(
    public subprojects: SubprojectOutputModel[] = []
  ) {
  }
}
