import {ProjectOutputModel} from '../../project/_models/ProjectOutputModel';

export class Subproject {
  constructor(
    public name: string = null,
    public externalId: string = null,
    public project: ProjectOutputModel = null,
  ) {  }
}
