import {SubprojectOutputListModel} from './SubprojectOutputListModel';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';

export class PaginatedResponse {
  constructor(
    public _embedded: SubprojectOutputListModel = new SubprojectOutputListModel(),
    public _links: LinksModel = null,
    public _page: PageModel = null
  ) {
  }
}
