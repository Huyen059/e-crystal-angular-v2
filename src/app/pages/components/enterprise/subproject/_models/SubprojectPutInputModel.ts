export class SubprojectPutInputModel {
  constructor(
    public name: string = null,
    public externalId: string = null,
    public project: string = null,
  ) {  }
}
