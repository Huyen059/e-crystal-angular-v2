import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmployeeDropdownComponent} from './employee-dropdown.component';



@NgModule({
  declarations: [
    EmployeeDropdownComponent
  ],
  exports: [
    EmployeeDropdownComponent
  ],
  imports: [
    CommonModule
  ]
})
export class EmployeeDropdownModule { }
