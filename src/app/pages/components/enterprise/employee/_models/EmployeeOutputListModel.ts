import {EmployeeOutputModel} from './EmployeeOutputModel';

export class EmployeeOutputListModel {
  constructor(
    public employees: EmployeeOutputModel[] = []
  ) {
  }
}
