import {SourcerOutputModel} from '../../sourcer/_models/SourcerOutputModel';
import {PersonOutputModel} from '../../person/_models/PersonOutputModel';
import {EmployeeTypeOutputModel} from '../../employeeType/_models/EmployeeTypeOutputModel';

export class Employee {
  constructor(
    public active: string = null,
    public contractType: string = null,
    public email: string = null,
    public employeeType: EmployeeTypeOutputModel = null,
    public externalId: string = null,
    public mobile: string = null,
    public name: string = null,
    public person: PersonOutputModel = null,
    public sourcer: SourcerOutputModel = null,
  ) {  }
}
