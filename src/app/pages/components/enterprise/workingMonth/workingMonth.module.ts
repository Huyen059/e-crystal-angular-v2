import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkingMonthRoutingModule } from './workingMonth-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    WorkingMonthRoutingModule
  ]
})
export class WorkingMonthModule { }
