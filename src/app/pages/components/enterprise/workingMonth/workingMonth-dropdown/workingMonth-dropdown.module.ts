import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WorkingMonthDropdownComponent} from './workingMonth-dropdown.component';



@NgModule({
  declarations: [
    WorkingMonthDropdownComponent
  ],
  exports: [
    WorkingMonthDropdownComponent
  ],
  imports: [
    CommonModule
  ]
})
export class WorkingMonthDropdownModule { }
