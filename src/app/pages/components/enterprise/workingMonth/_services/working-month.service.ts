import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WorkingMonthService {
  elementFullName = 'enterprise.workingMonth';

  fields = {
    name: 'name',
    month: 'month',
    year: 'year',
  };

  constructor() {}
}
