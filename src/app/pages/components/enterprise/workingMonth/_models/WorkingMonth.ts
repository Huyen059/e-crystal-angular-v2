import {WorkingYearOutputModel} from '../../workingYear/_models/WorkingYearOutputModel';

export class WorkingMonth {
  constructor(
    public name: string = null,
    public externalId: string = null,
    public month: string = null,
    public year: WorkingYearOutputModel = null,
  ) {
  }
}

