import {WorkingMonthReferenceModel} from './WorkingMonthReferenceModel';
import {WorkingMonthOutputModel} from './WorkingMonthOutputModel';

export class WorkingMonthOutputMapper {
  static map(workingMonthReferenceModel: WorkingMonthReferenceModel): WorkingMonthOutputModel {
    const workingMonthOutputModel = new WorkingMonthOutputModel();
    workingMonthOutputModel.name = workingMonthReferenceModel.name ? workingMonthReferenceModel.name : null;
    workingMonthOutputModel.externalId = workingMonthReferenceModel.externalId ? workingMonthReferenceModel.externalId : null;

    return workingMonthOutputModel;
  }
}

