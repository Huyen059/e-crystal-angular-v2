import {WorkingMonthOutputModel} from './WorkingMonthOutputModel';

export class WorkingMonthOutputListModel {
  constructor(
    public workingMonths: WorkingMonthOutputModel[] = []
  ) {
  }
}
