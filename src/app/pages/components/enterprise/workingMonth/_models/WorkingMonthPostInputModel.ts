export class WorkingMonthPostInputModel {
  constructor(
    public name: string = null,
    public externalId: string = null,
    public month: string = null,
    public year: string = null
  ) {
  }
}

