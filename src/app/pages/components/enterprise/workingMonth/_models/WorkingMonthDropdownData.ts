import {DropdownIn} from '../../../../ui/dropdown/_models/DropdownIn';
import {PageModel} from '../../../_models/PageModel';
import {DropdownData} from '../../../../ui/dropdown/_models/DropdownData';
import {elementFullNames} from '../../../_properties/elementFullNames';

export class WorkingMonthDropdownData extends DropdownData{
  elementFullName = elementFullNames.enterprise.workingMonth;
  dropdownIn: DropdownIn;
  // used for async request
  items: any[] = [];
  itemPage: PageModel = null;
  chosenItem: any = null;
  loading = false;
  error: string = null;
  queries: { [fieldName: string]: string } = {};
  // used to toggle dropdown options view
  isDropdownOptionsShown = false;
  // used for tracking position of active item when using arrow up/down
  activeDropdownOptionPosition: number = null;
}
