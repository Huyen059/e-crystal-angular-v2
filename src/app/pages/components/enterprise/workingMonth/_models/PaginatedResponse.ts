import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {WorkingMonthOutputListModel} from './WorkingMonthOutputListModel';

export class PaginatedResponse {
  constructor(
    public _embedded: WorkingMonthOutputListModel = new WorkingMonthOutputListModel(),
    public _links: LinksModel = null,
    public _page: PageModel = null
  ) {
  }
}
