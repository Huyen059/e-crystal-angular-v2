export class SourcerOutputModel {
  externalId: string;
  name: string;
  sourcerType: {
    resourceUri: string,
    name: string,
    externalId: string
  };
}
