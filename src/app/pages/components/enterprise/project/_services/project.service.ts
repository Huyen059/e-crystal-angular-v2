import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  elementFullName = 'enterprise.project';

  fields = {
    name: 'name',
    subproject: 'subproject',
  };

  constructor() {}
}
