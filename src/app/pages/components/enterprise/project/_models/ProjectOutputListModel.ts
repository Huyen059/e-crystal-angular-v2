import {ProjectOutputModel} from './ProjectOutputModel';

export class ProjectOutputListModel {
  constructor(
    public projects: ProjectOutputModel[] = []
  ) {
  }
}
