import {TimesheetReferenceModel} from '../../../erpTimesheets/timesheet/_models/TimesheetReferenceModel';

export class ProjectOutputModel {
  name: string;
  clientReference: string;
  developmentBudget: number;
  maintenanceBudget: number;
  invoicingMode: string;
  internalCode: string;
  externalId: string;
  client: TimesheetReferenceModel;
  order: TimesheetReferenceModel;
  clientResponsible: TimesheetReferenceModel;
  projectManager: TimesheetReferenceModel;
  technicalLead: TimesheetReferenceModel;
  domainAnalyst: TimesheetReferenceModel;
}
