import {ProjectReferenceModel} from './ProjectReferenceModel';
import {ProjectOutputModel} from './ProjectOutputModel';

export class ProjectOutputMapper {
  static map(projectReferenceModel: ProjectReferenceModel): ProjectOutputModel {
    const projectOutputModel = new ProjectOutputModel();
    projectOutputModel.name = projectReferenceModel.name;
    projectOutputModel.externalId = projectReferenceModel.externalId;
    return projectOutputModel;
  }
}
