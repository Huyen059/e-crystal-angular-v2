import {ProjectOutputListModel} from './ProjectOutputListModel';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';

export class PaginatedResponse {
  constructor(
    public _embedded: ProjectOutputListModel = new ProjectOutputListModel(),
    public _links: LinksModel = null,
    public _page: PageModel = null
  ) {
  }
}
