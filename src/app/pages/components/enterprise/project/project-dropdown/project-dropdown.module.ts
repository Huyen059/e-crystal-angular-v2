import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProjectDropdownComponent} from './project-dropdown.component';



@NgModule({
  declarations: [
    ProjectDropdownComponent
  ],
  exports: [
    ProjectDropdownComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ProjectDropdownModule { }
