import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {LoginPostInputModel} from '../_models/LoginPostInputModel';
import {Utils} from '../../pages/components/_utils/Utils';
import {LoginActionFactory} from '../../state/actions/session.actions';
import {NgRedux, select} from '@angular-redux/store';
import {AppState} from '../../state/AppState';
import {Observable, Subscription} from 'rxjs';
import {User} from '../_models/User';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  private submitted = false;
  loginErrorTranslationKey: string;

  @select(['session', 'user']) user$: Observable<User>;
  private userSubscription: Subscription;

  @select(['session', 'error']) error$: Observable<string>;
  private errorSubscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private ngRedux: NgRedux<AppState>,
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.userSubscription = this.user$.subscribe(user => {
      if (user !== null) {
        this.router.navigate(['/']);
      }
    });

    this.errorSubscription = this.error$.subscribe(errorTranslationKey => this.loginErrorTranslationKey = errorTranslationKey);
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
    this.errorSubscription.unsubscribe();
  }

  get form(): { [p: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.loginForm.invalid) {
      this.loginErrorTranslationKey = 'allFieldsRequiredError';
      return;
    }

    const userData = new LoginPostInputModel();
    userData.username = this.form.username.value;
    userData.password = this.form.password.value;

    this.ngRedux.dispatch(LoginActionFactory.userLoggedInAction(userData));
  }
  /* ----- MISC ----- */

  getDisplayText(text: string): string {
    return Utils.getDisplayText(text);
  }
}
