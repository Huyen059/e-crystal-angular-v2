export class LoginOutputModel {
  message: string;
  authenticated: boolean;
  profileName: string;
}
