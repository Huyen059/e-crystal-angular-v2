export class User {
  username: string;
  password: string;
  authorizationData: string;
  profileName: string;
}
