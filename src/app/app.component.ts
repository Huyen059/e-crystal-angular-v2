import {Component, OnInit} from '@angular/core';
import {Languages} from './pages/components/_models/Languages';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    if (!localStorage.getItem('language')) {
      localStorage.setItem('language', Languages.getDefaultLanguage().name);
    }
  }
}
