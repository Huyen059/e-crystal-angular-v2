import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';

import {DevToolsExtension, NgRedux, NgReduxModule} from '@angular-redux/store';
import {AppState} from './state/AppState';

import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {environment} from '../environments/environment';
import {epicMiddleware, INITIAL_STATE, rootReducer} from './state/store';
import {sessionEpics} from './state/epics/session.epic';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgReduxModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    private ngRedux: NgRedux<AppState>,
    private devTools: DevToolsExtension,
  ) {
    let enhancers = [];
    // ... add whatever other enhancers you want.

    // You probably only want to expose this tool in devMode.
    if (!environment.production && devTools.isEnabled()) {
      enhancers = [ ...enhancers, devTools.enhancer({trace: true, traceLimit: 5}) ];
      // enhancers = [ ...enhancers, devTools.enhancer() ];
    }

    let middlewares = [];


    middlewares = [...middlewares, epicMiddleware];

    // Tell @angular-redux/store about our rootReducer and our initial state.
    // It will use this to create a redux store for us and wire up all the
    // events
    ngRedux.configureStore(rootReducer, INITIAL_STATE, middlewares, enhancers);

    const epics = [
      ...sessionEpics,
    ];

    epics.forEach(epic => epicMiddleware.run(epic));

  }
}
